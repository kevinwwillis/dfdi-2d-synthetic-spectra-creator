function create2Dspectrum(tau_int, C_int, wl_hiR_sun_downsample_int, combBeamWLtemplate_int, calcCombParams_q)

%% Create synth spectrum with no LSF or downsample

% Testing params


% [smoothedProfile, ~] = getSpectraProfile(fitsread('E:\Research\1a_syntheticPipelineTestEnv\39_deslant_sun.fits'), 1);
% profileTemplate = smoothedProfile; clear smoothedProfile

realSpectrum1 = mat2gray(fitsread('E:\Research\1a_syntheticPipelineTestEnv\39_deslant_sun.fits'));

realSpectrum = continnumNorm(realSpectrum1, 300);
figure(88); clf(figure(88)); imagesc(realSpectrum); colormap gray

[profileTemplate, ~] = getSpectraProfile(realSpectrum, 1);

% Create the comb
combImg = createComb(wl_hiR_m, Y_px, C, tau_1, parP_q);

% Multiply the comb and sun spectrum
synthSpectrum = mat2gray(combImg) .* repmat(flux_pxv, size(combImg, 1), 1); %clear flux_pxv combImg Y0