function [I, wl] = createSyntheticComb_V5(imgWidth, imgHeight, wl, xUpsample, yUpsample, endSepWanted_px)

%% Testing Params

% clearvars;
% 
% img = fitsread('E:\Research\1D_pipeline_orig\HIP14810\profile\101_profile_fit\17_profile.fits');
% wl = fitsread(strcat('E:\Research\2DsyntheticSpectraCreation\HIP_101_STAR_wlsoln.fits'));
% 
% yUpsample = 20;
% xUpsample = 20;
% 
% imgHeight = 25;
% imgWidth = 4001;
% 
% beamNum = 101;
% 
% obsNum = 17;
% 
% endSepWanted_px = 1.6;
% 
% 
% wl = wl(17,:) * 1e-10;

% load('E:\Research\1D_pipeline_orig\inputs\wlsoln.mat', 'wlsoln')



% yb = smoothn(fftFilter_V1(img(:,500), 2), 7);
% [a,b,phase_col500, period_col500, fit] = sinefit((1:length(yb))', yb, 6.5, 9);
% origPeaks_col500_x = (phase_col500:period_col500:imgHeight);


%% Spatial Matricies

row_pixel_height = imgHeight;

row_pixel_range = (1 :1/yUpsample: row_pixel_height)';

col_pixel_range = 1 :1/xUpsample: imgWidth;

% col_pixel_matrix = repmat(col_pixel_range, row_pixel_height, 1);

lambda = interp1((1:length(wl)), wl, (1:1/xUpsample:imgWidth), 'linear');

% lambda = wl .* 1e-10; %squeeze(wlsoln(:, 1, beamNum))' .* 1e-10;                            % Create equally spaced series of wavelengths in meters

lambda_matrix = repmat(lambda, length(row_pixel_range), 1);                  % Replicate the row of wavelengths to image height

%% Unused functions

% OPD = d ./ 2;

% d_m = lambda ./ (2 .* atan(opd1/opd2));                                        % Vertical fringe density (sine period)

%% Optical Path distance (OPD) at each physical pixel location.

% Solve for OPD based on the desired comb line separation at the far right side of the spectra.
l1 = lambda(col_pixel_range==4000);
l2 = lambda(col_pixel_range==4001);
endSep_wl = l2-l1;

l2 = l1 + endSepWanted_px * endSep_wl;

% Modify options setting
options = optimset;
options = optimset(options,'Display', 'off');
options = optimset(options,'MaxFunEvals', 1000);
options = optimset(options,'MaxIter',     1000);
options = optimset(options,'TolFun', 1e-6);
options = optimset(options,'TolX',   1e-6);

opd1 = fminsearch(@(o) 1 - (cos(2 * pi * o / l2) - cos(2 * pi * o / l1)) / (l2 - l1), 20 * 1e-3, options);

Nrange = (0:1/yUpsample:(imgHeight-1)) / 8;

OPD_n = ( Nrange * lambda_matrix(1, (col_pixel_range==2000)) ) + opd1;

OPD = repmat(OPD_n', 1, length(col_pixel_range));                                   % Replicate vector

%% Intensity Calculations

d_phi = (2 .* pi ./ lambda_matrix) .* OPD;                            % EM wave recombination phase offset

% phiPrime = repmat((row_pixel_range - 1) / 10, 1, imgWidth);               % (Not used)Row-wise phase offset (Estimation)

I_1 = 0.25;                                                              % Split beam 1 intesity

I_2 = 0.25;                                                              % Split beam 2 intesity

I = I_1 + I_2 + 2 .* (I_1 .* I_2).^(1/2) .* cos(d_phi);               % Intensity of recombined beams


%%

% % % pe = 2 * pi * (( Nrange * lambda(2000) ) + opd1) / lambda
% % pe1 = 2 .* pi .* (( 1/8 .* lambda(col_pixel_range==2000) ) + opd1) ./ lambda;
% % pe2 = 2 .* pi .* (( 2/8 .* lambda(col_pixel_range==2000) ) + opd1) ./ lambda;
% % pe = pe2 - pe1;
% % pea = 2 .* pi ./ pe;
% periods_calc = 8 * lambda / lambda(col_pixel_range==2000);
% % pha = pe1 ./ (2.*pi);
% % pha = (pha - floor(pha));
% % pha = pha .* pea;
% phases_calc = 1 + 8 ./ lambda(col_pixel_range==2000) .* (opd1 - lambda .* floor((1 ./ lambda) .* (lambda(col_pixel_range==2000) ./ 8 + opd1)));


% % Check period on measured and calculated
% figure;hold on; plot(col_pixel_range, pea, '-k'); plot(col_pixel_range, (period_px), ':m')
% figure; plot(col_pixel_range, abs(pea' - period_px))
% figure; plot(col_pixel_range, abs(mat2gray(1 ./ pe)' - mat2gray(period_px)))
% figure;hold on; plot(col_pixel_range, mat2gray(1 ./ pe), '-k'); plot(col_pixel_range, mat2gray(period_px), ':m')

% % Check phase on measured and calculated
% figure;hold on; plot(col_pixel_range, pha, '-k'); plot(col_pixel_range, (phase_px), ':m')


% x1 = row_pixel_range;
% x1HQ = row_pixel_range(1) :0.1/yUpsample: row_pixel_range(end);
% fLo = 2*pi/10;
% fHi = 2*pi/6.5;
% 
% parfor colInd = 1:length(col_pixel_range)
%     
%     y1 = (I(:,colInd));
%     
%     [a, b, c, d, ~] = sinefit(x1, y1, fLo, fHi);
%     
%     
%     phase_px(colInd,1) = c/d; % change from peak starting at col px 1.
%     period_px(colInd,1) = (2*pi)/d;
%     
% %     err = (phase_px - phaseInput_px) / phaseInput_px;
%     
% %     fit0 = a + b * sin(c + d .* x1HQ);
% %     figure(1); plot(row_pixel_range, y1, '.-k', x1HQ, fit0, '-m')
%     % figure(2); imagesc(I')
% end
% 
% figure(77); plot(col_pixel_range, phase_px, '-k')
% figure(88); plot(col_pixel_range, period_px, '-k')




%% Plotting
%
% % y = I(:, 3500);
% % x = 1:length(y);
%
% figure(1); clf(figure(1))
% subplot(211); hold on
% xr = find(col_pixel_range==450):find(col_pixel_range==550);
% imagesc(col_pixel_range(xr), row_pixel_range, I(:,xr));
% % plot(repmat(500, 1, length(origPeaks_col500_x)), origPeaks_col500_x, 'om')
% set(gca,'YDir','normal');
% colormap(gray)
% % xlim([500 600])
% subplot(212); hold on
% xr = find(col_pixel_range==3900):find(col_pixel_range==4000);
% imagesc(col_pixel_range(xr), row_pixel_range, I(:,xr));
% set(gca,'YDir','normal');
% colormap(gray)
% % xlim([3900 4001])
% % ylabel('Row # [px.]')
% % xlabel('Col # [px.]')
%
% figure(2); clf(figure(2)); hold on
% p1 = plot(row_pixel_range, I(:,1), '.-k', 'displayname', 'First col');
% p2 = plot(row_pixel_range, I(:,imgWidth), '.-r', 'displayname', 'Last col');
% legend([p1 p2])
% xlabel('Row # [px.]')
% ylabel('Intensity')
%
%


% figure(3); clf(figure(3))
% span0 = 4;
% i1 = find(col_pixel_range==1);i2 = find(col_pixel_range==span0); i20=i1:i2;
% subplot(121); hold on
% imagesc(col_pixel_range(i20), row_pixel_range(:), I(:,i20));
% set(gca,'YDir','normal');
% colormap(gray)
% subplot(122); hold on
% i1 = find(col_pixel_range==col_pixel_range(end)-span0);i2 = length(col_pixel_range); ie=i1:i2;
% imagesc(col_pixel_range(ie), row_pixel_range(:), I(:,ie));
% set(gca,'YDir','normal');
% colormap(gray)
% 
% figure(4); clf(figure(4))
% imagesc(col_pixel_range, row_pixel_range, I);
% set(gca,'YDir','normal');
% colormap(gray)



%
% % pause(.3)
% % opd2
% % alpha_y_degrees
% % end
% %%