% function syntheticImgFinal = spectrumAndCombToFinalImg_V1(...
%     spectrumImg, combImg, imgHeight, imgWidth, rowRangeWithinPad, colRangeWithinPad)
% 
% 
% if strcmp(type0, 'TIO')
%     
%     spectralLinesImg_tmp = interp1(colRangeUp_pad, spectrumImg, (colRangeUp_pad - DD(beamNum, obsNum)), 'spline');
%     
%     spectralLinesImg_tmp = repmat(spectralLinesImg_tmp, rowRangeUpInd_pad(end), 1);
%     
%     syntheticImg = combImg .* spectralLinesImg_tmp;                                    % Combine comb and spectral line images
%     
%     syntheticImg2 = gaussfiltGPU_V1(syntheticImg, sigmaBlur);                               % PSF convolution
%     
%     if plots
%         figure(23); clf(figure(23)); hold on
%         imagesc(syntheticImg2);
%         colormap(gray);
%         set(gca,'YDir','normal')
%     end
%     
%     syntheticImg2(rowRangeWithinPad, colRangeWithinPad) = mat2gray(syntheticImg2(rowRangeWithinPad, colRangeWithinPad));                    % Normalize the synth spectra
%     
%     syntheticImgFinal = imresize(syntheticImg2(rowRangeWithinPad, colRangeWithinPad), [imgHeight, imgWidth], 'cubic', 'Antialiasing', false);    % Resize the synth spectra (downsample)
%     
%     
% else
%     
%     
% end
% 
% 
