% function solveForCombCreationParams_LSQ()

clearvars

showLSQProcessPlots_q = 1;

downsample = 10;

R_LSF = 15000;


yRangeUsed = 1:16;

load('sun_WL_ang.mat', 'WL_ang')
load('E:\Research\MARVELS_software\synthSpectraMaker_2D\sun_flux_pxv.mat', 'flux_pxv')

load('wlsoln.mat', 'wlsoln')
maxWL_ang = max(max(wlsoln(:,1,:)));
minWL_ang = min(min(wlsoln(:,1,:)));

wl_lowR_m = squeeze(wlsoln(:,1,1))' .* 1e-10; clear wlsoln

dWL_sun_ang = nanmean(diff(WL_ang));

wl_hiR_m = ( floor(minWL_ang) :dWL_sun_ang * downsample: ceil(maxWL_ang) ) * 1e-10;

dWL_dPX = nanmean(diff(wl_hiR_m)) * 1e10;

% flux_sun_pxv = gpuArray(spline(WL_ang, flux_pxv, wl_hiR_m * 1e10)); clear WL_ang
flux_sun_pxv = (spline(WL_ang, flux_pxv, wl_hiR_m * 1e10)); clear WL_ang

wl_cent_ang = mean(wl_lowR_m) * 1e10;

% Create LSF
gaussianRange_x = 1:901;
mu = 450;
dl = wl_cent_ang / R_LSF / dWL_dPX;
sigma = (dl / (2 * sqrt(2 * log(2))));
psf_1d = normpdf(gaussianRange_x, mu, sigma) / 0.4;
LSF = psf_1d / sum(psf_1d); 
% figure; plot(LSF/ max(LSF))
LSF(LSF/ max(LSF) < 0.001) = [];


clear psf_1d dl mu gaussianRange_x wl_cent_ang dWL_sun_ang minWL_ang maxWL_ang R_LSF downsample flux_pxv


Y_px = 1 :0.05: yRangeUsed(end);

% Create gridded data for final spectrum downsample interpolation
[WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px] = meshgrid(wl_hiR_m, Y_px);
[WL_beam_log_gridded_m, Y_beam_log_gridded_0_px] = meshgrid(wl_lowR_m, 1:yRangeUsed(end));


% Find real spectra
files0 = dir(strcat('E:\Research\1a_syntheticPipelineTestEnv\sunSpectra\*deslant*.fits'));

spectraNames = struct2cell(files0); clear files0

spectraNames = spectraNames(1,:);



for spectraNum = 1:20
    
%     realSpectrum = gpuArray(mat2gray(fitsread(strcat('E:\Research\1a_syntheticPipelineTestEnv\sunSpectra\', spectraNames{1, spectraNum}))));
    realSpectrum = (mat2gray(fitsread(strcat('E:\Research\1a_syntheticPipelineTestEnv\sunSpectra\', spectraNames{1, spectraNum}))));
    
    realSpectrum = realSpectrum(yRangeUsed, :);
    realSpectrum(:, 50:3950) = continnumNorm(realSpectrum(:, 50:3950), 1000);
    realSpectrum(:, 60:3940) = flattenImgRows(realSpectrum(:, 60:3940), 1);
    realSpectrum(:, [(1:60), (3940:4001)]) = 0;
    
% figure(88); clf(figure(88)); imagesc(realSpectrumGPU); colormap gray
    
    
%     [smoothedProfile, ~] = getSpectraProfile(realSpectrum, 1);
%     profileTemplate = smoothedProfile; clear smoothedProfile
%     
%     realSpectrumGPU = gpuArray(realSpectrum);
%     profileTemplateGPU = gpuArray(profileTemplate);


    disp(cat(2, '******** Spectra ', num2str(spectraNum), ' of ', num2str(spectraNum)))

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Rough 1st estimates
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %% Solve for C
    
    ub = 7.0e-08;
    lb = 5.0e-08;

    fun2 = @(x) solveForCombCreationParams_func(realSpectrum, wl_hiR_m, Y_px, x(1), 0.0069, 0, ...
        WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, WL_beam_log_gridded_m, Y_beam_log_gridded_0_px, LSF, flux_sun_pxv);


    left0 = lb;
    right0 = ub;

    C_best_px = 0;
    iter = 0;

    while (right0 - left0) / 2 > 1e-10

        iter = iter + 1;
        c = 0;
        if iter == 1
            LSQinputs = linspace(left0, right0, 100);
        else
            LSQinputs = linspace(left0, right0, 21);
        end
        lsq_iter_val = zeros(1, length(LSQinputs));

        for LSQinput = LSQinputs

            c = c + 1;

            [lsq_iter_val(1, c)] = fun2(LSQinput);


            if showLSQProcessPlots_q

                lsqIteration_genericPlot(c, LSQinputs, lsq_iter_val)
            end
        end

        [~, ind] = min(lsq_iter_val);
        C_best_px = LSQinputs(ind);


        try
            left0 = LSQinputs(ind - 1);
        catch
            left0 = LSQinputs(ind);
        end

        try
            right0 = LSQinputs(ind + 1);
        catch
            right0 = LSQinputs(ind);
        end
    end

    
    
    
    
     %% Solve for tau

    
    ub = 0.015;
    lb = 0.005;

    fun2 = @(x) solveForCombCreationParams_func(realSpectrum, gpuArray(wl_hiR_m), Y_px, C_best_px, x(1), 0, ...
        WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, WL_beam_log_gridded_m, Y_beam_log_gridded_0_px, LSF, flux_sun_pxv);


    left0 = lb;
    right0 = ub;

    tau_best_px = 0;
    iter = 0;

    while (right0 - left0) / 2 > 1e-5

        iter = iter + 1;
        c = 0;
        if iter == 1
            LSQinputs = linspace(left0, right0, 100);
        else
            LSQinputs = linspace(left0, right0, 21);
        end
        lsq_iter_val = zeros(1, length(LSQinputs));

        for LSQinput = LSQinputs

            c = c + 1;

            [lsq_iter_val(1, c)] = fun2(LSQinput);


            if showLSQProcessPlots_q

                lsqIteration_genericPlot(c, LSQinputs, lsq_iter_val)
            end
        end

        [~, ind] = min(lsq_iter_val);
        tau_best_px = LSQinputs(ind);


        try
            left0 = LSQinputs(ind - 1);
        catch
            left0 = LSQinputs(ind);
        end

        try
            right0 = LSQinputs(ind + 1);
        catch
            right0 = LSQinputs(ind);
        end
    end
    
    
    
    
    
    
    %% Solve for WL offset

    %   C     tau    wl_offset
%     ub = [8.0e-08, 0.02, 100e-10];
%     lb = [3.0e-08, 0.01, -100e-10];
    
    ub = 1e-9;
    lb = -1e-9;

    fun2 = @(x) solveForCombCreationParams_func(realSpectrum, gpuArray(wl_hiR_m), Y_px, C_best_px, tau_best_px, x(1), ...
        WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, WL_beam_log_gridded_m, Y_beam_log_gridded_0_px, LSF, flux_sun_pxv);


    left0 = lb;
    right0 = ub;

    wl_offset_best_m = 0;
    iter = 0;

    while (right0 - left0) / 2 > 1e-12

        iter = iter + 1;
        c = 0;
        if iter == 1
            LSQinputs = linspace(left0, right0, 500);
        else
            LSQinputs = linspace(left0, right0, 21);
        end
        lsq_iter_val = zeros(1, length(LSQinputs));

        for LSQinput = LSQinputs

            c = c + 1;

            [lsq_iter_val(1, c)] = fun2(LSQinput);


            if showLSQProcessPlots_q

                lsqIteration_genericPlot(c, LSQinputs, lsq_iter_val)
            end
        end

        [~, ind] = min(lsq_iter_val);
        wl_offset_best_m = LSQinputs(ind);


        try
            left0 = LSQinputs(ind - 1);
        catch
            left0 = LSQinputs(ind);
        end

        try
            right0 = LSQinputs(ind + 1);
        catch
            right0 = LSQinputs(ind);
        end
    end
    
    save('E:\Research\1a_syntheticPipelineTestEnv\beam1_CombParams_intermediate.mat', 'C_best_px', 'tau_best_px', 'wl_offset_best_m')
    
    
    % Create the comb
    combImg = createComb(gpuArray(wl_hiR_m), Y_px, C_best_px, tau_best_px, wl_offset_best_m, 0);
    
    % Multiply the comb and sun spectrum
    synthSpectrum = mat2gray(combImg) .* repmat(flux_sun_pxv, size(combImg, 1), 1);
    
    % Convolve synth spectra with the PSF
    flux_hiR_ds_lsf_adu = conv2(synthSpectrum, LSF, 'same');
    
    
    % Create gridded data for final spectrum downsample interpolation
    % star_spectrum_log_deslanted = gpuArray(zeros(26, 4001));
    star_spectrum_log_deslanted = (zeros(26, 4001));
    star_spectrum_log_deslanted = interp2( ...
        WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, flux_hiR_ds_lsf_adu, ...
        WL_beam_log_gridded_m, Y_beam_log_gridded_0_px, 'linear', 0);
    
    
    figure(78444); clf(figure(78444)); imagesc([mat2gray(realSpectrum(yRangeUsed, 300:3700)) ; (mat2gray([star_spectrum_log_deslanted(yRangeUsed, 300:3700)])) ;  (mat2gray(realSpectrum(yRangeUsed, 300:3700)) - (mat2gray([star_spectrum_log_deslanted(yRangeUsed, 300:3700)])))]); colormap gray

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 2nd pass - High accuracy estimates
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    load('E:\Research\1a_syntheticPipelineTestEnv\beam1_CombParams_intermediate.mat', 'C_best_px', 'tau_best_px', 'wl_offset_best_m')
    
    %% Solve for C
    
%     ub = C_best_px + C_best_px / 10;
%     lb = C_best_px - C_best_px / 10;

    ub = 9.0e-08;
    lb = 5.0e-08;

    fun2 = @(x) solveForCombCreationParams_func(realSpectrum, gpuArray(wl_hiR_m), Y_px, x(1), tau_best_px, wl_offset_best_m, ...
        WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, WL_beam_log_gridded_m, Y_beam_log_gridded_0_px, LSF, flux_sun_pxv);


    left0 = lb;
    right0 = ub;

    C_best_px2 = 0;
    iter = 0;

    while (right0 - left0) / 2 > 1e-12

        iter = iter + 1;
        c = 0;
        if iter == 1
            LSQinputs = linspace(left0, right0, 101);
        else
            LSQinputs = linspace(left0, right0, 21);
        end
        lsq_iter_val = zeros(1, length(LSQinputs));

        for LSQinput = LSQinputs

            c = c + 1;

            [lsq_iter_val(1, c)] = fun2(LSQinput);


            if showLSQProcessPlots_q

                lsqIteration_genericPlot(c, LSQinputs, lsq_iter_val)
            end
        end

        [~, ind] = min(lsq_iter_val);
        C_best_px2 = LSQinputs(ind);


        try
            left0 = LSQinputs(ind - 1);
        catch
            left0 = LSQinputs(ind);
        end

        try
            right0 = LSQinputs(ind + 1);
        catch
            right0 = LSQinputs(ind);
        end
    end

    
    
    
    
     %% Solve for tau
    
%     ub = tau_best_px + tau_best_px / 10;
%     lb = tau_best_px -  tau_best_px / 10;

    ub = tau_best_px + 0.0015;
    lb = tau_best_px - 0.0015;
    
    fun2 = @(x) solveForCombCreationParams_func(realSpectrum, gpuArray(wl_hiR_m), Y_px, C_best_px2, x(1), wl_offset_best_m, ...
        WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, WL_beam_log_gridded_m, Y_beam_log_gridded_0_px, LSF, flux_sun_pxv);


    left0 = lb;
    right0 = ub;

    tau_best_px2 = 0;
    iter = 0;

    while (right0 - left0) / 2 > 0.000001

        iter = iter + 1;
        c = 0;
        if iter == 1
            LSQinputs = linspace(left0, right0, 100);
        else
            LSQinputs = linspace(left0, right0, 50);
        end
        lsq_iter_val = zeros(1, length(LSQinputs));

        for LSQinput = LSQinputs

            c = c + 1;

            [lsq_iter_val(1, c)] = fun2(LSQinput);


            if showLSQProcessPlots_q

                lsqIteration_genericPlot(c, LSQinputs, lsq_iter_val)
            end
        end

        [~, ind] = min(lsq_iter_val);
        tau_best_px2 = LSQinputs(ind);


        try
            left0 = LSQinputs(ind - length(LSQinputs) / 10);
        catch
            left0 = LSQinputs(1);
        end

        try
            right0 = LSQinputs(ind + length(LSQinputs) / 10);
        catch
            right0 = LSQinputs(end);
        end
    end
    
    
    
    
    
    
    %% Solve for WL offset

    %   C     tau    wl_offset
%     ub = [8.0e-08, 0.02, 100e-10];
%     lb = [3.0e-08, 0.01, -100e-10];
    
    ub = wl_offset_best_m + abs(wl_offset_best_m / 2);
    lb = wl_offset_best_m - abs(wl_offset_best_m / 2);

    fun2 = @(x) solveForCombCreationParams_func(realSpectrum, gpuArray(wl_hiR_m), Y_px, C_best_px2, tau_best_px2, x(1), ...
        WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, WL_beam_log_gridded_m, Y_beam_log_gridded_0_px, LSF, flux_sun_pxv);


    left0 = lb;
    right0 = ub;

    wl_offset_best_m2 = 0;
    iter = 0;

    while (right0 - left0) / 2 > 1e-14

        iter = iter + 1;
        c = 0;
        if iter == 1
            LSQinputs = linspace(left0, right0, 100);
        else
            LSQinputs = linspace(left0, right0, 50);
        end
        lsq_iter_val = zeros(1, length(LSQinputs));

        for LSQinput = LSQinputs

            c = c + 1;

            [lsq_iter_val(1, c)] = fun2(LSQinput);


            if showLSQProcessPlots_q

                lsqIteration_genericPlot(c, LSQinputs, lsq_iter_val)
            end
        end

        [~, ind] = min(lsq_iter_val);
        wl_offset_best_m2 = LSQinputs(ind);

        
        try
            left0 = LSQinputs(ind - length(LSQinputs) / 10);
        catch
            left0 = LSQinputs(1);
        end

        try
            right0 = LSQinputs(ind + length(LSQinputs) / 10);
        catch
            right0 = LSQinputs(end);
        end
    end
    

end





save('E:\Research\1a_syntheticPipelineTestEnv\beam1_CombParams.mat', 'C_best_px2', 'tau_best_px2', 'wl_offset_best_m2')
    



% Create the comb
combImg = createComb(gpuArray(wl_hiR_m), Y_px, C_best_px2, tau_best_px2, wl_offset_best_m2, 0);

% Multiply the comb and sun spectrum
synthSpectrum = mat2gray(combImg) .* repmat(flux_sun_pxv, size(combImg, 1), 1);

% Convolve synth spectra with the PSF
flux_hiR_ds_lsf_adu = conv2(synthSpectrum, LSF, 'same');


% Create gridded data for final spectrum downsample interpolation
% star_spectrum_log_deslanted = gpuArray(zeros(26, 4001));
star_spectrum_log_deslanted = (zeros(26, 4001));
star_spectrum_log_deslanted = interp2( ...
    WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, flux_hiR_ds_lsf_adu, ...
    WL_beam_log_gridded_m, Y_beam_log_gridded_0_px, 'linear', 0);


figure(78444); clf(figure(78444)); imagesc([mat2gray(realSpectrum(yRangeUsed, 300:3700)) ; (mat2gray([star_spectrum_log_deslanted(yRangeUsed, 300:3700)])) ;  (mat2gray(realSpectrum(yRangeUsed, 300:3700)) - (mat2gray([star_spectrum_log_deslanted(yRangeUsed, 300:3700)])))]); colormap gray




% Calc and show correlation along spectra

for colNum = 300:3701
    
    corr0(1, colNum) = sum(abs(mat2gray(realSpectrum(:, colNum)) - mat2gray(star_spectrum_log_deslanted(:, colNum))));
end

corr0 = gather(mat2gray(corr0));

figure(780); clf(figure(780)); hold on; 
plot(300:3701, corr0(300:3701), '-k');
plot(300:3701, medfilt1(corr0(300:3701), 50), '-m');




