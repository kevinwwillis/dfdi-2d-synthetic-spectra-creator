function [synthImg, wl_instDrift_m, dWL_DopplerShift_m, totalShift_px] = createSingleSynthSTARspectra_V1(...
    ...
    X_px, Y_px, dX_instDrift_px, RV_mps, BCV_mps, wl_m, spectralLineCenters_px, spectralLineIntensities, fringePeriod_px, plots)

% create a star spectra synthetically

clearvars

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% yPadding = 10;                      % Pad images for proper PSF convolution

% xPadding = 10;                      % Pad images for proper PSF convolution

yUpsample_px = 10;                     % Upsample for spectra height

xUpsample_px = 10;                     % Upsample for spectra width

combOpacityLow = 0;                 % Choose opacity lower limit of the synth comb

combOpacityHigh = 1;                % Choose opacity upper limit of the synth comb

% slOpacityLow = 0;                   % Choose opacity lower limit of the synth spectral lines
% 
% slOpacityHigh = 1;                  % Choose opacity upper limit of the synth spectral lines

combEndLineSep_px = 1.6;               % Comb line separation at px 4000

sigmaBlur = 3 * round(mean([xUpsample_px, yUpsample_px]));


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

OS=computer; if strcmp(OS,'PCWIN64') || strcmp(OS,'PCWIN'); s='\'; else s='/'; end

% Define padding for images
pad_px = sigmaBlur * 2;

% Define the speed of light
%c_SOL = 2.99792458e8; % m/s vacuum
c_SOL_mps = 2.99705e8; % m/s air


%% y-axis spatial data

imgHeight_px = Y_px(end);

% Padded
Y_pad_px = 1 - pad_px/xUpsample_px : Y_px(end) + pad_px/xUpsample_px;

% Upsampled and padded
Y_up_pad_px = Y_pad_px(1) :1/xUpsample_px: Y_pad_px(end);
Y_up_pad_ind = 1:length(Y_up_pad_px);

Y_withinPad_ind = pad_px : length(Y_up_pad_px) - (pad_px - 1);

%% x-axis spatial data

imgWidth_px = X_px(end);

% Padded
X_pad_px = 1 - pad_px/xUpsample_px : X_px(end) + pad_px/xUpsample_px;

% Upsampled and padded
X_up_pad_px = X_pad_px(1) :1/xUpsample_px: X_pad_px(end);

X_WithinPad_ind = pad_px : length(X_up_pad_px) - (pad_px - 1);

%%

RV_BCV_mps = RV_mps + BCV_mps;

% Create px to WL conversion scale
xToWl_pp = fit(X_px', squeeze(wl_m(:, beamNum, 1)), 'spline');
%wlToCol_pp = fit(squeeze(wl(:, beamNum, 1)), X_', 'spline');

% Create model spectra
spectraImg_up_pad_1D = createSyntheticSpectralLines1D_V1(X_up_pad_px, spectralLineCenters_px, spectralLineIntensities);

% Find wl spacing
dWL_m = diff(wl_m);

% calc d(wl)/d(px) conversion
dPXdWL_m = 1 ./ [dWL_m(1); dWL_m];

% Calc WL shift from doppler formula. Only use these to find pixel shift.
dWL_DopplerShift_m = (RV_BCV_mps / c_SOL_mps) * wl_m;

% Calc pixel shift from doppler effect
dX_DopplerShift_px = dWL_DopplerShift_m .* dPXdWL_m;

% Calc total pixel shift from doppler effect and instrument drift
dX_DopplerAndInstShift_px = -(dX_DopplerShift_px + dX_instDrift_px);

% calc upsampled and padded range pixel shifts
dX_up_pad_DopplerShift_instDrift_px = interp1(X_px, dX_DopplerAndInstShift_px, X_up_pad_px);

% calc new wavelengths due to instrument drift only.
wl_instDrift_m = feval(xToWl_pp, X_px - dX_instDrift_px(beamNum, obsNum));

% calc upsampled and padded range WL shifts
wl_up_pad_instDrift_m = interp1(X_px, wl_instDrift_m, X_up_pad_px, 'pchip');

% Shift spectra by doppler and instrument shifts
spectraImg_up_pad_1D_DopplerAndInstShift = interp1(X_up_pad_px, spectraImg_up_pad_1D, (X_up_pad_px + dX_up_pad_DopplerShift_instDrift_px), 'spline');

% Replicate rows to create 2D spectra
spectraImg_up_pad_2D_DopplerAndInstShift = repmat(spectraImg_up_pad_1D_DopplerAndInstShift, Y_up_pad_ind(end), 1);

% Create comb image
combImg_up_pad = createSyntheticComb_V6(X_px, Y_px, X_up_pad_px, Y_up_pad_px, wl_up_pad_instDrift_m, fringePeriod_px, combEndLineSep_px);

% Normalize the comb image to desired range
combImg_up_pad = normScale(combImg_up_pad, combOpacityLow, combOpacityHigh);

% Combine the comb and spectra images
synthImg_up_pad_raw = combImg_up_pad .* spectraImg_up_pad_2D_DopplerAndInstShift;

% Convolve synth spectra with the PSF
synthImg_up_pad_PSF = gaussfiltGPU_V1(synthImg_up_pad_raw, sigmaBlur);

% Normalize the synth spectra and remove padding
synthImg_up_pad_PSF = mat2gray(synthImg_up_pad_PSF(Y_withinPad_ind, X_WithinPad_ind));

% Resize the synth spectra (downsample)
synthImg = imresize(synthImg_up_pad_PSF, [imgHeight_px, imgWidth_px], 'cubic', 'Antialiasing', false);

if plots
    if obsNum == 1
        syntheticImg_1 = synthImg;
    end
    
    figure(1); clf(figure(1)); hold on
    C00 = imfuse(syntheticImg_1, synthImg, 'falsecolor','Scaling','joint','ColorChannels',[1 2 0]);
    imagesc(C00);
    colormap(gray);
    set(gca,'YDir','normal')
    
    figure(23); clf(figure(23)); hold on
    imagesc(synthImg);
    colormap(gray(900000));
    set(gca,'YDir','normal')
    
    figure(11); clf(figure(11));
    imagesc(X_up_pad_px, Y_up_pad_px, combImg_up_pad);
    colormap(gray);
    set(gca,'YDir','normal')
    
    figure(12); clf(figure(12));
    imagesc(X_up_pad_px, Y_up_pad_px, spectraImg_up_pad_1D_DopplerAndInstShift);
    colormap(gray);
    set(gca,'YDir','normal')
    
    pause(0.1);
end

% Calcs for saved variables
totalShift_px = dX_DopplerAndInstShift_px;

% for chunkNum = 1:34
%     chunkRange = 301 + 100 * (chunkNum - 1) : 301 + 100 * chunkNum;
%     totalShift_chunks_px = mean(dX_DopplerAndInstShift_px(chunkRange));
% end




