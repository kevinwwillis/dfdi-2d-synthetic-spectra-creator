function [] = createSynthStarspectrum()



if addDispersion_q == 1 & useFullBeamDispersion_q == 1 & applyStar_dispersion_q == 1
    
    %       dWL_dipersionRespampled_star_ang = spline(dispersionPlate_wlsoln(5, :), dWL_dispersion_star_ang(obsNum, :), wl_beams_ang(1, :));
    
    
    % Create dispersed WL final sample array
    %       WL_beam_log_gridded_dispersed_star_m = log( exp(WL_beam_log_gridded_m) + repmat(dWL_dipersionRespampled_star_ang * 1e-10, Y_beam_px(end), 1) );
    WL_beam_log_gridded_dispersed_star_m = log( exp(WL_beam_log_gridded_star_m) + repmat(dWL_dispersion_star_m(obsNum, :), Y_beam_px(end), 1) );
    
    figure(81); clf(figure(81)); hold on;
    titlel(cat(2, {cat(2, 'Star Dispersion')}, {cat(2, 'Plate: ', underscoreTEXfixer(plateName), ' | Beam: ', beamNumStr, ' | Obs: ', obsNumStr)}))
    plot(wl_beams_m(beamNum, :) * 1e10, (exp(WL_beam_log_gridded_dispersed_star_m(1, :)) - exp(WL_beam_log_gridded_star_m(1, :))) * 1e10, '-k');
    xlabel('$\lambda$ [\AA]')
    ylabel('d$\lambda$ [\AA]')
    grid on
    hold off
    
    plot_dispersion_dir = strcat(pipeline_dir, s, 'plates', s, plateName, s, 'plots', s, 'synthSpectrumCreationPlots', s, 'dispersion');
    try mkdir(plot_dispersion_dir); catch; end
    print(gcf, '-r0', '-dpng', strcat(plot_dispersion_dir, s, 'star_beam', beamNumStr, '_obs', num2str(obsNum), '_dispersionPlot.png'))
end


% Calc WL shift from doppler formula. Only use these to find pixel shift.
if RV_BCV_mps(obsNum) == 0
    wl_hiR_wide_log_ds_star_m = wl_hiR_wide_log_star_m;
    
    wl_beam_log_ds_star_m = wl_beam_log_star_m;
    
else
    wl_hiR_wide_log_ds_star_m = wl_hiR_wide_log_star_m + log(1 + RV_BCV_mps(obsNum) / c_mps);
    
    wl_beam_log_ds_star_m = wl_beam_log_star_m + log(1 + RV_BCV_mps(obsNum) / c_mps);
end


% Shift spectra by resampling the spectrum at the doppler shifted wavelengths. (** Wide WL range eliminated here! **)
flux_hiR_log_ds_star_adu = interp1(wl_hiR_wide_log_ds_star_m, flux_hiR_wide_star_adu, wl_hiR_log_comb_m, 'spline', nanmean(flux_hiR_wide_star_adu));


% Apply TIO instrument drifts
if dX_instDrift_px(beamNum, obsNum) ~= 0
    flux_hiR_log_ds_id_star_adu = spline(X_hiR_star_px, flux_hiR_log_ds_star_adu, X_hiR_star_px + dX_instDrift_px(beamNum, obsNum));
else
    flux_hiR_log_ds_id_star_adu = flux_hiR_log_ds_star_adu;
end


% Apply atmospheric turbulance blurring
if applyAtmTurbBlur_q
    
    % If obsNum is not = 1 and there was is no instrument drift in all observations, we can resuse the original solution here by skipping.
    flux_hiR_log_ds_id_atmT_star_adu = convSV_LSF(flux_hiR_log_ds_id_star_adu, PSF_AtmTurb_chunks);
else
    flux_hiR_log_ds_id_atmT_star_adu = flux_hiR_log_ds_id_star_adu;
end


% Replicate rows to create 2D spectra
flux_hiR_log_ds_id_atmT_2D_star_adu = repmat(flux_hiR_log_ds_id_atmT_star_adu, size(flux_hiR_comb_adu, 1), 1);


% Combine the comb and spectra images
flux_hiR_log_ds_id_atmT_comb_star_adu = flux_hiR_comb_adu .* flux_hiR_log_ds_id_atmT_2D_star_adu;


% Convolve synth spectra with the LSF
if applyVariableLSF_q
    
    flux_hiR_log_ds_id_atmT_comb_LSF_star_adu = convSV_LSF(flux_hiR_log_ds_id_atmT_comb_star_adu, LSF_inst_chunks);
    
else
    flux_hiR_log_ds_id_atmT_comb_LSF_star_adu = conv2(flux_hiR_log_ds_id_atmT_comb_star_adu, LSF_inst_invariable_star, 'same');
end

if GPU_calulations_q
    flux_hiR_log_ds_id_atmT_comb_star_adu = gather(flux_hiR_log_ds_id_atmT_comb_star_adu);
    flux_hiR_log_ds_id_atmT_comb_LSF_star_adu = gather(flux_hiR_log_ds_id_atmT_comb_LSF_star_adu);
end


% Downsample spectrum
if addDispersion_q == 1 & applyStar_dispersion_q == 1
    
    if binningDownsample_q == 1
        flux_log_deslanted_star_adu = binImgResizeWL_V2(flux_hiR_log_ds_id_atmT_comb_LSF_star_adu, Y_px, WL_beam_log_gridded_dispersed_star_m(1, :), WL_hiR_log_gridded_comb_m(1, :));
    else
        flux_log_deslanted_star_adu = interp2( ...
            WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, flux_hiR_log_ds_id_atmT_comb_LSF_star_adu, ...
            WL_beam_log_gridded_dispersed_star_m, Y_beam_log_gridded_0_px, 'spline', nanmean(flux_hiR_log_ds_id_atmT_comb_LSF_star_adu(:)));
    end
    
else
    if binningDownsample_q == 1
        flux_log_deslanted_star_adu = binImgResizeWL_V2(flux_hiR_log_ds_id_atmT_comb_LSF_star_adu, Y_px, WL_beam_log_gridded_star_m(1, :), WL_hiR_log_gridded_comb_m(1, :));
    else
        flux_log_deslanted_star_adu = interp2( ...
            WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, flux_hiR_log_ds_id_atmT_comb_LSF_star_adu, ...
            WL_beam_log_gridded_star_m, Y_beam_log_gridded_0_px, 'spline', nanmean(flux_hiR_log_ds_id_atmT_comb_LSF_star_adu(:)));
    end
end


% COnform to MARVELS illumination profile
if matchMARVELSprofile_q
    flux_log_deslanted_star_adu = profileTemplate .* normScale(flux_log_deslanted_star_adu, 0.1, 1);
end


% Add noise to the image
if addNoise_q
    flux_log_deslanted_star_adu = imnoise(flux_log_deslanted_star_adu .* noiseLevel / 1e12, 'poisson') .* 1e12 / noiseLevel;
end


% Create spectra save name
fitsSavePath_star = strcat(star_spectra_deslanted_orig_dir, s, 'star_beam', beamNumStr, '_obs', obsNumStr, spectrum_deslanted_fileName, '.fits');
matSavePath_star = strcat(star_spectra_deslanted_orig_dir, s, 'star_beam', beamNumStr, '_obs', obsNumStr, spectrum_deslanted_fileName, '.mat');


% Save the spectra
fitswrite(flux_log_deslanted_star_adu, fitsSavePath_star)
flux_deslanted_adu = flux_log_deslanted_star_adu;
save(matSavePath_star, 'flux_deslanted_adu')



%% Star Plots

%     % Continuum plots
%     synthContinuumPlots_logspace(s, synthPipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
%       wl_beam_log_star_m, wl_hiR_wide_log_star_m, wl_hiR_log_comb_m, ...
%       flux_hiR_wide_star_adu, flux_hiR_wide_star_adu, flux_hiR_log_ds_star_adu, flux_hiR_log_ds_id_atmT_comb_star_adu, flux_hiR_log_ds_id_atmT_comb_LSF_star_adu, flux_log_deslanted_star_adu, 'Star')
%
%     % Image plots
%     synthImagePlots_logspace(s, synthPipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
%       wl_beam_log_star_m, wl_hiR_wide_log_star_m, wl_hiR_log_comb_m, ...
%       flux_hiR_wide_star_adu, flux_hiR_wide_star_adu, flux_hiR_log_ds_star_adu, flux_hiR_log_ds_id_atmT_comb_star_adu, flux_hiR_log_ds_id_atmT_comb_LSF_star_adu, flux_log_deslanted_star_adu, 'Star')
%
%     % Final spectrum continuum plot
%     synthFinalContinuumPlot_logspace(s, synthPipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
%       wl_beam_log_star_m, flux_log_deslanted_star_adu, 'Star')
%
%
%     realAndSynthComparison(s, synthPipeline_dir, remotePipeline_dir, plateName, beamNumStr, obsNum, flux_log_deslanted_star_adu, 1, 'Star', 0, plateEmulationName, templateBeam)
