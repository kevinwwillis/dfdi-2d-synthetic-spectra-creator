function [wl_ang, plateName, BCV_beam_mps, RV_BCV_mps, dX_id_px] = createSynthParams(...
    s, pipeline_orig_dir, pipeline_dir, beamNum, orbitPeriod_day, maxRV_mps, RV_mps, origObsCount, obsNums, modelObsNum, ...
    BCVmax_mps, spectraQuantity, surveyDaySpan_day, plateName, makeID_q, makeBCV_q, linearWLsol_q)




%% Create directory structure and save directory paths as variables
MARVELS_directories(s, pipeline_dir, plateName, '');


%% Load needed directory names
load(strcat(pipeline_dir, s, 'plates', s, plateName, s, plateName, '_directories.mat'))


%% Load and set the wavelengths for these spectra
load('wlsoln.mat', 'wlsoln')
wl_ang = squeeze(wlsoln(:, 1, beamNum))';

if linearWLsol_q
    wl_ang = linspace(wl_ang(1), wl_ang(end), 4001);
end


%% Create model definition array and save
if 0 == exist(strcat(general_dir, s, 'model.mat'), 'file')
    model = nan(1, 120);
else
    load(strcat(general_dir, s, 'model.mat'), 'model')
end
model(1, beamNum) = modelObsNum;
save(strcat(general_dir, s, 'model.mat'), 'model')


%% Copy over a required file from real results
magFile = strcat(pipeline_orig_dir, s, '47UMA', s, 'results', s, 'mag.mat');
destMagFile2 = strcat(general_dir, s, 'mag.mat');
copyfile(magFile, destMagFile2);



%% Create and save Barycentric velocity data
if 0 == exist(strcat(general_dir, s, 'BCV_mps.mat'), 'file')
    BCV_mps = nan(120, origObsCount);
else
    load(strcat(general_dir, s, 'BCV_mps.mat'), 'BCV_mps')

    if size(BCV_mps, 1) == 1
        BCV_mps = nan(120, origObsCount);
    end
end
BCV_beam_mps = BCV_mps(beamNum, :);


if makeBCV_q
    BCV_beam_mps(obsNums) = -BCVmax_mps + 2 * BCVmax_mps * rand(1, length(obsNums));
    BCV_beam_mps(1) = 0;
else
    BCV_beam_mps(obsNums) = 0;
end

BCV_mps(beamNum, :) = BCV_beam_mps;
save(strcat(general_dir, s, 'BCV_mps.mat'), 'BCV_mps')


%% Create and save observation date data

% obsDate_MJD = sort(randperm(surveyDaySpan_day, spectraQuantity), 'ascend');
% obsDate_MJD = (orbitPeriod_day - obsDate_MJD(1)) + obsDate_MJD;
% MJDobs = obsDate_MJD;

MJDobs = linspace(1, surveyDaySpan_day, origObsCount);
obsDate_MJD = MJDobs;
% if 0 == exist(strcat(general_dir, s, 'MJDobs.mat'), 'file')
%     MJDobs = nan(1, origObsCount);
% else
%     load(strcat(general_dir, s, 'MJDobs.mat'), 'MJDobs')
% end
% BCV_mps(beamNum, 1:length(BCV_beam_mps)) = BCV_beam_mps;
save(strcat(general_dir, s, 'MJDobs.mat'), 'MJDobs')


%% Load precalculated spectral line position and intensity data
% % load(strcat(oneDpipelineDir, s, basisPlate, s, 'wlsoln', s, 'intermediate', s, 'STAR_linelist.mat'), 'CENTERS')
% load(strcat(synthPipelineDir, s, 'lineCenters_HIP14810_STAR.mat'), 'spectraLinePos_px')
% spectralLineCenters_px_star = spectraLinePos_px;
% load(cat(2, synthPipelineDir, s, 'lineIntensitiesAndModels_HIP14810_STAR.mat'), 'model', 'lineIntensities')
% lineIntensities_star = lineIntensities;

% spectralLineCenters_px_star_orig = sort(randperm(4001, 400), 'ascend');
% spectralLineCenters_px_star_orig = 10 :20: 3991;
% lineIntensities_star_orig = ones(1, length(spectralLineCenters_m_star_orig));

% load('E:\Research\MARVELS_software\synthSpectraMaker_2D\sun_WL_ang.mat', 'WL_ang')
% wl_sun_m = WL_ang' * 1e-10;
% load('E:\Research\MARVELS_software\synthSpectraMaker_2D\sun_flux_pxv.mat', 'flux_pxv')
% flux_sun_pxv = flux_pxv';
% 
% % Hi-res sun data WL granularity
% dWL_dPX = 0.002;


% Create instrument drift data
if makeID_q
    dX_id_px = normrnd(0, 0.2, 1, spectraQuantity);
    dX_id_px(:,1) = 0;
else
    dX_id_px = zeros(1, spectraQuantity);
end

% % Load precalculated spectral line position and intensity data
% % load(strcat(oneDpipelineDir, s, basisPlate, s, 'wlsoln', s, 'intermediate', s, 'TIO_linelist.mat'), 'CENTERS')
% load(strcat(synthPipelineDir, s, 'lineCenters_HIP14810_TIO.mat'), 'spectraLinePos_px')
% spectralLineCenters_px_tio = spectraLinePos_px; clear spectraLinePos_px
% load(cat(2, synthPipelineDir, s, 'lineIntensitiesAndModels_HIP14810_TIO.mat'), 'model', 'lineIntensities')
% lineIntensities_tio = lineIntensities;


%% Create and save radial velocity data
if isempty(RV_mps)
  RV_mps = maxRV_mps * sin(2 * pi / orbitPeriod_day * obsDate_MJD);
end
RV_BCV_mps = RV_mps + BCV_beam_mps;

