function [] = createSynthTIOspectrum()




%% TIO

%     if obsNum ~= 1 & sum(dX_instDrift_px(:)) == 0 & addDispersion_q == 0 & useFullBeamDispersion_q == 0
%
%       % do nothing. Resave first synth spectra since they are the same.
%     else

if addDispersion_q == 1 & useFullBeamDispersion_q == 1 & applyTiO_dispersion_q == 1
    
    %         dWL_dipersionRespampled_tio_ang = spline(dispersionPlate_wlsoln(5, :), dWL_dispersion_tio_ang(obsNum, :), wl_beams_ang(1, :));
    
    
    % Create dispersed WL final sample array
    %WL_beam_log_gridded_dispersed_m = log( exp(WL_beam_log_gridded_m) + repmat(dWL_dispersion_m(obsNum, :), Y_beam_px(end), 1) );
    %         WL_beam_log_gridded_dispersed_tio_m = log( exp(WL_beam_log_gridded_m) + repmat(dWL_dipersionRespampled_tio_ang * 1e-10, Y_beam_px(end), 1) );
    WL_beam_log_gridded_dispersed_tio_m = log( exp(WL_beam_log_gridded_tio_m) + repmat(dWL_dispersion_tio_m(obsNum, :), Y_beam_px(end), 1) );
    
    
    figure(80); clf(figure(80)); hold on;
    titlel(cat(2, {cat(2, 'TiO Dispersion')}, {cat(2, 'Plate: ', underscoreTEXfixer(plateName), ' | Beam: ', beamNumStr, ' | Obs: ', obsNumStr)}))
    plot(wl_beams_tio_m(beamNum, :) * 1e10, (exp(WL_beam_log_gridded_dispersed_tio_m(1, :)) - exp(WL_beam_log_gridded_tio_m(1, :))) * 1e10, '-k');
    xlabel('$\lambda$ [\AA]')
    ylabel('d$\lambda$ [\AA]')
    grid on
    hold off
    
    plot_dispersion_dir = strcat(pipeline_dir, s, 'plates', s, plateName, s, 'plots', s, 'synthSpectrumCreationPlots', s, 'dispersion');
    try mkdir(plot_dispersion_dir); catch; end
    print(gcf, '-r0', '-dpng', strcat(plot_dispersion_dir, s, 'TiO_beam', beamNumStr, '_obs', num2str(obsNum), '_dispersionPlot.png'))
end


% Apply TIO instrument drifts
if dX_instDrift_px(beamNum, obsNum) ~= 0
    flux_hiR_wide_id_tio_adu = spline(X_hiR_wide_tio_px, flux_hiR_wide_tio_adu, X_hiR_wide_tio_px + dX_instDrift_px(beamNum, obsNum));
else
    flux_hiR_wide_id_tio_adu = flux_hiR_wide_tio_adu;
end


% Shift spectra by resampling the spectrum at the doppler shifted wavelengths. (** Wide WL range eliminated here! **)
flux_hiR_log_id_tio_adu = interp1(wl_hiR_wide_log_tio_m, flux_hiR_wide_id_tio_adu, wl_hiR_log_comb_m, 'spline', nanmean(flux_hiR_wide_id_tio_adu));


% Replicate the 1D spectra to create a 2D spectra
flux_hiR_log_id_2D_tio_adu = repmat(flux_hiR_log_id_tio_adu, size(flux_hiR_comb_adu, 1), 1);


% Combine comb and spectral line images
flux_hiR_log_id_2D_comb_tio_adu = flux_hiR_comb_adu .* flux_hiR_log_id_2D_tio_adu;


% Convolve synth spectra with the PSF
if applyVariableLSF_q
    
    flux_hiR_log_id_2D_comb_LSF_tio_adu = convSV_LSF(flux_hiR_log_id_2D_comb_tio_adu, LSF_inst_chunks);
else
    flux_hiR_log_id_2D_comb_LSF_tio_adu = conv2(flux_hiR_log_id_2D_comb_tio_adu, LSF_inst_invariable_tio, 'same');
end

if GPU_calulations_q
    flux_hiR_log_id_2D_comb_tio_adu = gather(flux_hiR_log_id_2D_comb_tio_adu);
    flux_hiR_log_id_2D_comb_LSF_tio_adu = gather(flux_hiR_log_id_2D_comb_LSF_tio_adu);
end

% Downsample spectrum
if addDispersion_q == 1 & applyTiO_dispersion_q == 1
    
    if binningDownsample_q == 1
        flux_log_deslanted_tio_adu = binImgResizeWL_V2(flux_hiR_log_ds_id_atmT_comb_LSF_tio_adu, Y_px, WL_beam_log_gridded_dispersed_tio_m(1, :), WL_hiR_log_gridded_comb_m(1, :));
    else
        flux_log_deslanted_tio_adu = interp2( ...
            WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, flux_hiR_log_id_2D_comb_LSF_tio_adu, ...
            WL_beam_log_gridded_dispersed_tio_m, Y_beam_log_gridded_0_px, 'spline', nanmean(flux_hiR_log_id_2D_comb_LSF_tio_adu(:)));
    end
    
else
    if binningDownsample_q == 1
        flux_log_deslanted_tio_adu = binImgResizeWL_V2(flux_hiR_log_ds_id_atmT_comb_LSF_star_adu, Y_px, WL_beam_log_gridded_tio_m(1, :), WL_hiR_log_gridded_comb_m(1, :));
    else
        flux_log_deslanted_tio_adu = interp2( ...
            WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, flux_hiR_log_id_2D_comb_LSF_tio_adu, ...
            WL_beam_log_gridded_tio_m, Y_beam_log_gridded_0_px, 'spline', nanmean(flux_hiR_log_id_2D_comb_LSF_tio_adu(:)));
    end
end


% Conform to MARVELS illumination profile
if matchMARVELSprofile_q
    flux_log_deslanted_tio_adu = profileTemplate .* normScale(flux_log_deslanted_tio_adu, 0.1, 1);
end


% Add noise to the image
if addNoise_q
    flux_log_deslanted_tio_adu = imnoise(flux_log_deslanted_tio_adu .* noiseLevel / 1e12, 'poisson') .* 1e12 / noiseLevel;
end
%     end

% Create spectra save name
fitsSavePath_tio = strcat(tio_spectra_deslanted_orig_dir, s, 'tio_beam', beamNumStr, '_obs', obsNumStr, spectrum_deslanted_fileName, '.fits');
matSavePath_tio = strcat(tio_spectra_deslanted_orig_dir, s, 'tio_beam', beamNumStr, '_obs', obsNumStr, spectrum_deslanted_fileName, '.mat');


% Save the spectra
fitswrite(flux_log_deslanted_tio_adu, fitsSavePath_tio)
flux_deslanted_adu = flux_log_deslanted_tio_adu;
save(matSavePath_tio, 'flux_deslanted_adu')





    
    %% TiO Plots
    
%     % Continuum plots
%     synthContinuumPlots_logspace(s, synthPipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
%       wl_beam_log_tio_m, wl_hiR_wide_log_tio_m, wl_hiR_log_comb_m, ...
%       flux_hiR_wide_tio_adu, flux_hiR_wide_tio_adu, flux_hiR_log_id_tio_adu, flux_hiR_log_id_2D_comb_tio_adu, flux_hiR_log_id_2D_comb_LSF_tio_adu, flux_log_deslanted_tio_adu, 'TiO')
%     
%     % Image plots
%     synthImagePlots_logspace(s, synthPipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
%       wl_beam_log_tio_m, wl_hiR_wide_log_tio_m, wl_hiR_log_comb_m, ...
%       flux_hiR_wide_tio_adu, flux_hiR_wide_tio_adu, flux_hiR_log_id_tio_adu, flux_hiR_log_id_2D_comb_tio_adu, flux_hiR_log_id_2D_comb_LSF_tio_adu, flux_log_deslanted_tio_adu, 'TiO')
%     
%     % Final spectrum continuum plot
%     synthFinalContinuumPlot_logspace(s, synthPipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
%       wl_beam_log_tio_m, flux_log_deslanted_tio_adu, 'TiO')
%     
%     
%     realAndSynthComparison(s, synthPipeline_dir, remotePipeline_dir, plateName, beamNumStr, obsNum, flux_log_deslanted_tio_adu, 1, 'TiO', 1, plateEmulationName, templateBeam)