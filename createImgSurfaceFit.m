function f = createImgSurfaceFit(x, y, img, interpMethod)


X = repmat(x(1:10:end), 1, length(y));

Y = repelem(y(1:10:end), length(x));

Z =[];
for r = 1:10:size(img, 1)
    Z = [Z, img(r, (1:10:end))];
end

f = fit([X',Y'], Z', interpMethod);