function [I, wl] = createSyntheticComb_V4(imgWidth, imgHeight, wl, xUpsample, yUpsample)

%% Testing Params

% clearvars;
% 
img = fitsread('E:\Research\1D_pipeline_orig\HIP14810\profile\101_profile_fit\17_profile.fits');
wl = fitsread(strcat('E:\Research\2DsyntheticSpectraCreation\HIP_101_STAR_wlsoln.fits'));

yUpsample = 100;
imgHeight = size(img,1);

xUpsample = 10;
imgWidth = size(img,2);

beamNum = 101;

obsNum = 17;

wl = wl(17,:) * 1e-10;

% load('E:\Research\1D_pipeline_orig\inputs\wlsoln.mat', 'wlsoln')



% yb = smoothn(fftFilter_V1(img(:,500), 2), 7);
% [a,b,phase_col500, period_col500, fit] = sinefit((1:length(yb))', yb, 6.5, 9);
% origPeaks_col500_x = (phase_col500:period_col500:imgHeight);


%% Spatial Matricies

row_pixel_height = imgHeight;

row_pixel_range = (1 :1/yUpsample: row_pixel_height)';

col_pixel_range = 1 :1/xUpsample: imgWidth;

% col_pixel_matrix = repmat(col_pixel_range, row_pixel_height, 1);

lambda = interp1((1:length(wl)), wl, (1:1/xUpsample:imgWidth), 'linear');

% lambda = wl .* 1e-10; %squeeze(wlsoln(:, 1, beamNum))' .* 1e-10;                            % Create equally spaced series of wavelengths in meters

lambda_matrix = repmat(lambda, length(row_pixel_range), 1);                  % Replicate the row of wavelengths to image height

%% Unused functions

% OPD = d ./ 2;

% d_m = lambda ./ (2 .* atan(opd1/opd2));                                        % Vertical fringe density (sine period)

%% Optical Path distance (OPD) at each physical pixel location.

opd1 = 7.6 * 1e-3;

a2PiCyclesCount_col500 = (imgHeight-1) / 8;

Nrange = linspace(0, a2PiCyclesCount_col500, length(row_pixel_range));

OPD_n = ( Nrange * lambda_matrix(1, (col_pixel_range==500)) ) + opd1;

OPD = repmat(OPD_n', 1, length(col_pixel_range));                                   % Replicate vector

%% Intensity Calculations

d_phi = (2 .* pi ./ lambda_matrix) .* OPD;                            % EM wave recombination phase offset

% phiPrime = repmat((row_pixel_range - 1) / 10, 1, imgWidth);               % (Not used)Row-wise phase offset (Estimation)

I_1 = 0.5;                                                              % Split beam 1 intesity

I_2 = 0.5;                                                              % Split beam 2 intesity

I = I_1 + I_2 + 2 .* (I_1 .* I_2).^(1/2) .* cos(d_phi);               % Intensity of recombined beams

%% Plotting
% 
% % y = I(:, 3500);
% % x = 1:length(y);
% 
% figure(1); clf(figure(1))
% subplot(211); hold on
% xr = find(col_pixel_range==450):find(col_pixel_range==550);
% imagesc(col_pixel_range(xr), row_pixel_range, I(:,xr));
% % plot(repmat(500, 1, length(origPeaks_col500_x)), origPeaks_col500_x, 'om')
% set(gca,'YDir','normal'); 
% colormap(gray)
% % xlim([500 600])
% subplot(212); hold on
% xr = find(col_pixel_range==3900):find(col_pixel_range==4000);
% imagesc(col_pixel_range(xr), row_pixel_range, I(:,xr));
% set(gca,'YDir','normal'); 
% colormap(gray)
% % xlim([3900 4001])
% % ylabel('Row # [px.]')
% % xlabel('Col # [px.]')
% 
% figure(2); clf(figure(2)); hold on
% p1 = plot(row_pixel_range, I(:,1), '.-k', 'displayname', 'First col');
% p2 = plot(row_pixel_range, I(:,imgWidth), '.-r', 'displayname', 'Last col');
% legend([p1 p2])
% xlabel('Row # [px.]')
% ylabel('Intensity')
%
%
figure(3); clf(figure(3))
imagesc(I(:,500));
% 
% % pause(.3)
% % opd2
% % alpha_y_degrees
% % end
% %%