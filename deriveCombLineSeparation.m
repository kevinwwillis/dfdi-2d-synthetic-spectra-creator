function combLineSep_px_m = deriveCombLineSeparation(C, tau_1, wl_hiR_m, wl_lowR_m, plot_q)



n_y1 = ceil(2 * (C + tau_1) ./ wl_hiR_m + 1 / 2);
wl_zro1 = 4 * tau_1 ./ (2 * n_y1 - 1);
wl_zro2 = 4 * tau_1 ./ (2 * (n_y1 - 1) - 1);
combLineSep_m = 2 * (wl_zro2 - wl_zro1);

dWL = diff(wl_lowR_m);
dWL = [dWL(1), dWL];
combLineSep_px = 1 ./ (dWL ./ spline(wl_hiR_m', combLineSep_m', wl_lowR_m')');

combLineSep_px_m = [combLineSep_px ; combLineSep_m];

if plot_q
    figure(31); clf(figure(31)); plot(combLineSep_px); grid on
end








%% Does not work
%combLineSep_pred_px = (WL_up_pad_m(1,:).^2 .* dPXdWL_up_pad + WL_up_pad_m(1,:) * endCombSepWanted_px) / (2 * endCombSepWanted_px); 


%% Not a derivation, but works till this problem is solved

% [maxima, minima] = splineMaximaMinima(spline(X_px, combImg_OneRow));
% 
% maximaSep = interp1(maxima(2:end), diff(maxima), X_px); 
% 
% minimaSep = interp1(minima(2:end), diff(minima), X_px);
% 
% combLineSep_px = nanmean([maximaSep ; minimaSep], 1);


