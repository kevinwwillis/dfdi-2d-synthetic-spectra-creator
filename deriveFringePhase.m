function fringePhase_px = deriveFringePhase(fringePeriod_px, OPD, WL_m)



fringePhase_px = (2 .* pi .* OPD ./ WL_m) ./ fringePeriod_px(1,:) ;

fringePhase_px = fringePhase_px - floor(fringePhase_px);

fringePhase_px = fringePhase_px(1, :) .* fringePeriod_px;


