function spectralLinesNorm = createSyntheticSpectralLines1D_WL(wl_m, spectraLineCenters_m, lineIntensities_pxv, noLineSpread)

% Create 1D spectral line image


spectralLinesRaw = ones(1, length(wl_m));
if ~noLineSpread
    
    
    for indVal = 1:length(spectraLineCenters_m)
        
        wl_n_m = spectraLineCenters_m(indVal);
        
        spectralLinesRaw = spectralLinesRaw - lineIntensities_pxv(indVal) * (normpdf(wl_m, wl_n_m));
    end
    
    spectralLinesNorm = mat2gray(spectralLinesRaw);
    
else
    
    wl_m_2 = wl_m(1):wl_m(end);
    
    for indVal = 1:length(spectraLineCenters_m)
        
        x = indClosestVal(wl_m_2, spectraLineCenters_m(indVal));
        
        spectralLinesRaw(1, x) = 0;
    end
    
    spectralLinesNorm = mat2gray(spectralLinesRaw);
    
end







