% Create the master comb image used in 2D synthetic spectra creation

% clearvars

%%

plots_q = 0;

getCombParams_q = 0;

downsample = 10; % Downsample multiplyer

% Load solved comb params
load('E:\Research\1a_syntheticPipelineTestEnv\beam1_CombParams.mat', 'C_best_px2', 'tau_best_px2', 'wl_offset_best_m2')

% tau_1 = 0.0147; % Determins comb line separation
% tau_1 = 0.014;
tau_1 = tau_best_px2; clear tau_best_px2

% C = 6.9555e-08;
% C = 4.4555e-08; % Higher = smaller slope (less steep); Determines fringe density
C = C_best_px2; clear C_best_px2

%%

load('wlsoln.mat', 'wlsoln')

maxWL_ang = max(max(wlsoln(:,1,:)));
minWL_ang = min(min(wlsoln(:,1,:)));

wl_example_m = squeeze(wlsoln(:,1,1))' .* 1e-10;

clear wlsoln

%%

load('sun_WL_ang.mat', 'WL_ang')

dWL_sun_ang = nanmean(diff(WL_ang));

clear WL_ang


wl_ang = floor(minWL_ang) :dWL_sun_ang * downsample: ceil(maxWL_ang);

wl_m = wl_ang * 1e-10;
 
clear wl_ang

R = round(wl_m(1) / (wl_m(2) - wl_m(1)));



dir0 = strcat('E:\Research\MARVELS_software\synthSpectraMaker_2D\masterCombImages\masterComb_R', num2str(R), '_test\');
mkdir(dir0)

save(strcat(dir0, 'wl_m.mat'), 'wl_m')

%% Spatial Matricies

% WL_up_pad_m = repmat(wl_up_pad_m, length(Y_up_pad_px), 1);                  % Replicate the row of wavelengths to image height
% 
% load('E:\Research\2DsyntheticSpectraCreation\combLineSepFit.mat', 'combLineSepFit')
% load('E:\Research\2DsyntheticSpectraCreation\fringePeriodFit.mat', 'fringePeriodFit')
% tau_1 = feval(combLineSepFit{basedOnBeamNum, 1}, endCombSepWanted_px);
% C = feval(fringePeriodFit{basedOnBeamNum, 1}, period_dPXdRad);

save(strcat(dir0, 'params.mat'), 'tau_1', 'C', 'R')

%%

Y_px = 1 :0.05: 30;
save(strcat(dir0, 'Y_px.mat'), 'Y_px')

Y0 = ( tau_1 + C .* (Y_px - 1) )';

combImg = zeros(length(Y0), length(wl_m));
parfor rowInd = 1:length(Y0)
    
    combImg(rowInd, :) = cos( 2 * pi * Y0(rowInd) ./ (wl_m + wl_offset_best_m2));
end

save(strcat(dir0, 'masterCombImg.mat'), 'combImg')

%% Predicted values
    
if getCombParams_q
    % Comb line period calculated
    n_y1 = ceil(2 * (C + tau_1) ./ wl_m + 1 / 2);
    wl_zro1 = 4 * tau_1 ./ (2 * n_y1 - 1);
    wl_zro2 = 4 * tau_1 ./ (2 * (n_y1 - 1) - 1);
    combLineSep_m = 2 * (wl_zro2 - wl_zro1);
    
    dWL = diff(wl_example_m);
    dWL = [dWL(1), dWL];
    combLineSep_px = 1 ./ (dWL ./ spline(wl_m', combLineSep_m', wl_example_m')');
    
    figure(31); clf(figure(31)); plot(combLineSep_px)
    
    save(strcat(dir0, 'combLineSep_m.mat'), 'combLineSep_m')
    
    
    % Measure fringe period
    fringePeriod_px = nan(1,length(wl_m));
    for colInd = 1 :10: length(wl_m)
        
        [~, pks1] = findpeaks(combImg(:, colInd), Y_px);
        
        fringePeriod_px(1, colInd) = mean(diff(pks1));
    end
    
    figure(31); clf(figure(31)); plot(fringePeriod_px)
    
    save(strcat(dir0, 'fringePeriod_px.mat'), 'fringePeriod_px')
end

% [X,Y] = meshgrid(wl_m, Y_px);
% flux_comb_interpolant_pxv = griddedInterpolant(X',Y',combImg');
% save(strcat(dir0, 'flux_comb_interpolant_pxv.mat'), 'flux_comb_interpolant_pxv')

if plots_q
    
    figure(66); clf(figure(66));
    subplot(211); hold on
    plot(wl_m, combLineSep_m, '-k')
    % plot(X_px, combLineSep_measured_px, ':m')
    
    subplot(212); hold on
    plot(wl_m, fringePeriod_px, '-k')
    % plot(X_px, fringePeriod_measured_px, ':m')
    
    
    figure(11); clf(figure(11)); hold on;
    imagesc(wl_m, Y_px, combImg);
    colormap(gray);
    set(gca,'YDir','normal')
end
