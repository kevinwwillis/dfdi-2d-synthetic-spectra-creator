% function marvels2DsyntheticSpectraCreation_testing2

clearvars

% image needs to be substaintially higher resolution than the original, that way small
% features can be blured and downsampled to look natural.



% dydx_comb = 4;



% rimg = fitsread(strcat('E:\Research\1D_pipeline_orig\47UMA\deslanted\048_deslanted_fit\28_deslant.fits'));
rimg = fitsread(strcat('E:\Research\PV_density_analysis\deslantTIOimgs\day121_beam48_deslant_tio.fits'));

rheight = size(rimg, 1);
rcent = round(rheight/2);



sheight = rheight;

x = 1:4001;
img = ones(sheight, 4001);
initPhase = 1;
period = 8;
combLineSep = 5;%1.6; %Original resolution value
combOpacity = 0.3;



% Need to know actual starting phase of the spectra, but this is how its coded:

% Testing Params
% load('E:\Research\1D_pipeline_orig\47UMA\wlsoln\intermediate\STAR_linelist.mat', 'CENTERS')
load('E:\Research\1D_pipeline_orig\47UMA\wlsoln\intermediate\TIO_linelist.mat', 'CENTERS')
slinesx = CENTERS{48, 28};

load('E:\Research\PV_density_analysis\sineFitParams\1_DayPeriod\options_SRaw_NeilSineFit\beam101_All_allColSineFitParams.mat', 'All_phases')

x = 1:4001;

% Measure spectral line intensity at each line. Intensities deviate largely.

% Fit spline & Measure spline values at sub-pixel line center points

rimg_1d = sum(rimg, 1);

rimg_1d = mat2gray(rimg_1d);

pp = spline((10:4001-10)', rimg_1d(10:4001-10)');

rimg_1d_splined = nan(1, 4001);
rimg_1d_splined(10:4001-10) = ppval(pp, (10:4001-10));

rb = 200;
lb = 500;

for ii = 1:length(slinesx)
   
    v = ppval(pp, slinesx(ii));
    
    leftbox0x = round(slinesx(ii) - lb : slinesx(ii) - 1);
    leftbox0x = leftbox0x(leftbox0x > 1 & leftbox0x < 4001);
    leftbox0y = rimg_1d_splined(leftbox0x);
    [lmvy, lmvx] = max(leftbox0y);
    lcpx(ii) = leftbox0x(lmvx);
    lcpy(ii) = lmvy;
    
    c = 0; pass = 0;
    while pass == 0
        rightbox0x = round(slinesx(ii) + 1 : slinesx(ii) + rb + c * rb/2);
        rightbox0x = rightbox0x(rightbox0x > 1 & rightbox0x < 4001);
        rightbox0y = rimg_1d_splined(rightbox0x);
        [rmvy, rmvx] = max(rightbox0y);
        rcpx(ii) = rightbox0x(rmvx);
        rcpy(ii) = rmvy;
        
%         rcpx - slinesx(ii)
        if rcpx - slinesx(ii) > 10 | c == 4
            pass = 1;
        else
            c = c + 1;
        end
    end
    
    dydx = (rmvy - lmvy) / (rightbox0x(rmvx) - leftbox0x(lmvx));
    
    b = lmvy - dydx * leftbox0x(lmvx);
    
    mv = dydx * slinesx(ii) + b;
    
    rlineIntensities(ii) = mv - v;
    
%     figure(3); clf(figure(3)); hold on
% %     plot(x, rimg_1d, '-k')
%     x1 = round(slinesx(ii) - lb - 50 : slinesx(ii) + rb + 50 + c * rb/2);
%     x1 = x1(x1>1 & x1<4001);
%     plot(x1, rimg_1d_splined(x1), '-c')
%     plot([slinesx(ii) - lb, slinesx(ii) - lb], [0, 1], '-r')
%     plot([slinesx(ii) + rb + c * rb/2, slinesx(ii) + rb + c * rb/2], [0, 1], '-r')
%     plot(leftbox0x(lmvx), lmvy, 'sm')
%     plot(rightbox0x(rmvx), rmvy, 'sb')
%     plot(slinesx(ii), v, 'ok')
%     xlim([slinesx(ii) - lb - 50 slinesx(ii) + rb + 50 + c * rb/2])
%     pause(0.1)
% %     if c > 0
% %        1; 
% %     end
end



figure(333); clf(figure(333)); hold on
imagesc(1:4001, 0:1, rimg_1d); colormap(gray)
plot(x, rimg_1d, '-k')
plot(lcpx, lcpy, '*m', rcpx, rcpy, 'sg')
x2 = [slinesx',slinesx'];
y = ppval(pp, slinesx);
plot(x2', ([y', y'+rlineIntensities'])', '-r')

% tys = y+rlineIntensities;
% tx0 = [slinesx, lcpx, rcpx];
tx0 = [lcpx, rcpx];
[tx1, i] = unique(tx0);
% ty0 = [tys, lcpy, rcpy];
ty0 = [lcpy, rcpy];
ty1 = ty0(i);

[tx2, i] = sort(tx1, 'ascend');
ty2 = ty1(i);


y = feval(fit(tx2', ty2', 'smoothingSpline', 'smoothingparam', 0.0001), x);

plot(x, y, '-g')


contAtLines = feval(fit(tx2', ty2', 'smoothingSpline', 'smoothingparam', 0.0001), slinesx);
y = ppval(pp, slinesx);
rlineIntensities = contAtLines' - y;

rlineIntensities = (mat2gray(rlineIntensities)) + 0.0000001;






mg = meshgrid(1:size(img, 1), 1:size(img, 2))';

phases = combLineSep * x;

per_mg = meshgrid(phases, 1:size(img, 1));

combLinesRaw = sin( ( (2.*pi) ./ period) .* mg + per_mg) + 1;
combLines = mat2gray(combLinesRaw) .* combOpacity;

%%%%%%%%

spectralLinesRaw = zeros(1,4001);
iii = 0;
for ii = slinesx
    iii = iii + 1;
%     spectralLinesRaw = spectralLinesRaw + normpdf(ii, 1:4001, rlineIntensities(iii)); 
    spectralLinesRaw = spectralLinesRaw + rlineIntensities(iii) * mat2gray(normpdf(1:4001, ii)); 
end

spectralLinesRaw = repmat(spectralLinesRaw, sheight,1);

spectralLinesNorm = mat2gray(spectralLinesRaw);

img2 = mat2gray(img - mat2gray(combLines + spectralLinesNorm));

rimgn = mat2gray(rimg);

% plot lines for a check
figure(1); clf(figure(1)); hold on

imagesc([mat2gray(rimgn - img2); rimgn ; img2]); colormap(gray); set(gca,'YDir','normal')

xx =[slinesx',slinesx'];
yy = [repmat(18, length(slinesx), 1),repmat(22, length(slinesx), 1)];
plot(xx', yy', '-r')

hold off

