% function I = createSyntheticComb_V2(imgWidth, imgHeight, beamNum)

clearvars

%% Testing Params

load('E:\Research\2DsyntheticSpectraCreation\hiresSunSpectra.mat')

imgHeight = 24;

imgWidth = size(sun_lum_px, 1);

% fiberNum = 1;

% load('E:\Research\1D_pipeline_orig\inputs\wlsoln.mat', 'wlsoln')



%% Spatial Matricies

row_pixel_height = imgHeight;

row_pixel_range = (1 : row_pixel_height)';

col_pixel_range = 1 : imgWidth;

col_pixel_matrix = repmat(col_pixel_range, row_pixel_height, 1);

% lambda = squeeze(wlsoln(:, 1, beamNum))' .* 1e-10;                    % Create equally spaced series of wavelengths in meters

lambda = sun_wl_angstom' .* 1e-10;                                      % Create equally spaced series of wavelengths in meters

lambda_matrix = repmat(lambda, row_pixel_height, 1);                    % Replicate the row of wavelengths to image height

%% Unused functions

% OPD = d ./ 2;

% d_m = lambda ./ (2 .* alpha_x_rad + pi*2);                            % Vertical fringe density (sine period)

%% Optical Path distance (OPD) at each physical pixel location.

alpha_x_degrees = -0.000005;                                            % Mirror x-axis angle (degrees)

alpha_x_rad = alpha_x_degrees * 2 * pi / 180;                           % Mirror x-axis angle (rad)

OPD_x = col_pixel_range .* tan(alpha_x_rad);                            % x-axis optical distance

OPD = repmat(OPD_x, row_pixel_height, 1);                               % Replicate vector

alpha_y_degrees = 0.0000019;                                            % Mirror y-axis angle (degrees)

alpha_y_rad = alpha_y_degrees * 2 * pi / 180;                           % Mirror y-axis angle (rad)

OPD_y = row_pixel_range .* tan(alpha_y_rad);                            % y-axis optical distance

OPD = OPD + repmat(OPD_y, 1, imgWidth);                                 % Replicate vector and add xy OPDs

%% Intensity Calculations

d_phi = (2 .* pi ./ lambda_matrix) .* OPD;                              % EM wave recombination phase offset

phiPrime = repmat((row_pixel_range - 1) / 10, 1, imgWidth);             % (Not used)Row-wise phase offset (Estimation)

I_1 = 0.1;                                                              % Split beam 1 intesity

I_2 = 0.1;                                                              % Split beam 2 intesity

I = I_1 + I_2 + 2 .* (I_1 .* I_2).^(1/2) .* cos(d_phi);                 % Intensity of recombined beams

%% Plotting

Ifa = I*2500 + repmat((sun_lum_px)', imgHeight, 1);
Ifm = I*2500 .* repmat((sun_lum_px)', imgHeight, 1);

figure(1); clf(figure(1))
imagesc(Ifm);set(gca,'YDir','normal'); colormap(gray)
% xlim([3900 4001])

figure(5); clf(figure(5))
subplot(121); hold on
title('Addition')
imagesc(Ifa);set(gca,'YDir','normal'); colormap(gray)
xlim([1933500 1934200])
subplot(122); hold on
title('Multiplication')
imagesc(Ifm);set(gca,'YDir','normal'); colormap(gray)
xlim([1933500 1934200])

figure(2); clf(figure(2))
plot(row_pixel_range, If(:,1), '.-k', row_pixel_range, If(:,imgWidth), '.-r')


Ifr = spline((1:length(sun_lum_px))', sun_lum_px, 1:4001);
Ifr2 = spline((1:length(I))', I, 1:4001);
Ifr = Ifr2 + repmat(mat2gray(Ifr), imgHeight, 1);

figure(3); clf(figure(3))
imagesc(Ifr);set(gca,'YDir','normal'); colormap(gray)

%%