% function combViewTesting(combImg_star, combImg_blur_star, combImg_blur_norm_star, combImg_blur_norm_resize_star)

% create a typical plate of spectra synthetically with their own unique parameters

clearvars

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

synthPipelineDir = 'E:\Research\1a_syntheticPipelineTestEnv';

oneDpipelineDir = 'E:\Research\1D_pipeline_orig';

plots = 0;                          % Toggle plot viewing

beamNums = 1:2;                     % Beams to create

spectraQuantity = 5;               % How many observations

BCVmax_mps = 20000;                     % Max barycentric velocity to use

basisPlate = 'HIP14810';            % Stars based on which current plate?

basisBeamNum = 101;            % Stars based on which current plate?

modelSpectra = 1;                   % Choose model spectra for all beams

% yPadding = 10;                      % Pad images for proper PSF convolution

% xPadding = 10;                      % Pad images for proper PSF convolution

yUpsample_px = 20;                     % Upsample for spectra height

xUpsample_px = 20;                     % Upsample for spectra width

X_px = 1:4001;                        % Choose pixel width of each spectra

Y_px = 1:25;                          % Choose pixel height of each spectra

combOpacityLow = 0;                 % Choose opacity lower limit of the synth comb

combOpacityHigh = 1;                % Choose opacity upper limit of the synth comb

slOpacityLow = 0;                   % Choose opacity lower limit of the synth spectral lines

slOpacityHigh = 1;                  % Choose opacity upper limit of the synth spectral lines

combEndLineSep_px = 1.6;               % Comb line separation at px 4000

midFringePeriod_px = 8;                % Comb line fringe period

sigmaBlur = 1 * round(sqrt(xUpsample_px * yUpsample_px));

% Stellar params

maxRV_mps = 10;                         % [m/s] Maximum RV to create

orbitPeriod_day = 100;                  % [days] Obit period of planet

surveyDaySpan_day = 600;                % [days] Survey span

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

OS=computer; if strcmp(OS,'PCWIN64') || strcmp(OS,'PCWIN'); s='\'; else s='/'; end

% Define padding for images
pad_px = 1;%sigmaBlur * 2;

% Define the speed of light
%c_SOL = 2.99792458e8; % m/s vacuum
c_SOL_mps = 2.99705e8; % m/s air

% Load and set the wavelengths for these spectra
load(strcat(oneDpipelineDir, '\inputs\wlsoln.mat'), 'wlsoln')
wlsoln_m = squeeze(wlsoln(:,1,:)) * 1e-10; % Angstom to Meters

%% y-axis spatial data

imgHeight_px = Y_px(end);
Y_ind = 1:length(Y_px);

% Upsampled
Y_up_px = 1 :1/yUpsample_px: imgHeight_px;
Y_up_ind = 1:length(Y_up_px);

% Padded
Y_pad_px = 1 - pad_px/xUpsample_px : Y_px(end) + pad_px/xUpsample_px;
Y_pad_ind = 1:length(Y_pad_px);

% Upsampled and padded
Y_up_pad_px = Y_pad_px(1) :1/xUpsample_px: Y_pad_px(end);
Y_up_pad_ind = 1:length(Y_up_pad_px);

Y_withinPad_ind = pad_px : length(Y_up_pad_px) - (pad_px - 1);

%% x-axis spatial data

imgWidth_px = X_px(end);
X_ind = 1:length(X_px);

% Upsampled
X_up_px = 1 :1/xUpsample_px: imgWidth_px;
X_up_ind = 1:length(X_up_px);

% Padded
X_pad_px = 1 - pad_px/xUpsample_px : X_px(end) + pad_px/xUpsample_px;
X_pad_ind = 1:length(X_pad_px);

% Upsampled and padded
X_up_pad_px = X_pad_px(1) :1/xUpsample_px: X_pad_px(end);
X_up_pad_ind = 1:length(X_up_pad_px);

X_WithinPad_ind = pad_px : length(X_up_pad_px) - (pad_px - 1);


%% STAR

wl_m_star = repmat(wlsoln_m, 1, 1, spectraQuantity);

beamNum = 1;

wl_up_pad_rest_m = interp1(X_px, squeeze(wl_m_star(:, 1, 1)), X_up_pad_px, 'pchip');

% Create comb image
[combImg_up_pad, combLineSep_px(:, beamNum), fringePeriod_px(:, beamNum), combLineSlope_px(:, beamNum)]...
    = createSyntheticComb_V7(...
    X_px, Y_px, X_up_pad_px, Y_up_pad_px, wl_up_pad_rest_m, midFringePeriod_px, combEndLineSep_px, beamNum);

% Normalize comb image to desired scale
combImg_up_pad = normScale(combImg_up_pad, combOpacityLow, combOpacityHigh);




combImg_blur_star = gaussfiltGPU_V1(combImg_up_pad, sigmaBlur);
combImg_blur_norm_star = mat2gray(combImg_blur_star(Y_withinPad_ind, X_WithinPad_ind));
combImg_blur_norm_resize_star = imresize(combImg_blur_norm_star, [imgHeight_px, imgWidth_px], 'cubic', 'Antialiasing', false);



figure(101); clf(figure(101)); hold on
imagesc(combImg_up_pad);
colormap(gray);
set(gca,'YDir','normal')


ii=1; 
figure(101+ii); clf(figure(101+ii)); hold on
imagesc(combImg_blur_star);
colormap(gray);
set(gca,'YDir','normal')

ii=ii+1;
figure(101+ii); clf(figure(101+ii)); hold on
imagesc(combImg_blur_norm_star);
colormap(gray);
set(gca,'YDir','normal')

ii=ii+1;
figure(101+ii); clf(figure(101+ii)); hold on
imagesc(combImg_blur_norm_resize_star);
colormap(gray);
set(gca,'YDir','normal')