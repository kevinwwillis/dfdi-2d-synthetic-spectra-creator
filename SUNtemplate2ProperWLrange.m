% function SUNtemplate2ProperWLrange

% converts the hi resolution TIO template to the same WL range as the master comb image

clearvars

OS=computer; if strcmp(OS,'PCWIN64') || strcmp(OS,'PCWIN'); s='\'; else s='/'; end; clear OS


%% Load master comb and its params

masterCombFolderName = 'masterComb_R247500_test';
masterComb_dir = strcat('E:\Research\MARVELS_software\synthSpectraMaker_2D\masterCombImages\', masterCombFolderName);

load(strcat(masterComb_dir, s, 'wl_m.mat'), 'wl_m')

% Rename comb WLs
wl_hiR_comb_m = wl_m; clear wl_m


%% Load sun and its params

load('E:\Research\MARVELS_software\synthSpectraMaker_2D\sun_WL_ang.mat', 'WL_ang')

load('E:\Research\MARVELS_software\synthSpectraMaker_2D\sun_flux_pxv.mat', 'flux_pxv')


% Rename comb flux
flux_hiR_sun_adu_0 = flux_pxv'; clear flux_pxv

% Rename comb WLs
wl_hiR_sun_ang_0 = WL_ang'; clear WL_ang

wl_hiR_sun_m_0 = wl_hiR_sun_ang_0 * 1e-10;



% Get index of desired range
rangeInd = (find(wl_hiR_sun_m_0 == wl_hiR_comb_m(1))) : (find(wl_hiR_sun_m_0 == wl_hiR_comb_m(end)));
rangeWideInd = (find(wl_hiR_sun_m_0 == wl_hiR_comb_m(1)) - 100) : (find(wl_hiR_sun_m_0 == wl_hiR_comb_m(end)) + 100);

% Cut down sun flux to the comb hires WL range
flux_hiR_cR_sun_adu = flux_hiR_sun_adu_0(1, rangeInd);

% Cut down sun WLs to the comb hires WL range
wl_hiR_cR_sun_m = wl_hiR_sun_m_0(1, rangeInd);

% Cut down sun flux to a bit wider than comb hires WL range
flux_hiR_wide_sun_adu = flux_hiR_sun_adu_0(1, rangeWideInd);

% Ensure comb flux is normalized
% flux_hiR_wide_sun_adu = mat2gray(flux_hiR_wide_sun_adu);

% Cut down sun WLs to a bit wider than comb hires WL range
wl_hiR_wide_sun_m = wl_hiR_sun_m_0(1, rangeWideInd);

% Create logspace sun WLs
wl_hiR_wide_log_sun_m = log(wl_hiR_wide_sun_m);



figure(1); clf(figure(1)); imagesc(repmat(flux_hiR_sun_adu, 20, 1)); colormap gray

save('E:\Research\MARVELS_software\synthSpectraMaker_2D\masterSuntemplate.mat', 'wl_hiR_cR_sun_m', 'flux_hiR_cR_sun_adu', 'wl_hiR_wide_sun_m', 'wl_hiR_wide_log_sun_m', 'flux_hiR_wide_sun_adu')



% realSpectrum0 = (fitsread('E:\Research\1a_syntheticPipelineTestEnv\sunSpectra\others\39_deslant_sun.fits'));
% % realSpectrum0 = (fitsread('E:\Research\1a_syntheticPipelineTestEnv\sunSpectra\01_deslant.fits'));
% 
% load('wlsoln.mat', 'wlsoln')
% wl_m = squeeze(wlsoln(:,1,:))' * 1e-10; % Angstom to Meters
% 
% wl_m = wl_m(1,:);
% 
% 
% % WL2PX_pp = spline(wl_beam_log_m, (1:4001));
% % X_hiR_px = spline(wl_beam_log_m, (1:4001), wl_hiR_wide_sun_m);
% 
% 
% figure(132); clf(figure(132));
% subplot(211); hold on
% imagesc(wl_m, 1:size(realSpectrum0, 1), realSpectrum0); colormap gray
% %         xlim([1 4001])
% %         xlim([3000 4001])
% % xlim([-14.5 -14.4])
% xlim([5140 5170]*1e-10)
% subplot(212); hold on
% imagesc(wl_hiR_wide_sun_m, 1:size(flux_hiR_wide_sun_adu, 1), flux_hiR_wide_sun_adu); colormap gray
% %         xlim([1 4001])
% %         xlim([3000 4001])
% xlim([5140 5170]*1e-10)
% % xlim([-14.5 -14.4])
% % xlim([0 4500])










% 
% 
% %% interpolate to new range
% 
% % figure; plot(diff(wl_hiR_sun_m), '-k')
% 
% % figure; plot(diff(wl_hiR_comb_m), '-k')
% 
% 
% % Get index of desired range 
% rangeInd = (find(wl_hiR_sun_m_0 == wl_hiR_comb_m(1))) : (find(wl_hiR_sun_m_0 == wl_hiR_comb_m(end)));
%  
% % Cut down sun flux to the comb hires WL range 
% flux_hiR_sun_adu = flux_hiR_sun_adu_0(1, rangeInd); 
%  
% % Cut down sun WLs to the comb hires WL range 
% wl_hiR_sun_m = wl_hiR_sun_m_0(1, rangeInd); 
% 
% 
% % flux_hiR_sun_adu = zeros(1, size(wl_hiR_comb_m, 2));
% % 
% % flux_hiR_sun_adu = interp1(wl_hiR_sun_m_0', flux_hiR_sun_adu_0', wl_hiR_comb_m', 'spline')';
% % 
% % % Convert all TIO WL values outside the comb WL range to zero
% % % flux_hiR_sun_adu(~isnan(flux_hiR_sun_adu)) = mat2gray(flux_hiR_sun_adu(~isnan(flux_hiR_sun_adu)));
% % 
% % % Convert all TIO WL values outside the comb WL range to zero
% % % flux_hiR_sun_adu(isnan(flux_hiR_sun_adu)) = 0;
% 
% % wl_hiR_sun_m = wl_hiR_comb_m;
% 
% 
% figure(1); clf(figure(1)); imagesc(repmat(flux_hiR_sun_adu, 20, 1)); colormap gray
% 
% save('E:\Research\MARVELS_software\synthSpectraMaker_2D\masterSuntemplate.mat', 'flux_hiR_sun_adu', 'wl_hiR_sun_m')
% 
% 
% 





