function combLineSep_px = measureCombLineSeparation(combImg_OneRow, X_px)


%% Does not work
%combLineSep_pred_px = (WL_up_pad_m(1,:).^2 .* dPXdWL_up_pad + WL_up_pad_m(1,:) * endCombSepWanted_px) / (2 * endCombSepWanted_px); 


%% Not a derivation, but works till this problem is solved

[maxima, minima] = splineMaximaMinima(spline(X_px, combImg_OneRow));

maximaSep = interp1(maxima(2:end), diff(maxima), X_px); 

minimaSep = interp1(minima(2:end), diff(minima), X_px);

combLineSep_px = nanmean([maximaSep ; minimaSep], 1);


