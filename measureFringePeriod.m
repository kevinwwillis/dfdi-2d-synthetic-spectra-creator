function fringePeriod_px = measureFringePeriod(X_px, Y_px, samplePoints_X_px, combImg)



fringePeriod_raw_px = nan(1,length(samplePoints_X_px));

for colInd = 1:length(samplePoints_X_px)
    
    [~, pks1] = findpeaks(combImg(:, samplePoints_X_px(colInd)), Y_px);
    
    fringePeriod_raw_px(1, colInd) = mean(diff(pks1));
end

fringePeriod_px = polyval(polyfit(samplePoints_X_px, fringePeriod_raw_px, 3), X_px);