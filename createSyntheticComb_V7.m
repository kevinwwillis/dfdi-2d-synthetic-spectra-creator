function [combImg, combLineSep_px, fringePeriod_measured_px, combLineSlope_px] = createSyntheticComb_V7(X_px, Y_px, X_up_pad_px, Y_up_pad_px, wl_up_pad_m, period_dPXdRad, endCombSepWanted_px, basedOnBeamNum)



% %% Testing Params
% 
% clearvars;
% 
% img = fitsread('E:\Research\1D_pipeline_orig\HIP14810\profile\101_profile_fit\17_profile.fits');
% 
% period_dPXdRad = 8;
% 
% yUpsample_px = 20;
% 
% xUpsample_px = 20;
% 
% pad_px = 0;
% 
% Y_px = 1:25;                          % Choose pixel height of each spectra
% 
% X_px = 1:4001;                        % Choose pixel width of each spectra
% 
% basedOnBeamNum = 101;
% 
% obsNum = 17;
% 
% endCombSepWanted_px = 1.6;
% 
% load(strcat('E:\Research\1D_pipeline_orig\inputs\wlsoln.mat'));
% wl_up_pad_m = interp1(X_px, squeeze(wlsoln(:, 1, basedOnBeamNum)) * 1e-10, (1 :1/xUpsample_px: X_px(end)), 'linear');
% 
% 
% %% y-axis spatial data
% 
% imgHeight_px = Y_px(end);
% Y_ind = 1:length(Y_px);
% 
% % Upsampled
% Y_up_px = 1 :1/yUpsample_px: imgHeight_px;
% Y_up_ind = 1:length(Y_up_px);
% 
% % Padded
% Y_pad_px = 1 - pad_px/xUpsample_px : Y_px(end) + pad_px/xUpsample_px;
% Y_pad_ind = 1:length(Y_pad_px);
% 
% % Upsampled and padded
% Y_up_pad_px = Y_pad_px(1) :1/xUpsample_px: Y_pad_px(end);
% Y_up_pad_ind = 1:length(Y_up_pad_px);
% 
% Y_withinPad_ind = pad_px : length(Y_up_pad_px) - (pad_px - 1);
% 
% %% x-axis spatial data
% 
% 
% imgWidth_px = X_px(end);
% X_ind = 1:length(X_px);
% 
% % Upsampled
% X_up_px = 1 :1/xUpsample_px: imgWidth_px;
% X_up_ind = 1:length(X_up_px);
% 
% % Padded
% X_pad_px = 1 - pad_px/xUpsample_px : X_px(end) + pad_px/xUpsample_px;
% X_pad_ind = 1:length(X_pad_px);
% 
% % Upsampled and padded
% X_up_pad_px = X_pad_px(1) :1/xUpsample_px: X_pad_px(end);
% X_up_pad_ind = 1:length(X_up_pad_px);
% 
% X_WithinPad_ind = pad_px : length(X_up_pad_px) - (pad_px - 1);


%% Spatial Matricies

WL_up_pad_m = repmat(wl_up_pad_m, length(Y_up_pad_px), 1);                  % Replicate the row of wavelengths to image height

% load('E:\Research\2DsyntheticSpectraCreation\combLineSepFit.mat', 'combLineSepFit')
% load('E:\Research\2DsyntheticSpectraCreation\fringePeriodFit.mat', 'fringePeriodFit')
% 
% tau_1 = feval(combLineSepFit{basedOnBeamNum, 1}, endCombSepWanted_px);
% 
% C = feval(fringePeriodFit{basedOnBeamNum, 1}, period_dPXdRad);

tau_1 = 0.0147;
C = 6.9555e-08;

%%

Y0 = ( tau_1 + C .* (Y_up_pad_px - 1) )';
Y = repmat(Y0, 1, length(X_up_pad_px));

combImg = cos( 2 * pi * Y ./ WL_up_pad_m );

%% Predicted values

[fringePeriod_px, combLineSep_px] = combAndFringePeriodCalculation(X_px, X_up_pad_px, WL_up_pad_m, tau_1, C);

combLineSlope_px = fringePeriod_px ./ combLineSep_px;

% fringePhase_px = deriveFringePhase(fringePeriod_px, O_m, WL_up_pad_m(1,:));


% %% Measured values
% % 
% % Measure comb line separation
% combLineSep_measured_px = deriveCombLineSeparation(combImg(1,:), X_up_pad_px);
% goodInd = ~isnan(combLineSep_measured_px);
% combLineSep_measured_px = polyval(polyfit(X_up_pad_px(goodInd)', combLineSep_measured_px(goodInd)', 3), X_px);
% 
% Measure fringe period
fringePeriod_measured_px = measureFringePeriod(X_up_pad_px, Y_up_pad_px, X_px(1:10:end), combImg);
goodInd = ~isnan(fringePeriod_measured_px);
fringePeriod_measured_px = polyval(polyfit(X_up_pad_px(goodInd)', fringePeriod_measured_px(goodInd)', 3), X_px);
% 
% % Calc comb line slope
% combLineSlope_measured_px = fringePeriod_measured_px ./ combLineSep_measured_px;
% 
% 
% 
% 
% figure(66); clf(figure(66)); 
% subplot(211); hold on
% plot(X_px, combLineSep_px, '-k')
% plot(X_px, combLineSep_measured_px, ':m')
% 
% subplot(212); hold on
% plot(X_px, fringePeriod_px, '-k')
% plot(X_px, fringePeriod_measured_px, ':m')


% 1;
% figure(11); clf(figure(11)); hold on;
% imagesc(X_up_pad_px, Y_up_pad_px, combImg);
% colormap(gray);
% set(gca,'YDir','normal')


