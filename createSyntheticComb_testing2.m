


%% Testing Params

clearvars;

img = fitsread('E:\Research\1D_pipeline_orig\HIP14810\profile\101_profile_fit\17_profile.fits');

period_dPXdRad = 8;

yUpsample_px = 20;

xUpsample_px = 20;

pad_px = 0;

Y_px = 1:45;                          % Choose pixel height of each spectra

X_px = 1:4001;                        % Choose pixel width of each spectra

beamNum = 101;

obsNum = 17;

endCombSepWanted_px = 1.6;

wl = fitsread(strcat('E:\Research\2DsyntheticSpectraCreation\HIP_101_STAR_wlsoln.fits'));
wl = wl(obsNum, :) * 1e-10;
wl_up_pad_m = interp1((1:length(wl)), wl, (1:1/xUpsample_px:X_px(end)), 'linear');


%% y-axis spatial data

imgHeight_px = Y_px(end);
Y_ind = 1:length(Y_px);

% Upsampled
Y_up_px = 1 :1/yUpsample_px: imgHeight_px;
Y_up_ind = 1:length(Y_up_px);

% Padded
Y_pad_px = 1 - pad_px/xUpsample_px : Y_px(end) + pad_px/xUpsample_px;
Y_pad_ind = 1:length(Y_pad_px);

% Upsampled and padded
Y_up_pad_px = Y_pad_px(1) :1/xUpsample_px: Y_pad_px(end);
Y_up_pad_ind = 1:length(Y_up_pad_px);

Y_withinPad_ind = pad_px : length(Y_up_pad_px) - (pad_px - 1);

%% x-axis spatial data


imgWidth_px = X_px(end);
X_ind = 1:length(X_px);

% Upsampled
X_up_px = 1 :1/xUpsample_px: imgWidth_px;
X_up_ind = 1:length(X_up_px);

% Padded
X_pad_px = 1 - pad_px/xUpsample_px : X_px(end) + pad_px/xUpsample_px;
X_pad_ind = 1:length(X_pad_px);

% Upsampled and padded
X_up_pad_px = X_pad_px(1) :1/xUpsample_px: X_pad_px(end);
X_up_pad_ind = 1:length(X_up_pad_px);

X_WithinPad_ind = pad_px : length(X_up_pad_px) - (pad_px - 1);


%% Spatial Matricies

WL_up_pad_m = repmat(wl_up_pad_m, length(Y_up_pad_px), 1);                  % Replicate the row of wavelengths to image height

dWL = diff(wl);
dWLdPX = [dWL(1) dWL];
dPXdWL_up_pad = polyval(polyfit(X_px, 1./dWLdPX, 3), X_up_pad_px);

period_dRADdPX = 1 / period_dPXdRad;

wl_mid_m = WL_up_pad_m(1, (X_up_pad_px==2000));

% n_1 = round((4 * (wl_mid_m / period_dPXdRad + 0.0223) - wl(end)) / (2 * wl(end)));
% 1/dWLdPX(end) * (4 * (wl_mid_m / period_dPXdRad + 0.01) / (2 * n_1 + 3) - wl(end))
% O_m = (endCombSepWanted_px * dWLdPX(end) - wl(end)) * (2 * n_1 + 3) / 4 - wl_mid_m / period_dPXdRad;
% 
% 
% 1/dWLdPX(end) * (wl(end) - 4 * (wl_mid_m / period_dPXdRad + 0.0223) / (2 * n_1 + 3))
% 
% 
% 
% (-2 * endCombSepWanted_px * wl_mid_m - period_dPXdRad * endCombSepWanted_px * wl(end) - wl(end)^2 * period_dPXdRad * 1/dWLdPX(end)) / (2 * endCombSepWanted_px * period_dPXdRad)
% 
% 
% O_m = (wl(end)^2 * 1/dWLdPX(end) + wl(end) * endCombSepWanted_px) / (2 * endCombSepWanted_px); % m^2 * px/m + m * px / px = m * px + m
% 
% fun = @(o) endCombSepWanted_px - 1/dWLdPX(end) * (wl(end) - 4 * (wl_mid_m / period_dPXdRad + o) / (2 * round((4 * (wl_mid_m / period_dPXdRad + o) - wl(end)) / (2 * wl(end))) + 3));
% 
% O_m = fminsearch(fun, 0.01);
% 
% 1/dWLdPX(end) * (wl(end) - 4 * (wl_mid_m / period_dPXdRad + O_m) / (2 * round((4 * (wl_mid_m / period_dPXdRad + O_m) - wl(end)) / (2 * wl(end))) + 3))

O_range_m = 0.001:0.0001: 0.1;
% sep = 1/dWLdPX(end) .* (wl(end) - 4 .* (wl_mid_m ./ period_dPXdRad + O_range_m)./ (2 .* round((4 .* (wl_mid_m ./ period_dPXdRad + O_range_m) - wl(end)) ./ (2 .* wl(end))) + 3));
% figure; plot(O_range_m, sep, '.-k');



%%

rr = find(X_up_pad_px==3990) : find(X_up_pad_px==4001);

y_rad = (Y_up_pad_px - 1) / period_dPXdRad;% Y_up_pad_px / period_dPXdRad;

% Y_rad = repmat(y_rad', 1, size(WL_up_pad_m, 2));

B_dRADdM = 2 .* pi ./ WL_up_pad_m(1,rr); % rad / m

% A_rad = 2 .* pi .* y_rad .* wl_mid_m ./ WL_up_pad_m; % rad * rad/px * m / m = rad


% O_range_m = O_range_m(1:20);


for oi = 1:length(O_range_m)
    
    disp(num2str(oi))
    
    O_m = O_range_m(oi);
    
%     combImg = 0.5 + 0.5 * cos(A_rad(:,rr) + B_dRADdM(:,rr) .* O_m); % rad + rad * m / m = rad
    combImg = 0.5 + 0.5 * cos(B_dRADdM .* O_m); % rad + rad * m / m = rad
    
    
    % Measure comb line separation
%     [~, pks0] = findpeaks(combImg(1, :), X_up_pad_px(rr));
    pp = spline(X_up_pad_px(rr), combImg(1, :));
    [maxima, minima] = splineMaximaMinima(pp);
    period0_px(1,oi) = nanmean([nanmean(diff(maxima)), nanmean(diff(minima))]);
    
%     % Measure fringe period
%     periodFR_px_tmp = nan(1,6);
%     for colInd = 0:5
%         [~, pks0] = findpeaks(combImg(1, end-colInd*xUpsample_px), Y_up_pad_px);
%         periodFR_px_tmp(end+1) = mean(diff(pks0));
%     end
%     periodFR_px(1,oi) = nanmean(periodFR_px_tmp);
    
    
%     slope0(1,oi) = periodFR_px(1,oi)/period0_px(1,oi);
end

%% Predicted values

combLineSep_pred_px = (WL_up_pad_m(1,:).^2 .* dPXdWL_up_pad + WL_up_pad_m(1,:) * endCombSepWanted_px) / (2 * endCombSepWanted_px);

fringePeriod_pred_px = period_dPXdRad .* WL_up_pad_m(1,:) ./ WL_up_pad_m(1, (X_up_pad_px==2000));

slope_pred = fringePeriod_pred_px ./ combLineSep_pred_px;

% fringePhase_pred_px = (2 .* pi .* O_0 ./ WL_up_pad_m(1,:)) ./ fringePeriod_pred_px(1,:) ;
% fringePhase_pred_px = fringePhase_pred_px - floor(fringePhase_pred_px);
% fringePhase_pred_px = fringePhase_pred_px(1, :) .* fringePeriod_pred_px;

%% Measured values

% Measure comb line separation
[~, pks0] = findpeaks(combImg(15, :), X_up_pad_px);
combLineSep_raw_px = diff(pks0);
combLineSep_px = polyval(polyfit(pks0(2:end), combLineSep_raw_px, 3), X_up_pad_px);

% Measure fringe period
cols = 1 :10 * xUpsample_px: 4001;
fringePeriod_raw_px = nan(1,length(cols));
for colInd = 1:length(cols)
    [~, pks1] = findpeaks(combImg(:, cols(colInd)), Y_up_pad_px);
    fringePeriod_raw_px(1, colInd) = mean(diff(pks1));
end
fringePeriod_px = polyval(polyfit(cols, fringePeriod_raw_px, 3), X_up_pad_px);

% Calc comb line slope
slope0 = fringePeriod_px ./ combLineSep_px;


figure(22); clf(figure(22))
% subplot(311); hold on;
title('Comb Line Sep.')
plot(O_range_m, period0_px, '-k')
% subplot(312); hold on;
% title('Fringe Per.')
% plot(O_range_m, periodFR_px, '-k')
% subplot(313); hold on;
% title('Comb Line Slope')
% plot(O_range_m, slope0, '-k')



figure(22); clf(figure(22))
subplot(311); hold on;
title('Comb Line Sep.')
plot(X_up_pad_px, combLineSep_pred_px, '-k')
plot(pks0(2:end), combLineSep_raw_px, '.b')
plot(X_up_pad_px, combLineSep_px, '-m')
subplot(312); hold on;
title('Fringe Per.')
plot(X_up_pad_px, fringePeriod_pred_px, '-k')
plot(cols, fringePeriod_raw_px, '.b')
plot(X_up_pad_px, fringePeriod_px, '-m')
subplot(313); hold on;
title('Comb Line Slope')
plot(X_up_pad_px, slope_pred, '-k')
plot(X_up_pad_px, slope0, '-m')


figure(11); clf(figure(11)); hold on;
imagesc(X_up_pad_px, Y_up_pad_px, combImg);
colormap(gray);
set(gca,'YDir','normal')

