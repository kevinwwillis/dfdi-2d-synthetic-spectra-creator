function createSyntheticSpectra_V1(plateName, beamNum, obsNum, wl, colRange, rowRange, imageSavePath, colShift)



beamNumStr=num2str(beamNum); if beamNum<100;beamNumStr=strcat('0',num2str(beamNum));end; if beamNum<10;beamNumStr=strcat('00',num2str(beamNum));end
if obsNum < 10; obsNumStr = strcat('0',num2str(obsNum)); else obsNumStr = num2str(obsNum); end

combOpacityLow = 0;
combOpacityHigh = 1;

slOpacityLow = 0;
slOpacityHigh = 1;

yUpsample = 100;
xUpsample = 10;

% Test vals 
% imageSavePath = 'E:\Research\PipelineSoftware\continuumFitting\syntheticImg_test.fits';
% plateName = 'HIP14810';
% beamNum = 101;
% obsNum = 17;
% colShift = 0;

img = fitsread(cat(2, 'E:\Research\1D_pipeline_orig\', plateName, '\profile\', beamNumStr, '_profile_fit\', obsNumStr, '_profile.fits');
% wl = fitsread(strcat('E:\Research\2DsyntheticSpectraCreation\HIP_101_STAR_wlsoln.fits'));
% wl = wl(obsNum,:);

%%

imgn = mat2gray(img);

spectralLinesImg = 1 - createSyntheticSpectralLines_V1(img, plateName, beamNum, obsNum, xUpsample, yUpsample, 1, colShift);

[combLineImg, ~] = createSyntheticComb_V4(colRange(end), rowRange(end), beamNum, wl, xUpsample, yUpsample);

%%

ci = normScale(combLineImg, combOpacityLow, combOpacityHigh);

syntheticImg = mat2gray((ci) .* (spectralLinesImg));

%%

syntheticImg2 = imgaussfilt(syntheticImg, 20);

syntheticImg2(:, 50:end-50) = mat2gray(syntheticImg2(:, 50:end-50));

syntheticImg2 = imresize(syntheticImg2, [size(img,1), size(img,2)], 'cubic', 'Antialiasing', false);


%%

figure(1); clf(figure(1)); hold on
imagesc([mat2gray(imgn - syntheticImg2); imgn ; syntheticImg2]); 
colormap(gray); 
set(gca,'YDir','normal')


% figure(2); clf(figure(2)); hold on
% subplot(211)
% imagesc(syntheticImg)
% colormap(gray); 
% set(gca,'YDir','normal')
% subplot(212)
% imagesc(syntheticImg2)
% colormap(gray); 
% set(gca,'YDir','normal')
% hold off


fitswrite(syntheticImg, imageSavePath)











