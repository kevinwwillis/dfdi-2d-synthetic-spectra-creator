


%% Testing Params

clearvars;

img = fitsread('E:\Research\1D_pipeline_orig\HIP14810\profile\101_profile_fit\17_profile.fits');

period_dPXdRad = 8;
yUpsample_px = 20;
xUpsample_px = 20;
pad_px = 0;
Y_px = 1:25;                          % Choose pixel height of each spectra
X_px = 1:4001;                        % Choose pixel width of each spectra
beamNum = 101;
obsNum = 17;
endSepWanted_px = 1.6;

wl = fitsread(strcat('E:\Research\2DsyntheticSpectraCreation\HIP_101_STAR_wlsoln.fits'));
wl = wl(obsNum, :) * 1e-10;
wl_up_pad_m = interp1((1:length(wl)), wl, (1:1/xUpsample_px:X_px(end)), 'linear');


plotRange = [3991 4001];

plotRange2 = [3001 4001];


%% y-axis spatial data

imgHeight_px = Y_px(end);
Y_ind = 1:length(Y_px);

% Upsampled
Y_up_px = 1 :1/yUpsample_px: imgHeight_px;
Y_up_ind = 1:length(Y_up_px);

% Padded
Y_pad_px = 1 - pad_px/xUpsample_px : Y_px(end) + pad_px/xUpsample_px;
Y_pad_ind = 1:length(Y_pad_px);

% Upsampled and padded
Y_up_pad_px = Y_pad_px(1) :1/xUpsample_px: Y_pad_px(end);
Y_up_pad_ind = 1:length(Y_up_pad_px);

Y_withinPad_ind = pad_px : length(Y_up_pad_px) - (pad_px - 1);

%% x-axis spatial data


imgWidth_px = X_px(end);
X_ind = 1:length(X_px);

% Upsampled
X_up_px = 1 :1/xUpsample_px: imgWidth_px;
X_up_ind = 1:length(X_up_px);

% Padded
X_pad_px = 1 - pad_px/xUpsample_px : X_px(end) + pad_px/xUpsample_px;
X_pad_ind = 1:length(X_pad_px);

% Upsampled and padded
X_up_pad_px = X_pad_px(1) :1/xUpsample_px: X_pad_px(end);
X_up_pad_ind = 1:length(X_up_pad_px);

X_WithinPad_ind = pad_px : length(X_up_pad_px) - (pad_px - 1);


plotRangeInd = find(X_up_pad_px == plotRange(1)) : find(X_up_pad_px == plotRange(end));

plotRange2Ind = find(X_up_pad_px == plotRange2(1)) : find(X_up_pad_px == plotRange2(end));

%% Spatial Matricies

% wl_up_pad_m = interp1(X_px', wl_m', X_up_pad_px, 'linear');

WL_up_pad_m = repmat(wl_up_pad_m, length(Y_up_pad_px), 1);                  % Replicate the row of wavelengths to image height


%% Optical Path distance (OPD) at each physical pixel location

WL_up_pad_m_R = WL_up_pad_m(:, (plotRangeInd));

% a2PiCyclesCount_col500 = (Y_px(end)) / period_dPXdRad;
% 
% Nrange = linspace(1, a2PiCyclesCount_col500, length(Y_up_pad_px));

period_dRADdPX = 1 / period_dPXdRad;

% y_dRADdPX = linspace(0, (Y_px(end)-1) / period_dPXdRad, length(Y_up_pad_px));% Y_up_pad_px / period_dPXdRad;

% y_dRADdPX = linspace(0, 2, length(Y_up_pad_px));% Y_up_pad_px / period_dPXdRad;

% y_dRADdPX = linspace(0, 1, length(Y_up_pad_px)) ./ Y_up_pad_px;

y_dRADdPX = linspace(0, 3, length(Y_up_pad_px));

Y_dRADdPX = repmat(y_dRADdPX', 1, size(WL_up_pad_m_R, 2));

wl_mid_m = WL_up_pad_m(1, (X_up_pad_px==500));

% Y = repmat(Y_up_pad_px', 1, size(WL_up_pad_m_R, 2));
Y = Y_dRADdPX;

% A_rad_R = 2 .* pi .* Y_dRADdPX .* wl_mid_m ./ (WL_up_pad_m_R .* period_dRADdPX); % rad * rad/px * m / (m * (rad/px)) = rad

A_rad_R = 2 .* pi .* Y .* wl_mid_m ./ (WL_up_pad_m_R); % rad * rad/px * m / m = rad

B_rad_R = 2 .* pi ./ WL_up_pad_m_R; % rad / m





% periods0 = 8 .* WL_up_pad_m(1,:) ./ WL_up_pad_m(1, (X_up_pad_px==500));
% 
% phases0 = (2 .* pi .* 0.02 ./ WL_up_pad_m(1,:)) ./ periods0(1,:) ;
% 
% phases0 = phases0 - floor(phases0);
% 
% phases0 = phases0(1, :) .* periods0;



% Nrange = (Y_up_pad_px - 1) / period_dPXdRad;

% Nrange = radRange / period_dPXdRad;
% 
% OPD_end_m_0 = 20e-3;
% 
% J = ( Nrange * wl_up_pad_matrix_m(1, (X_up_pad_px == 2000)) );
% 
% 
% wl_up_pad_matrix_m_R = wl_up_pad_matrix_m(:, (plotRangeInd));
% 
% B = 2 .* pi ./ wl_up_pad_matrix_m_R;


% combImg = 0.5 + 0.5 .* cos( (2 .* pi .* repmat(Nrange', 1, length(X_up_pad_px)) .* wl_up_pad_matrix_m(1, (X_up_pad_px==500))) ./ (8 .* wl_up_pad_matrix_m));



fLo = 2*pi/5;
fHi = 2*pi/0.1;

fLoFR = 0;
fHiFR = 2*pi/0.1;

cntT = 2000;

[OPD_end_m_all, period0_px, periodFR_px, slope] = deal(nan(1, cntT));
cnt = 0;

% opdAll = linspace(0, 1, cntT);
% opdAll = linspace(0.02-1e-200 , 0.2+1e-200, 100);
% 
% opdAll=[];
% for ii = 0 :0.0001: 5
% %     opdAll(end+1) = ii * wl_up_pad_matrix_m(1, (X_up_pad_px==500)) / 100;
%     
% end

% opdAll = (logspace(-1,1,1000)-1);
% opdAll = opdAll - min(opdAll);
% opdAll = opdAll/max(opdAll);

% opdAll = 100 :100: 100000;
opdAll = 0.00 :0.0001: 1;

% for OPD_end_m = linspace(OPD_end_m_0 - 1e-1, OPD_end_m_0 + 1e-1, cntT)
% parfor cnt = 1:cntT;
for cnt = 1:length(opdAll);

    
    OPD_end_m_all(cnt) = opdAll(cnt);
    
%     OPD_R = OPD_R + opdAll(cnt);
% 
%     %% Intensity Calculations
%     
%     d_phi = (2 .* pi ./ wl_up_pad_matrix_m_R) .* OPD_R;
%     
%     combImg = 0.5 + 0.5 .* cos(d_phi);               % Intensity of recombined beams
%     
    
%     combImg = 0.5 + 0.5 .* cos(A_rad_R + B_rad_R .* opdAll(cnt));
     combImg = 1 + cos(A_rad_R + B_rad_R .* opdAll(cnt)); % rad + rad * m / m = rad
    
    
    
    % Measure comb line separation
%     [~, ~, c, d, ~] = sinefit(X_up_pad_px(plotRangeInd(end-20:end))', combImg(15, end-20:end)', fLo, fHi);
%     period0_px(cnt) = (2*pi)/d;
%     
%     ans0 = lsqnonlin(@(x) combImg(15, :) - (0.5 + 0.5 .* sin(2*pi/x(1) .* X_up_pad_px(plotRangeInd) + x(2))), [0.02, 0.1], [0.01, 0], [5, 5])
%     ans0 = fminbnd(@(x) sum(abs(combImg(15, :) - (0.5 + 0.5 .* sin(2.*pi./x(1) .* X_up_pad_px(plotRangeInd) + x(2))))), [0.01, 0], [5, 5])
%     ans0 = fmincon(@(x) sum(abs(combImg(15, :) - (0.5 + 0.5 .* sin(2.*pi./x(1) .* X_up_pad_px(plotRangeInd) + x(2))))), [1, 0.1], [],[],[],[],[0.01, 0], [5, 5])
%     fit0 = 0.5 + 0.5 * sin(2*pi/ans0(1) .* X_up_pad_px(plotRangeInd) + ans0(2));
%     figure; plot(X_up_pad_px(plotRangeInd)', combImg(15, :)', '-k', X_up_pad_px(plotRangeInd)', fit0, ':m')
%     
% %      fit=a+b*sin(c+d.*(x));
%     [a, b, c, d, fit0] = sinefit(X_up_pad_px(plotRangeInd(end-20:end))', combImg(15, end-20:end)', fLo, fHi);
%     xx = X_up_pad_px(plotRangeInd(end-20:end));
%     xx = xx(1) :0.00001: xx(end);
%     fit0=a+b*sin(c+d.*(xx));
%     figure; plot(X_up_pad_px(plotRangeInd(end-20:end))', combImg(15, end-20:end)', '-k', xx, fit0, '.m')

%     [~, pks0] = findpeaks(combImg(15, end-30:end), X_up_pad_px(plotRangeInd(end-30:end)));
    [~, pks0] = findpeaks(combImg(15, :), X_up_pad_px(plotRangeInd));
    period0_px(cnt) = mean(diff(pks0));
%     [pv0, pks0] = findpeaks(combImg(15, end-30:end), X_up_pad_px(plotRangeInd(end-30:end)));
%     figure(8); clf(figure(8)); hold on;
%     plot(X_up_pad_px(plotRangeInd(end-30:end))', combImg(15, end-30:end)', '-k')
%     plot(pks0, pv0, '.m')
    
    % Measure fringe period
    periodFR_px_tmp = nan(1,6);
    for colInd = 0:5
%         [~, ~, ~, d] = sinefit(Y_up_pad_px', combImg(:, end-colInd*xUpsample_px), fLoFR, fHiFR);
%         periodFR_px_tmp(end+1) = (2*pi)/d;
        [~, pks0] = findpeaks(combImg(:, end-colInd*xUpsample_px), Y_up_pad_px);
        periodFR_px_tmp(end+1) = mean(diff(pks0));
    end
    periodFR_px(cnt) = nanmean(periodFR_px_tmp);
    
    slope0(cnt) = periodFR_px(cnt)/period0_px(cnt);
    
    
%     if mod(opdAll(cnt), 0.01) == 0
        figure(11); clf(figure(11)); hold on;
        title({strcat('OPD = ', num2str(opdAll(cnt))),...
            strcat('Line Sep = ', num2str(period0_px(cnt))),...
            strcat('Fringe Per. = ', num2str(periodFR_px(cnt))), ...
            strcat('Line Slope = ', num2str(slope0(cnt)))})
        imagesc(X_up_pad_px(plotRangeInd), Y_up_pad_px, combImg);
        colormap(gray);
        set(gca,'YDir','normal')
        
        figure(22); clf(figure(22))
        subplot(311); hold on;
        title('Comb Line Sep.')
        plot(OPD_end_m_all(1:cnt), period0_px(1:cnt), '-k')
        subplot(312); hold on;
        title('Fringe Per.')
        plot(OPD_end_m_all(1:cnt), periodFR_px(1:cnt), '-k')
        subplot(313); hold on;
        title('Comb Line Slope')
        plot(OPD_end_m_all(1:cnt), slope0(1:cnt), '-k')
        pause(0.01)
%     end


%     figure(11); clf(figure(11));
%     subplot(211); hold on;
%     title(strcat(num2str(OPD_end_m), ' : ', num2str(OPD_end_m_0 + 1e-3)))
%     imagesc(X_up_pad_px(plotRangeInd), Y_up_pad_px, combImg(:, (plotRangeInd)));
%     colormap(gray);
%     set(gca,'YDir','normal')
%     
%     subplot(212); hold on;
%     title(strcat(num2str(OPD_end_m), ' : ', num2str(OPD_end_m_0 + 1e-3)))
%     imagesc(X_up_pad_px(plotRangeInd), Y_up_pad_px, combImg(:, (plotRange2Ind)));
%     colormap(gray);
%     set(gca,'YDir','normal')
    
    
    
end
% end

        figure(22); clf(figure(22))
        subplot(311); hold on;
        title('Comb Line Sep.')
        plot(OPD_end_m_all(1:cnt), period0_px(1:cnt), '-k')
        subplot(312); hold on;
        title('Fringe Per.')
        plot(OPD_end_m_all(1:cnt), periodFR_px(1:cnt), '-k')
        subplot(313); hold on;
        title('Comb Line Slope')
        plot(OPD_end_m_all(1:cnt), slope0(1:cnt), '-k')


