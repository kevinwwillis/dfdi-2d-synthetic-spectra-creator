function [phaseShift_rad, phaseShift_px] = deriveFringePhaseShift(wl_rest_m, X_px, combLineSep_px, fringePeriod_px, RV, dX_total_px)



dX_dY = combLineSep_px ./ fringePeriod_px;

dPHA_rad_dY_px = 2 * pi ./ fringePeriod_px;

if isempty(dX_total_px)
    % Define the speed of light
    %c_SOL = 2.99792458e8; % m/s vacuum
    c_SOL_mps = 2.99705e8; % m/s air
    
    % If RV = 1 m/s, what is dWL across the spectra
    dWL = wl_rest_m .* RV ./ c_SOL_mps;
    
    % What is dX
    WL_2_X = fit(wl_rest_m', X_px', 'pchip');
    dX = feval(WL_2_X, wl_rest_m + dWL)' - X_px;
else
    dX = dX_total_px;
end

% What is dY
dY = dX ./ dX_dY;

% What is d_PHA_rad
d_PHA_rad = dPHA_rad_dY_px .* dY;

%%

phaseShift_rad = d_PHA_rad;

phaseShift_px = dY;