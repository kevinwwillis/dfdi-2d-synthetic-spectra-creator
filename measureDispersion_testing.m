% function measureDispersion_testing



showLSQProcessPlots_q = 1;

downsample = 80;

R_LSF = 15000;

yRangeUsed = 1:16;

load('sun_WL_ang.mat', 'WL_ang')
load('E:\Research\MARVELS_software\synthSpectraMaker_2D\sun_flux_pxv.mat', 'flux_pxv')

load('wlsoln.mat', 'wlsoln')
maxWL_ang = max(max(wlsoln(:,1,:)));
minWL_ang = min(min(wlsoln(:,1,:)));

wl_lowR_m = squeeze(wlsoln(:,1,1))' .* 1e-10; clear wlsoln

dWL_sun_ang = nanmean(diff(WL_ang));

wl_hiR_m = ( floor(minWL_ang) :dWL_sun_ang * downsample: ceil(maxWL_ang) ) * 1e-10;

dWL_dPX = nanmean(diff(wl_hiR_m)) * 1e10;

% flux_sun_pxv = gpuArray(spline(WL_ang, flux_pxv, wl_hiR_m * 1e10)); clear WL_ang
flux_sun_pxv = (spline(WL_ang, flux_pxv, wl_hiR_m * 1e10)); clear WL_ang

wl_cent_ang = mean(wl_lowR_m) * 1e10;

% Create LSF
gaussianRange_x = 1:901;
mu = 450;
dl = wl_cent_ang / R_LSF / dWL_dPX;
sigma = (dl / (2 * sqrt(2 * log(2))));
psf_1d = normpdf(gaussianRange_x, mu, sigma) / 0.4;
LSF = psf_1d / sum(psf_1d); 
% figure; plot(LSF/ max(LSF))
LSF(LSF/ max(LSF) < 0.001) = [];


clear psf_1d dl mu gaussianRange_x wl_cent_ang dWL_sun_ang minWL_ang maxWL_ang R_LSF downsample flux_pxv


Y_px = 1 :0.05: yRangeUsed(end);

% Create gridded data for final spectrum downsample interpolation
[WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px] = meshgrid(wl_hiR_m, Y_px);
[WL_beam_log_gridded_m, Y_beam_log_gridded_0_px] = meshgrid(wl_lowR_m, 1:yRangeUsed(end));


load('E:\Research\1a_syntheticPipelineTestEnv\beam1_CombParams.mat', 'C_best_px2', 'tau_best_px2', 'wl_offset_best_m2')


indind = 1:length(wl_hiR_m);
xM_lowR = size(wl_lowR_m, 2);

chunkCount = 800;
chunkWidth_lowR = 4000 / chunkCount;
chunkLeftEdges = 1:chunkWidth_lowR:(xM_lowR - mod(xM_lowR, 2));
% chunkLeftEdges(end) = [];
chunkRightEdges = chunkLeftEdges + chunkWidth_lowR - 1;
chunkCenters_px = round((chunkRightEdges(1) - chunkLeftEdges(1)) / 2 + chunkLeftEdges);
chunkCenters_m = wl_lowR_m(chunkCenters_px);


% Find real spectra
files0 = dir(strcat('E:\Research\1a_syntheticPipelineTestEnv\sunSpectra\*deslant*.fits'));

spectraNames = struct2cell(files0); clear files0

spectraNames = spectraNames(1,:);

realSpectrum = (mat2gray(fitsread(strcat('E:\Research\1a_syntheticPipelineTestEnv\sunSpectra\', spectraNames{1, 1}))));

realSpectrum = realSpectrum(yRangeUsed, :);
realSpectrum(:, 50:3950) = continnumNorm(realSpectrum(:, 50:3950), 1000);
realSpectrum(:, 60:3940) = flattenImgRows(realSpectrum(:, 60:3940), 1);
realSpectrum(:, [(1:60), (3940:4001)]) = 0;


realSpectrum2 = realSpectrum;

for col = 1:4001
    
    tmp0 = normScale(realSpectrum2(:, col), 1, 0);
    tmp0(tmp0 < mean(tmp0(:))) = 0;
    realSpectrum2(:, col) = tmp0;
end

runonce = 1;
plot_q = 0;

wl_offset_best_chunks_m = zeros(chunkCount, 1);

tic

parfor chunkNum = 1:chunkCount % chunkCount-5:chunkCount
    
    
    range_lowR = chunkLeftEdges(chunkNum):chunkRightEdges(chunkNum);
    range_hiR = indind((indClosestVal(wl_hiR_m, wl_lowR_m(range_lowR(1)))) - 1) : (indind(indClosestVal(wl_hiR_m, wl_lowR_m(range_lowR(end))) + 1));
    range_hiR_wide = (indind(indClosestVal(wl_hiR_m, wl_lowR_m(range_lowR(1)))) - 451) : (indind(indClosestVal(wl_hiR_m, wl_lowR_m(range_lowR(end)))) + 451);
    range_hiR_wide(range_hiR_wide < 1 | range_hiR_wide > length(wl_hiR_m)) = [];
    
    if sum(sum(realSpectrum(:, range_lowR))) == 0
        wl_offset_best_chunks_m(chunkNum, 1) = nan;
        
    else
        
        ub = 2e-11;
        lb = -2e-11;

        
        fun2 = @(x) solveForDispersion_func2(realSpectrum2(:, range_lowR), wl_hiR_m(range_hiR_wide), Y_px, C_best_px2, tau_best_px2, x(1), ...
            WL_hiR_log_gridded_comb_m(:, range_hiR_wide), Y_hiR_gridded_comb_px(:, range_hiR_wide), WL_beam_log_gridded_m(:, range_lowR), Y_beam_log_gridded_0_px(:, range_lowR), ...
            LSF, flux_sun_pxv(:, range_hiR_wide), 0);
        
        
        left0 = lb;
        right0 = ub;
        
        lengthInit = 20;
        lengthSub = 20;
        wl_offset_best_chunks_m(chunkNum, 1) = 0;
        iter = 0;
        edgeFail = 0;
        leftEdgeFail = 1;
        rightEdgeFail = 1;
        saveCount = 0;
        LSQsaveArray = nan(lengthInit * 2, 2);
        
        precision0 = 1e-12;
        
        while (right0 - left0) > precision0 | leftEdgeFail == 1 | rightEdgeFail == 1
            
            iter = iter + 1;
            c = 0;
            if iter == 1 | leftEdgeFail == 1 | rightEdgeFail == 1
                LSQinputs = linspace(left0, right0, lengthInit);
            else
                LSQinputs = linspace(left0, right0, lengthSub);
            end
            lsq_iter_val = nan(1, length(LSQinputs));
            
            c = 0;
            forceCheck = 0;
            forceRecheckCount = 0;
            skip0 = 0;
            skipReset = 0;
            superSkip0 = 0;
            lastSign = 0;
            
            while c < length(LSQinputs)
                
                c = c + 1;
                
                
                %%
%                 [lsq_iter_val(1, c), clip0] = fun2(LSQinputs(c));
%                 
%                 if runonce
%                     figure(1); clf(figure(1));
%                     subplot(311)
%                     a2 = imagesc(realSpectrum(:, range_lowR));
%                     subplot(312)
%                     b2 = imagesc(clip0);
%                     subplot(313)
%                     c2 = imagesc((mat2gray(realSpectrum(:, range_lowR)) - mat2gray(clip0)));
%                     colormap gray; pause(0.001);
%                 else
%                     %                                 figure(1);
%                     set(a2, 'CData', realSpectrum(:, range_lowR))
%                     set(b2, 'CData', clip0)
%                     set(c2, 'CData', (mat2gray(realSpectrum(:, range_lowR)) - mat2gray(clip0)))
%                 end
                %%
                
                if c >122
                    1;
                end
                
                if c > round(length(LSQinputs) / (20 * (forceRecheckCount / 4 + 1))) + 1
                    
                    if superSkip0 ~=0 & superSkip0 <= round(length(LSQinputs) / (20 * (forceRecheckCount / 4 + 1))) + 1
                        % Skip next iteration
                        skip0 = skip0 + 1;
                        superSkip0 = superSkip0 + 1;
                        
                    elseif skip0 ~= 0 & skip0 <= round(length(LSQinputs) / (50 * (forceRecheckCount / 4 + 1))) + 1
                        
                        % Skip next iteration
                        skip0 = skip0 + 1;
                        
                    else
                        
                        if isnan(lsq_iter_val(1, c))
                            
                            % See if we already used and calc using a input within the precision bound
                            indSave = indClosestVal(LSQsaveArray(:, 2), LSQinputs(c));
                            saveDiff = LSQsaveArray(indSave(1), 2) - LSQinputs(c);
                            
                            if abs(saveDiff) < precision0 / length(LSQinputs)
                                
                                lsq_iter_val(1, c) = LSQsaveArray(indSave(1), 1);
                            
                            else
                                [lsq_iter_val(1, c), clip0] = fun2(LSQinputs(c));
                                
                                % Save all iterations
                                saveCount = saveCount + 1;
                                LSQsaveArray(saveCount, [1,2]) = [lsq_iter_val(1, c), LSQinputs(c)];
                            end
                            

                            
                            if plot_q
                                if runonce
                                    figure(1); clf(figure(1));
                                    subplot(311)
                                    a2 = imagesc(realSpectrum(:, range_lowR));
                                    subplot(312)
                                    b2 = imagesc(clip0);
                                    subplot(313)
                                    c2 = imagesc((mat2gray(realSpectrum(:, range_lowR)) - mat2gray(clip0)));
                                    colormap gray; pause(0.001);
                                else
                                    %                                 figure(1);
                                    set(a2, 'CData', realSpectrum(:, range_lowR))
                                    set(b2, 'CData', clip0)
                                    set(c2, 'CData', (mat2gray(realSpectrum(:, range_lowR)) - mat2gray(clip0)))
                                end
                            end
                        end
                        
                        
                        
                        if forceCheck == 0
                            
                            if skip0 > 0 & sign((lsq_iter_val(1, c) - lsq_iter_val(1, c - (1 + skip0)))) ~= lastSign & sign((lsq_iter_val(1, c) - lsq_iter_val(1, c - (1 + skip0)))) == 1 % We skipped a point or more before finding a negative slope. Go back and check the skipped point.
                                forceCheck = skip0 + 3;
                                c = c - skip0 - 3;
                                skip0 = 0;
                                superSkip0 = 0;
                                forceRecheckCount = forceRecheckCount + 1;
                                skipReset = 1;
                                
                            elseif skip0 > 0
                                skip0 = 0;
                                superSkip0 = 0;
                                forceCheck = 0;
                                skipReset = 1;
                                
                            else
                                forceCheck = 0;
                                superSkip0 = 0;
                                skipReset = 0;
                            end
                        else
                            forceCheck = forceCheck - 1;
                            lastSign = sign((lsq_iter_val(1, c) - lsq_iter_val(1, c - 1)));
                        end
                        
                        if forceCheck == 0
                            
                            if lsq_iter_val(1, c) > nanmin(lsq_iter_val) & skip0 == 0 & skipReset == 0 & sign((lsq_iter_val(1, c) - lsq_iter_val(1, c - (1 + skip0)))) == lastSign
                                superSkip0 = 1;
                                skip0 = 1;
                                
                            elseif sign((lsq_iter_val(1, c) - lsq_iter_val(1, c - (1 + skip0)))) == lastSign & skip0 == 0 & skipReset == 0
                                skip0 = 1;
                                
                            elseif sign((lsq_iter_val(1, c) - lsq_iter_val(1, c - 1))) == lastSign & skip0 == 0
                                skip = 1;
                                
                            elseif sign((lsq_iter_val(1, c) - lsq_iter_val(1, c - (1 + skip0)))) == -1*lastSign & ...
                                    isnan(sum(lsq_iter_val(1, (c - 1 - round(length(LSQinputs) / (50 * (forceRecheckCount + 1))) - 1):c))) & ...
                                    sign((lsq_iter_val(1, c) - lsq_iter_val(1, c - (1 + skip0)))) == 1
                                
                                skipSize = round(length(LSQinputs) / (50 * (forceRecheckCount / 4 + 1))) + 1;
                                superSkipSize = round(length(LSQinputs) / (20 * (forceRecheckCount / 4 + 1))) + 1;
                                
                                if superSkipSize == sum(isnan(lsq_iter_val(1, (c - superSkipSize - 1) : c))) & superSkipSize > 1
                                    forceCheck = superSkipSize + 3;
                                    c = c - (superSkipSize + 3);
                                    forceRecheckCount = forceRecheckCount + 1;
                                    
                                elseif skipSize > 1
                                    forceCheck = skipSize + 3;
                                    c = c - (skipSize + 3);
                                    forceRecheckCount = forceRecheckCount + 1;
                                end
                                skip0 = 0;
                                superSkip0 = 0;
                                
                            elseif skipReset == 0
                                lastSign = sign((lsq_iter_val(1, c) - lsq_iter_val(1, c - 1)));
                            end
                        end
                    end
                else

                    [lsq_iter_val(1, c), clip0] = fun2(LSQinputs(c));
                    
                    % Save all iterations
                    saveCount = saveCount + 1;
                    LSQsaveArray(saveCount, [1,2]) = [lsq_iter_val(1, c), LSQinputs(c)];
                    
                    
                    if plot_q
                        if runonce
                            figure(1); clf(figure(1));
                            subplot(311)
                            a2 = imagesc(realSpectrum(:, range_lowR));
                            subplot(312)
                            b2 = imagesc(clip0);
                            subplot(313)
                            c2 = imagesc((mat2gray(realSpectrum(:, range_lowR)) - mat2gray(clip0)));
                            colormap gray; pause(0.001);
                        else
                            %                         figure(1);
                            set(a2, 'CData', realSpectrum(:, range_lowR))
                            set(b2, 'CData', clip0)
                            set(c2, 'CData', (mat2gray(realSpectrum(:, range_lowR)) - mat2gray(clip0)))
                        end
                    end
                    
                    if c ~= 1
                        lastSign = sign((lsq_iter_val(1, c) - lsq_iter_val(1, c - 1)));
                    end
                end
                
                if showLSQProcessPlots_q & skip0 <= 1
                    
%                     lsqIteration_genericPlot(c, LSQinputs, lsq_iter_val)
                end
            end
            
            
            
            [~, ind] = min(lsq_iter_val);
            try
                wl_offset_best_chunks_m(chunkNum, 1) = LSQinputs(ind(1));
            catch
            end
            
            
            try
                left0 = LSQinputs(ind - 1);%round(length(LSQinputs) / 4));
                leftEdgeFail = 0;
            catch
                left0 = LSQinputs(1) - nanmean(diff(LSQinputs(1:round(length(LSQinputs) / 2)))) * 10;
                leftEdgeFail = 1;
            end
            
            if leftEdgeFail == 1
                right0 = LSQinputs(2);
                rightEdgeFail = 0;
            else
                try
                    right0 = LSQinputs(ind + 1);%round(length(LSQinputs) / 4));
                    rightEdgeFail = 0;
                catch
                    right0 = LSQinputs(end) + nanmean(diff(LSQinputs((end - round(length(LSQinputs) / 2)):end))) * 10;
                    rightEdgeFail = 1;
                end
            end
            
            if rightEdgeFail
                
                left0 = LSQinputs(end - 1);
            end
        end
        
        if plot_q
            if runonce == 0
                [~, clip0] = fun2(wl_offset_best_chunks_m(chunkNum, 1));
                set(a2, 'CData', realSpectrum(:, range_lowR))
                set(b2, 'CData', clip0)
                set(c2, 'CData', (mat2gray(realSpectrum(:, range_lowR)) - mat2gray(clip0)))
            end
        end
    end
end


save('E:\Research\1a_syntheticPipelineTestEnv\wlOffsetChunks_SolvedData.mat', 'wl_offset_best_chunks_m')




load('E:\Research\1a_syntheticPipelineTestEnv\wlOffsetChunks_SolvedData.mat', 'wl_offset_best_chunks_m')


star_spectrum_log_deslanted = (zeros(size(realSpectrum)));
star_spectrum_log_deslanted3 = (zeros(size(realSpectrum)));

for chunkNum = 1:chunkCount
    
    
    range_lowR = chunkLeftEdges(chunkNum):chunkRightEdges(chunkNum);
    range_hiR = indind((indClosestVal(wl_hiR_m, wl_lowR_m(range_lowR(1)))) - 1) : (indind(indClosestVal(wl_hiR_m, wl_lowR_m(range_lowR(end))) + 1));
    range_hiR_wide = (indind(indClosestVal(wl_hiR_m, wl_lowR_m(range_lowR(1)))) - 451) : (indind(indClosestVal(wl_hiR_m, wl_lowR_m(range_lowR(end)))) + 451);
    range_hiR_wide(range_hiR_wide < 1 | range_hiR_wide > length(wl_hiR_m)) = [];
    
    
    
    % Create the comb
    combImg = createComb(wl_hiR_m(range_hiR_wide), Y_px, C_best_px2, tau_best_px2, wl_offset_best_chunks_m(chunkNum, 1), 0, 0);
%     figure; imagesc(combImg)
    
    % Multiply the comb and sun spectrum
    synthSpectrum = mat2gray(combImg) .* repmat(flux_sun_pxv(range_hiR_wide), size(combImg, 1), 1);
    %     figure; imagesc(synthSpectrum)
    
    % Convolve synth spectra with the PSF
    flux_hiR_ds_lsf_adu = conv2(synthSpectrum, LSF, 'same');
    %     figure; imagesc(flux_hiR_ds_lsf_adu)
    
    
    % Create gridded data for final spectrum downsample interpolation
    % star_spectrum_log_deslanted = gpuArray(zeros(26, 4001));
    
    star_spectrum_log_deslanted(:, range_lowR) = interp2( ...
        WL_hiR_log_gridded_comb_m(:, range_hiR_wide), Y_hiR_gridded_comb_px(:, range_hiR_wide), flux_hiR_ds_lsf_adu, ...
        WL_beam_log_gridded_m(:, range_lowR), Y_beam_log_gridded_0_px(:, range_lowR), 'linear', 0);
    

end

star_spectrum_log_deslanted2 = star_spectrum_log_deslanted;
star_spectrum_log_deslanted2(:, chunkRightEdges) = 0;


for col = 1:4001
    
    tmp0 = normScale(star_spectrum_log_deslanted(:, col), 1, 0);
    tmp0(tmp0 < mean(tmp0(:))) = 0;
    star_spectrum_log_deslanted3(:, col) = tmp0;
end


% figure(78444); clf(figure(78444)); imagesc([mat2gray(realSpectrum(yRangeUsed, 300:3700)) ; (mat2gray([star_spectrum_log_deslanted(yRangeUsed, 300:3700)])) ;  (mat2gray(realSpectrum(yRangeUsed, 300:3700)) - (mat2gray([star_spectrum_log_deslanted(yRangeUsed, 300:3700)])))]); colormap gray
figure(78444); clf(figure(78444)); 

% subplot(211)
imagesc([mat2gray(realSpectrum(yRangeUsed, :)) ; mat2gray(star_spectrum_log_deslanted2(yRangeUsed, :)) ; star_spectrum_log_deslanted(yRangeUsed, :) ;  (mat2gray(realSpectrum(yRangeUsed, :)) - (mat2gray([star_spectrum_log_deslanted(yRangeUsed, :)])))]); colormap gray

% subplot(212)


figure(44); clf(figure(44)); imagesc(star_spectrum_log_deslanted3); colormap gray


figure(178444); clf(figure(178444)); hold on
titlel('Dispersion Measurment')
plot(chunkCenters_m * 1e10, wl_offset_best_chunks_m * 1e10, '.k')
xlabel('$\lambda [\AA]$')
ylabel('d$\lambda [\AA]$')
grid on
hold off





figure(78444); clf(figure(78444)); 
imagesc([mat2gray(realSpectrum(yRangeUsed, 300:3700)) ; mat2gray(star_spectrum_log_deslanted(yRangeUsed, 300:3700))]); colormap gray




