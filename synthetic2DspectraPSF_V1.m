function PSF = synthetic2DspectraPSF_V1(wl)

nu = 3e8 ./ wl; %convert wavelength into frequency

R = 12000;

nu_0 = nu(1);

ii = 0;
for nu_n = nu
    
    ii = ii + 1;
    
    dNu = nu_n / R / 2.35;
    
%     dNu2 = nu_n^2 / R / 2.35;
    
    A = 1 / (2 * pi * nu_n.^2);
    
    B = exp( - (nu_n - nu_0)^2 / (2 * dNu^2) );
    
    PSF(ii) = A * B;
end
 
% PSF = exp(-1 .* (2 .* pi * fre_mean * (1:length(wl))).^2 / 2 ./ (2.35 * resolution)^2);





