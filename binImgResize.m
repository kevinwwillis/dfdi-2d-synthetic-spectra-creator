function img2 = binImgResize(img, X_px, Y_px, X_up_px, Y_up_px, xUpsample, yUpsample)
% function img2 = binImgResize(img, X_px, Y_px, X_up_px, Y_up_px, X_up_ind, Y_up_ind, xUpsample, yUpsample)

tic

img2 = nan(Y_px(end), X_px(end));



% for y=1, xr_1 = -5:5, xr2=-4:6

xr_ind = repmat( ((-xUpsample/2) : (xUpsample/2)), length(X_px), 1) + repmat((((X_px-1) * xUpsample) + 1)', 1, (xUpsample+1));

xr_ind = arrayfun(@(x) {xr_ind(x, (xr_ind(x, :) > 0 & xr_ind(x, :) <= length(X_up_px)))}, X_px)';


yr_ind = repmat( ((-yUpsample/2) : (yUpsample/2)), length(Y_px), 1) + repmat((((Y_px-1) * yUpsample) + 1)', 1, (yUpsample+1));

yr_ind = arrayfun(@(x) {yr_ind(x, (yr_ind(x, :) > 0 & yr_ind(x, :) <= length(Y_up_px)))}, Y_px)';

lxh = length(X_up_px);
lyh = length(Y_up_px);
for x = X_px
    
    for y = Y_px
        
        img2(y, x) = nanmean(nanmean(img(yr_ind{y}, xr_ind{x})));
    end
    
%     img2(:, x) = mat2gray(img2(:, x));
    
%     figure(11); clf(figure(11)); hold on;
%     subplot(311)
%     imagesc(X_up_px, Y_up_px, img)
%     colormap(gray);
%     set(gca,'YDir','normal')
%     
%     subplot(312)
%     imagesc(X_px(1:x), Y_px, img2(:,1:x))
%     colormap(gray);
%     set(gca,'YDir','normal')
%     
%     subplot(313); hold on
%     plot(linspace(1, Y_px(end), length(img(:,x))), mat2gray(img(:,x)), '.-k')
%     plot(Y_px, mat2gray(img2(:,x)), '.-m')
%     
%     pause(0.01)
end
toc

figure(11); clf(figure(11)); hold on;
subplot(211)
imagesc(X_up_px, Y_up_px, img)
colormap(gray);
set(gca,'YDir','normal')

subplot(212)
imagesc(X_px, Y_px, img2)
colormap(gray);
set(gca,'YDir','normal')

1;

