% Create the master comb image used in 2D synthetic spectra creation

clearvars

load('wlsoln.mat', 'wlsoln')

load('sun_WL_ang.mat', 'WL_ang')
load('E:\Research\MARVELS_software\synthSpectraMaker_2D\sun_flux_pxv.mat', 'flux_pxv')

[smoothedProfile, ~] = getSpectraProfile(fitsread('E:\Research\1a_syntheticPipelineTestEnv\39_deslant_sun.fits'), 1);
profileTemplate = smoothedProfile; clear smoothedProfile

%%
plots_q = 0;

downsample = 10;

% tau_1 = 0.0147;% Determines comb line separation
tau_1 = 0.0147;

% C = 6.9555e-08;
C = 6.5555e-08; % Higher = smaller slope (less steep); Determines fringe density

%%


maxWL_ang = max(max(wlsoln(:,1,:)));
minWL_ang = min(min(wlsoln(:,1,:)));

wl_example_m = squeeze(wlsoln(:,1,1))' .* 1e-10;

clear wlsoln

%%
dWL_sun_ang = nanmean(diff(WL_ang));

wl_m = ( floor(minWL_ang) :dWL_sun_ang * downsample: ceil(maxWL_ang) ) * 1e-10;
flux_pxv = spline(WL_ang, flux_pxv, wl_m * 1e10);
 
clear WL_ang

R = round(wl_m(1) / (wl_m(2) - wl_m(1)));

Y_px = 1 :0.05: 30;



%% Predicted values
    
% Comb line period calculated
n_y1 = ceil(2 * (C + tau_1) ./ wl_m + 1 / 2);
wl_zro1 = 4 * tau_1 ./ (2 * n_y1 - 1);
wl_zro2 = 4 * tau_1 ./ (2 * (n_y1 - 1) - 1);
combLineSep_m = 2 * (wl_zro2 - wl_zro1); clear wl_zro2 wl_zro1 n_y1

dWL = diff(wl_example_m);
dWL = [dWL(1), dWL];
combLineSep_px = 1 ./ (dWL ./ spline(wl_m', combLineSep_m', wl_example_m')');

figure(31); clf(figure(31)); plot(combLineSep_px); grid on

%%
range2 = 1:4001;
% range2 = 1500:4001;
indind = 1:length(wl_m);
if range2(1) == 1 & range2(end) == 4001
   range0 = indind;
else
    
end
clear indind


Y0 = ( tau_1 + C .* (Y_px - 1) )';

combImg = zeros(length(Y0), length(wl_m));
parfor rowInd = 1:length(Y0)
    
    combImg(rowInd, range0) = cos( 2 * pi * Y0(rowInd) ./ (wl_m(range0) - 5e-10) );
end

combImg = flipud(combImg);

chunkWidth = 1000;

%% Add atmospheric turbulance to the original specrum
% [PSF, ~] = generateVariantLSF(260000, 0.001, wl_m * 1e10, chunkWidth)
% flux_lsf_pxv = flux_pxv;
[PSF, ~] = generateVariantLSF(100000, 600000, wl_m * 1e10, chunkWidth);
flux_lsf_pxv(:, range0) = convSV_LSF(flux_pxv(:, range0), PSF);


%%
flux_hiR_id_adu(:, range0) = mat2gray(combImg(:, range0)) .* repmat(flux_lsf_pxv(:, range0), size(combImg, 1), 1); %clear flux_pxv combImg Y0
% figure(1); imagesc(flux_hiR_id_adu); colormap gray
%%
% G_LSF = createLSF(wl_example_m * 1e10, wl_m * 1e10, 10935);
[PSF, ~] = generateVariantLSF(11000, 43000, wl_m * 1e10, chunkWidth);

%%
% Convolve synth spectra with the PSF
% flux_hiR_ds_lsf_adu(:, range0) = conv2(flux_hiR_id_adu(:, range0), G_LSF, 'same');
% flux_hiR_ds_lsf_adu = flux_hiR_id_adu;
tic
flux_hiR_ds_lsf_adu(:, range0) = convSV_LSF(flux_hiR_id_adu(:, range0), PSF);
toc
% figure(1); imagesc(flux_hiR_ds_lsf_adu); colormap gray

%%  

% Create gridded data for final spectrum downsample interpolation
[WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px] = meshgrid(wl_m, Y_px);

% Create gridded data for final spectrum downsample interpolation
[WL_beam_log_gridded_m, Y_beam_log_gridded_0_px] = meshgrid(wl_example_m, 1:26);

star_spectrum_log_deslanted = zeros(26, 4001);
star_spectrum_log_deslanted(:, range2) = interp2( ...
    WL_hiR_log_gridded_comb_m(:, range0), Y_hiR_gridded_comb_px(:, range0), flux_hiR_ds_lsf_adu(:, range0), ...
    WL_beam_log_gridded_m(:, range2), Y_beam_log_gridded_0_px(:, range2), 'spline', nanmean(flux_hiR_ds_lsf_adu(:)));
% figure(1); imagesc(star_spectrum_log_deslanted); colormap gray

clear WL_hiR_log_gridded_comb_m Y_hiR_gridded_comb_px WL_beam_log_gridded_m Y_beam_log_gridded_0_px

%%
star_spectrum_log_deslanted(:, range2) = profileTemplate(:, range2) .* normScale(star_spectrum_log_deslanted(:, range2), 0.1, 1.1);

%%
noiseLevel = 1e12/2;
star_spectrum_log_deslanted(:, range2) = imnoise(star_spectrum_log_deslanted(:, range2) .* 1/noiseLevel, 'poisson') .* noiseLevel;

%%
realAndSynthComparison([], [], [], [], [], star_spectrum_log_deslanted, 0)







