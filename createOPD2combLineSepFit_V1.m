% function createOPD2combLineSepFit_V1




%% Params

clearvars;


xUpsample_px = 10;

Y_px = 1:25;                          % Choose pixel height of each spectra

X_px = 1:4001;                        % Choose pixel width of each spectra

beamNum = 101;

obsNum = 17;




%% x-axis spatial data

% Upsampled and padded
X_up_pad_px = 3980 :1/xUpsample_px: X_px(end);
Y_up_pad_px = 1 :1/xUpsample_px: Y_px(end);

load(strcat('E:\Research\1D_pipeline_orig\inputs\wlsoln.mat'));
%% Spatial Matricies
for beamNum = 1:120
    
    beamNum
    
    
    wl_up_pad_m = interp1(X_px, squeeze(wlsoln(:, 1, beamNum)) * 1e-10, X_up_pad_px, 'linear');
    wl_up_pad_m = repmat(wl_up_pad_m, length(Y_up_pad_px), 1);
   
    %%
    
    C_all = 5e-8 :1e-9: 1e-7;
    tau_1_all = 2.5e-3 :2.5e-4: 5e-2;
    
    B_dRADdM = 2 .* pi ./ wl_up_pad_m;
    
    combLineSep_px = nan(1, length(C_all)*length(tau_1_all));
    fringePeriod_px = nan(1, length(C_all)*length(tau_1_all));
    
%     ii = 0;
%     for C = C_all
%         for tau_1 = tau_1_all
%             ii = ii + 1;
%             
%             combImg = cos(B_dRADdM .* repmat(((tau_1 + C .* (Y_up_pad_px-1)))', 1, length(X_up_pad_px)) );
%             
%             pp = spline(X_up_pad_px, combImg(1, :));
%             [maxima, minima] = splineMaximaMinima(pp);
%             combLineSep_px(1,ii) = nanmean([nanmean(diff(maxima)), nanmean(diff(minima))]);
%             
%             for col = 1:5
%                 pp = spline(Y_up_pad_px, combImg(:, 1+col));
%                 [maxima, minima] = splineMaximaMinima(pp);
%                 fringePeriod_px_tmp(1,col) = nanmean([nanmean(diff(maxima)), nanmean(diff(minima))]);
%             end
%             fringePeriod_px(1,ii) = nanmean(fringePeriod_px_tmp(1,col));
%             
%             
%             figure(22); clf(figure(22));
%             
%             subplot(311); hold on
%             imagesc(X_up_pad_px, Y_up_pad_px, combImg)
%             
%             subplot(312); hold on
%             title('Comb Line Sep.')
%             plot(combLineSep_px(1, 1:ii), '-k')
%             
%             subplot(313); hold on
%             title('Fringe period')
%             plot(fringePeriod_px, '-k')
%             
%             pause(0.01)
%         end
%     end
    
    
        ii = 0;
        tau_1 = tau_1_all(20);
        for C = C_all
            
            ii = ii + 1;
            
            combImg = cos(B_dRADdM .* repmat(((tau_1 + C .* (Y_up_pad_px-1)))', 1, length(X_up_pad_px)) );
            
%             pp = spline(X_up_pad_px, combImg(1, :));
%             [maxima, minima] = splineMaximaMinima(pp);
%             combLineSep_px(1,ii) = nanmean([nanmean(diff(maxima)), nanmean(diff(minima))]);
            
            for col = 1:5
                pp = spline(Y_up_pad_px, combImg(:, 1+col));
                [maxima, minima] = splineMaximaMinima(pp);
                fringePeriod_px_tmp(1,col) = nanmean([nanmean(diff(maxima)), nanmean(diff(minima))]);
            end
            fringePeriod_px(1,ii) = nanmean(fringePeriod_px_tmp(1,col));
            
            
%             figure(22); clf(figure(22));
%             
%             subplot(211); hold on
%             imagesc(X_up_pad_px, Y_up_pad_px, combImg)
%             
%             subplot(212); hold on
%             title('Fringe period')
%             plot(fringePeriod_px, '-k')
%             
%             pause(0.01)
        end
    
    ii = 0;
    C = C_all(1);
    for tau_1 = tau_1_all
        ii = ii + 1;
        
        combImg = cos(B_dRADdM .* repmat(((tau_1 + C .* (Y_up_pad_px-1)))', 1, length(X_up_pad_px)) );
        
        pp = spline(X_up_pad_px, combImg(1, :));
        [maxima, minima] = splineMaximaMinima(pp);
        combLineSep_px(1,ii) = nanmean([nanmean(diff(maxima)), nanmean(diff(minima))]);
        
%         for col = 1:5
%             pp = spline(Y_up_pad_px, combImg(:, 1+col));
%             [maxima, minima] = splineMaximaMinima(pp);
%             fringePeriod_px_tmp(1,col) = nanmean([nanmean(diff(maxima)), nanmean(diff(minima))]);
%         end
%         fringePeriod_px(1,ii) = nanmean(fringePeriod_px_tmp(1,col));
        
        
%         figure(22); clf(figure(22));
%         
%         subplot(211); hold on
%         imagesc(X_up_pad_px, Y_up_pad_px, combImg)
%         
%         subplot(212); hold on
%         title('Comb Line Sep.')
%         plot(combLineSep_px(1, 1:ii), '-k')
%         
%         pause(0.01)
    end
    
    goodInd = ~isnan(combLineSep_px);
    combLineSep_px = combLineSep_px(goodInd);
    
    combLineSepFit{beamNum, 1} = fit(combLineSep_px', tau_1_all(goodInd)', 'spline');
    
    
    goodInd = ~isnan(fringePeriod_px);
    fringePeriod_px = fringePeriod_px(goodInd);
    
    fringePeriodFit{beamNum, 1} = fit(fringePeriod_px', C_all(goodInd)', 'spline');
    
    
    
    
%     figure(22); clf(figure(22)); 
%     subplot(211); hold on
%     title('Comb Line Sep.')
%     plot(O_range_m, period0_px, '-k')
%     plot(feval(combSepFit{beamNum, 1}, period0_px), period0_px, ':m')
%     subplot(212); hold on
%     title('Resid')
%     plot(O_range_m, abs(feval(combSepFit{beamNum, 1}, period0_px)' - O_range_m))
    
end

save('combLineSepFit.mat', 'combLineSepFit')
save('fringePeriodFit.mat', 'fringePeriodFit')
