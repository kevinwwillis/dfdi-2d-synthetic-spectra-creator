function I = createSyntheticComb_V2(imgWidth, imgHeight, wl)

%% Testing Params

% imgHeight = 24;
% 
% imgWidth = 4001;
% 
% beamNum = 1;
% 
% load('E:\Research\1D_pipeline_orig\inputs\wlsoln.mat', 'wlsoln')



%% Spatial Matricies

row_pixel_height = imgHeight;

row_pixel_range = (1 : row_pixel_height)';

col_pixel_range = 1 : imgWidth;

col_pixel_matrix = repmat(col_pixel_range, row_pixel_height, 1);

lambda = wl; %squeeze(wlsoln(:, 1, beamNum))' .* 1e-10;                            % Create equally spaced series of wavelengths in meters

lambda_matrix = repmat(lambda, row_pixel_height, 1);                  % Replicate the row of wavelengths to image height

%% Unused functions

% OPD = d ./ 2;

% d_m = lambda ./ (2 .* atan(opd1/opd2));                                        % Vertical fringe density (sine period)

%% Optical Path distance (OPD) at each physical pixel location.

% alpha_x_degrees = -0.000005;                                          % Mirror x-axis angle (degrees)
% 
% alpha_x_rad = alpha_x_degrees * 2 * pi / 180;                     % Mirror x-axis angle (rad)
% 
% OPD_x = col_pixel_range .* tan(alpha_x_rad);                      % x-axis optical distance
% 
% OPD = repmat(OPD_x, row_pixel_height, 1);                             % Replicate vector


opd1 = 7.4 * 1e-3;
for opd1 = 0 :0.01: 1
opd2 = 1e-4;

alpha_y_degrees = 0.9;                                          % Mirror y-axis angle (degrees)

alpha_y_rad = alpha_y_degrees * 2 * pi / 180;                     % Mirror y-axis angle (rad)

OPD_y = row_pixel_range .* tan(alpha_y_rad);                      % y-axis optical distance

OPD = repmat(OPD_y, 1, imgWidth);                                   % Replicate vector and add xy OPDs

OPD_y = linspace(opd1, opd2, row_pixel_height)';

OPD = repmat(OPD_y, 1, imgWidth);                                   % Replicate vector

%% Intensity Calculations

d_phi = (2 .* pi ./ lambda_matrix) .* OPD;                            % EM wave recombination phase offset

% phiPrime = repmat((row_pixel_range - 1) / 10, 1, imgWidth);               % (Not used)Row-wise phase offset (Estimation)

I_1 = 0.1;                                                              % Split beam 1 intesity

I_2 = 0.1;                                                              % Split beam 2 intesity

I = I_1 + I_2 + 2 .* (I_1 .* I_2).^(1/2) .* cos(d_phi);               % Intensity of recombined beams

%% Plotting


figure(1); clf(figure(1))
imagesc(I);set(gca,'YDir','normal'); colormap(gray)
xlim([3900 4001])
ylabel('Row # [px.]')
xlabel('Col # [px.]')

figure(2); clf(figure(2)); hold on
p1 = plot(row_pixel_range, I(:,1), '.-k', 'displayname', 'First col');
p2 = plot(row_pixel_range, I(:,imgWidth), '.-r', 'displayname', 'Last col');
legend([p1 p2])
xlabel('Row # [px.]')
ylabel('Intensity')

pause(0.001)
end
%%