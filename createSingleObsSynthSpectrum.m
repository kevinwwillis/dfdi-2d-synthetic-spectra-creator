function [] = createSingleObsSynthSpectrum(s, pipeline_dir, plateName, beamNum, obsNum, type0, plateEmulationName, templateBeam, ...
    wl_hiR_log_comb_ang, wl_beams_ang, wl_beam_log_ang, wl_hiR_wide_log_ang, wl_beam_log_gridded_ang, dWL_dispersion_ang, wl_hiR_log_gridded_comb_ang, ...
    flux_hiR_wide_adu, flux_hiR_comb_adu, ...
    RV_BCV_mps, dX_instDrift_px, profileTemplate, noiseLevel, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable, ...
    X_hiR_wide_px, ...
    Y_beam_px, Y_beam_log_gridded_0_px, Y_hiR_gridded_comb_px, Y_px, ...
    spectra_deslanted_orig_dir, ...
    addDispersion_q, useFullBeamDispersion_q, applyDispersion_q, applyAtmTurbBlur_q, binningDownsample_q, matchMARVELSprofile_q, addNoise_q, applyVariableLSF_q, GPU_calulations_q, ...
    realComparisonPlot_q, obsDispersionPlot_q, stageContinuumPlot_q, finalSpectraPlot_q, stageImagePlot_q)


load(strcat(pipeline_dir, s, 'MARVELS_fileNames.mat'), 'spectrum_deslanted_fileName');

beamNumStr = n2s(templateBeam);
obsNumStr = n2s(obsNum);

c = c_mps;

if addDispersion_q == 1 & useFullBeamDispersion_q == 1 & applyDispersion_q == 1
    
    %dWL_dipersionRespampled_ang = spline(dispersionPlate_wlsoln(5, :), dWL_dispersion_ang(obsNum, :), wl_beams_ang(1, :));
    
    
    % Create dispersed WL final sample array
    %WL_beam_log_gridded_dispersed_ang = log( exp(WL_beam_log_gridded_ang) + repmat(dWL_dipersionRespampled_ang, Y_beam_px(end), 1) );
    WL_beam_log_gridded_dispersed_ang = log( exp(wl_beam_log_gridded_ang) + repmat(dWL_dispersion_ang(obsNum, :), Y_beam_px(end), 1) );
    
    
    % Plot & save disperison profile
    if obsDispersionPlot_q
        figure(81); clf(figure(81)); hold on;
        titlel(cat(2, {cat(2, type0, ' Dispersion')}, {cat(2, 'Plate: ', underscoreTEXfixer(plateName), ' | Beam: ', beamNumStr, ' | Obs: ', obsNumStr)}))
        plot(1:4001, (exp(WL_beam_log_gridded_dispersed_ang(1, :)) - exp(wl_beam_log_gridded_ang(1, :))), '-k');
        xlabel('px')
        ylabel('d$\lambda$ [\AA]')
        grid on
        hold off
        
        plot_dispersion_dir = strcat(pipeline_dir, s, 'plates', s, plateName, s, 'plots', s, 'synthSpectrumCreationPlots', s, 'dispersion');
        try mkdir(plot_dispersion_dir); catch; end
        print(gcf, '-r0', '-dpng', strcat(plot_dispersion_dir, s, upper(type0), '_beam', beamNumStr, '_obs', num2str(obsNum), '_dispersionPlot.png'))
    end
end



%% Determine Doppler shifted WLs
if strcmp(upper(type0), 'STAR')
    
    % Calc WL shift from doppler formula. Only use these to find pixel shift.
    if RV_BCV_mps(obsNum) == 0
        wl_hiR_wide_log_ds_ang = wl_hiR_wide_log_ang;
        
        wl_beam_log_shifted_ang = wl_beam_log_ang;
        
    else
        wl_hiR_wide_log_ds_ang = wl_hiR_wide_log_ang + log(1 + RV_BCV_mps(obsNum) / c);
        
        wl_beam_log_shifted_ang = wl_beam_log_ang + log(1 + RV_BCV_mps(obsNum) / c);
    end
    
else
    % TIO has no Doppler shift.
    wl_hiR_wide_log_ds_ang = wl_hiR_wide_log_ang;
end



%% Apply TIO instrument drifts
if dX_instDrift_px(1, obsNum) ~= 0
    
    flux_hiR_wide_id_adu = spline(X_hiR_wide_px, flux_hiR_wide_adu, X_hiR_wide_px + dX_instDrift_px(1, obsNum));
else
    flux_hiR_wide_id_adu = flux_hiR_wide_adu;
end



%% Shift spectra by resampling the spectrum at the shifted wavelengths. (** Wide WL range eliminated here! **)
flux_hiR_log_id_ds_adu = interp1(wl_hiR_wide_log_ds_ang, flux_hiR_wide_id_adu, wl_hiR_log_comb_ang, 'spline', nanmean(flux_hiR_wide_adu));



%% Apply atmospheric turbulance blurring
if applyAtmTurbBlur_q
    
    % If obsNum is not = 1 and there was is no instrument drift in all observations, we can resuse the original solution here by skipping.
    flux_hiR_log_shifted_atmT_adu = convSV_LSF(flux_hiR_log_id_ds_adu, PSF_AtmTurb_chunks);
else
    flux_hiR_log_shifted_atmT_adu = flux_hiR_log_id_ds_adu;
end


%% Replicate rows to create 2D spectra
flux_hiR_log_shifted_atmT_2D_adu = repmat(flux_hiR_log_shifted_atmT_adu, size(flux_hiR_comb_adu, 1), 1);



%% Combine the comb and spectra images
flux_hiR_log_ds_id_atmT_comb_adu = flux_hiR_comb_adu .* flux_hiR_log_shifted_atmT_2D_adu;



%% Convolve synth spectra with the LSF
if applyVariableLSF_q
    
    flux_hiR_log_ds_id_atmT_comb_LSF_adu = convSV_LSF(flux_hiR_log_ds_id_atmT_comb_adu, LSF_inst_chunks);
    
else
    flux_hiR_log_ds_id_atmT_comb_LSF_adu = conv2(flux_hiR_log_ds_id_atmT_comb_adu, LSF_inst_invariable, 'same');
end

if GPU_calulations_q
    flux_hiR_log_ds_id_atmT_comb_adu = gather(flux_hiR_log_ds_id_atmT_comb_adu);
    flux_hiR_log_ds_id_atmT_comb_LSF_adu = gather(flux_hiR_log_ds_id_atmT_comb_LSF_adu);
end



%% Downsample spectrum
if addDispersion_q == 1
    
    if binningDownsample_q == 1
        flux_log_deslanted_adu = binImgResizeWL_V2(flux_hiR_log_ds_id_atmT_comb_LSF_adu, Y_px, WL_beam_log_gridded_dispersed_ang(1, :), wl_hiR_log_gridded_comb_ang(1, :));
    else
        flux_log_deslanted_adu = interp2( ...
            wl_hiR_log_gridded_comb_ang, Y_hiR_gridded_comb_px, flux_hiR_log_ds_id_atmT_comb_LSF_adu, ...
            WL_beam_log_gridded_dispersed_ang, Y_beam_log_gridded_0_px, 'spline', nanmean(flux_hiR_log_ds_id_atmT_comb_LSF_adu(:)));
    end
    
else
    if binningDownsample_q == 1
        flux_log_deslanted_adu = binImgResizeWL_V2(flux_hiR_log_ds_id_atmT_comb_LSF_adu, Y_px, wl_beam_log_gridded_ang(1, :), wl_hiR_log_gridded_comb_ang(1, :));
    else
        flux_log_deslanted_adu = interp2( ...
            wl_hiR_log_gridded_comb_ang, Y_hiR_gridded_comb_px, flux_hiR_log_ds_id_atmT_comb_LSF_adu, ...
            wl_beam_log_gridded_ang, Y_beam_log_gridded_0_px, 'spline', nanmean(flux_hiR_log_ds_id_atmT_comb_LSF_adu(:)));
    end
end



%% Conform to MARVELS illumination profile
if matchMARVELSprofile_q
    flux_log_deslanted_adu = profileTemplate .* normScale(flux_log_deslanted_adu, 0.1, 1);
end



%% Add noise to the image
if addNoise_q
    flux_log_deslanted_adu = imnoise(flux_log_deslanted_adu .* noiseLevel / 1e12, 'poisson') .* 1e12 / noiseLevel;
end



%% Create spectra save name
fitsSavePath = strcat(spectra_deslanted_orig_dir, s, upper(type0), '_beam', beamNumStr, '_obs', obsNumStr, spectrum_deslanted_fileName, '.fits');
matSavePath = strcat(spectra_deslanted_orig_dir, s, upper(type0), '_beam', beamNumStr, '_obs', obsNumStr, spectrum_deslanted_fileName, '.mat');


%% Save the spectra
fitswrite(flux_log_deslanted_adu, fitsSavePath)
flux_adu = flux_log_deslanted_adu;
save(matSavePath, 'flux_adu')


% Save WL solution
fileDir0 = strcat(pipeline_dir, s, 'plates', s, plateName, s, 'wlsoln_rest');
mkdir(fileDir0)
fileName0 = strcat(fileDir0, s, upper(type0), '_beam', beamNumStr, '_allObs_wlsoln.mat');
if 0 ~= exist(fileName0, 'file')
    load(fileName0, 'wl_allObs_ang')
else
    wl_allObs_ang = nan(obsNum, 4001);
end
wl_allObs_ang(obsNum, :) = WL_beam_log_gridded_dispersed_ang(1, :);

save(fileName0, 'wl_allObs_ang')


%% Star Plots

% Continuum plots
if stageContinuumPlot_q
    synthContinuumPlots_logspace(s, pipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
        wl_beam_log_ang, wl_hiR_wide_log_ang, wl_hiR_log_comb_ang, ...
        flux_hiR_wide_adu, flux_hiR_wide_adu, flux_hiR_log_shifted_atmT_2D_adu, flux_hiR_log_ds_id_atmT_comb_adu, flux_hiR_log_ds_id_atmT_comb_LSF_adu, flux_log_deslanted_adu, type0)
end

% Image plots
if stageImagePlot_q
    synthImagePlots_logspace(s, pipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
        wl_beam_log_ang, wl_hiR_wide_log_ang, wl_hiR_log_comb_ang, ...
        flux_hiR_wide_adu, flux_hiR_wide_adu, flux_hiR_log_shifted_atmT_2D_adu, flux_hiR_log_ds_id_atmT_comb_adu, flux_hiR_log_ds_id_atmT_comb_LSF_adu, flux_log_deslanted_adu, type0)
end

% Final spectrum continuum plot
if finalSpectraPlot_q
    synthFinalContinuumPlot_logspace(s, pipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
        wl_beam_log_ang, flux_log_deslanted_adu, type0)
end


if realComparisonPlot_q
    realAndSynthComparison(s, pipeline_dir, plateName, beamNumStr, obsNum, flux_log_deslanted_adu, 1, type0, 1, plateEmulationName, templateBeam)
end








