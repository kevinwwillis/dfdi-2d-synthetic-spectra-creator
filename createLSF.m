function LSF = createLSF(wl_lowR_ang, wl_hiR_ang, R)


% % center_wl_up_ind = round(length(WL_up) / 2);
% % 
% % R_up = WL_up(center_wl_up_ind) / (WL_up(center_wl_up_ind) - WL_up(center_wl_up_ind - 1));
% % 
% % wl_mid_ind = round(length(WL) / 2);
% % 
% % R = WL(wl_mid_ind) ./ (WL(wl_mid_ind) - WL(wl_mid_ind - 1));
% % 
% % dl = R_up / R;
% 
% % Hi-res sun data WL granularity
% dWL_dPX = nanmean(diff(WL_up));
% 
% % MARVELS spectral resolution for 3.25 pixel sampling per resolution element (??)
% R = 10935;
% 
% wl_wanted_1_m = round(WL(1) * 1e15) / 1e15;
% wl_wanted_2_m = round(WL(end) * 1e15) / 1e15;
% 
% center_WL_wanted_m = (wl_wanted_1_m + wl_wanted_2_m) / 2;
% 
% dl = center_WL_wanted_m / R / dWL_dPX;
% 
% %dl = 240.47;
% 
% sigma = dl / (2 * sqrt(2 * log(2)));
% 
% X_LSF = 1:900;
% % X_LSF = 1:length(WL_up);
% 
% mu = numel(X_LSF) / 2;
% 
% G_0 = 1 / (sigma * sqrt(2 * pi)) * exp(-(X_LSF - mu).^2 / (2 * sigma^2));
% 
% G_LSF = G_0 ./ sum(G_0);


% generate 1D PSF (Dr. Ge code)

% MARVELS spectral resolution for 3.25 pixel sampling per resolution element (??)
% R = 10935;
% R = 30000;

% Hi-res sun data WL granularity
% dWL_dPX = 0.002;

dWL_dPX = nanmean(diff(wl_hiR_ang));

wl_wanted_1_ang = round(wl_lowR_ang(1));
wl_wanted_2_ang = round(wl_lowR_ang(4001));

center_WL_wanted_ang = (wl_wanted_1_ang + wl_wanted_2_ang) / 2;

dl = center_WL_wanted_ang / R / dWL_dPX;

gaussianRange_x = 1:2001;

mu = 1000;

sigma = (dl / (2 * sqrt(2 * log(2))));

psf_1d = normpdf(gaussianRange_x, mu, sigma) / 0.4;

LSF = psf_1d / sum(psf_1d);



