function [synthImg, wl_instDrift_m] = createSingleSynthTIOspectra_V1(...
    ...
    X_px, Y_px, dX_instDrift_px, wl_m, spectralLineCenters_px, spectraLineIntensities, fringePeriod_px, plots)

% Create a TIO spectra synthetically


% clearvars

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% yPadding = 10;                      % Pad images for proper PSF convolution

% xPadding = 10;                      % Pad images for proper PSF convolution

yUpsample_px = 10;                     % Upsample for spectra height

xUpsample_px = 10;                     % Upsample for spectra width

combOpacityLow = 0;                 % Choose opacity lower limit of the synth comb

combOpacityHigh = 1;                % Choose opacity upper limit of the synth comb

% slOpacityLow = 0;                   % Choose opacity lower limit of the synth spectral lines

% slOpacityHigh = 1;                  % Choose opacity upper limit of the synth spectral lines

combEndLineSep_px = 1.6;               % Comb line separation at px 4000

sigmaBlur = 3 * round(mean([xUpsample_px, yUpsample_px]));

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

OS=computer; if strcmp(OS,'PCWIN64') || strcmp(OS,'PCWIN'); s='\'; else s='/'; end

% Define padding for images
pad_px = sigmaBlur * 2;


%% y-axis spatial data

imgHeight_px = Y_px(end);

% Padded
Y_pad_px = 1 - pad_px/xUpsample_px : Y_px(end) + pad_px/xUpsample_px;

% Upsampled and padded
Y_up_pad_px = Y_pad_px(1) :1/xUpsample_px: Y_pad_px(end);
Y_up_pad_ind = 1:length(Y_up_pad_px);

Y_withinPad_ind = pad_px : length(Y_up_pad_px) - (pad_px - 1);

%% x-axis spatial data

imgWidth_px = X_px(end);

% Padded
X_pad_px = 1 - pad_px/xUpsample_px : X_px(end) + pad_px/xUpsample_px;

% Upsampled and padded
X_up_pad_px = X_pad_px(1) :1/xUpsample_px: X_pad_px(end);

X_WithinPad_ind = pad_px : length(X_up_pad_px) - (pad_px - 1);

%%

% Create px to WL conversion scale
xToWl_pp = fit(X_px', wl_m', 'spline');
%wlToCol_pp = fit(squeeze(wl(:, beamNum, 1)), X_', 'spline');

% Create spectra
spectraImg_up_pad_1D = createSyntheticSpectralLines1D_V1(X_up_pad_px, spectralLineCenters_px, spectraLineIntensities);

% calc shifted tio WLs
wl_instDrift_m = feval(xToWl_pp, X_px - dX_instDrift_px);

% Upsample and pad the WLs
wl_up_pad_instDrift_m = interp1(X_px, wl_instDrift_m, X_up_pad_px, 'pchip');

% Create comb image
combImg_up_pad = createSyntheticComb_V6(X_px, Y_px, X_up_pad_px, Y_up_pad_px, wl_up_pad_instDrift_m, fringePeriod_px, combEndLineSep_px);

% Normalize comb image to desired scale
combImg_up_pad = normScale(combImg_up_pad, combOpacityLow, combOpacityHigh);

% Shift spectral image by the instrument drift
spectraImg_up_pad_1D_instDrift = interp1(X_up_pad_px, spectraImg_up_pad_1D, (X_up_pad_px - dX_instDrift_px), 'spline');

% Replicate the 1D spectra to create a 2D spectra
spectraImg_up_pad_2D_instDrift = repmat(spectraImg_up_pad_1D_instDrift, Y_up_pad_ind(end), 1);

% Combine comb and spectral line images
synthImg_up_pad_raw = combImg_up_pad .* spectraImg_up_pad_2D_instDrift;

% Convolve synth spectra with the PSF
synthImg_up_pad_PSF = gaussfiltGPU_V1(synthImg_up_pad_raw, sigmaBlur);

% Normalize the synth spectra and remove padding
synthImg_up_PSF_norm = mat2gray(synthImg_up_pad_PSF(Y_withinPad_ind, X_WithinPad_ind));

% Resize the synth spectra (downsample)
synthImg = imresize(synthImg_up_PSF_norm, [imgHeight_px, imgWidth_px], 'cubic', 'Antialiasing', false);


if plots
    if obsNum == 1
        syntheticImg_1 = synthImg;
    end
    
    figure(2); clf(figure(2)); hold on
    C00 = imfuse(syntheticImg_1, synthImg,'falsecolor','Scaling','joint','ColorChannels',[1 2 0]);
    imagesc(C00);
    colormap(gray);
    set(gca,'YDir','normal')
    
    figure(23); clf(figure(23)); hold on
    imagesc(synthImg);
    colormap(gray);
    set(gca,'YDir','normal')
    
    figure(22); clf(figure(22));
    subplot(211); hold on
    imagesc(combImg_up_pad);
    colormap(gray);
    set(gca,'YDir','normal')
    subplot(212); hold on
    imagesc(spectraImg_up_pad_2D_instDrift);
    colormap(gray);
    set(gca,'YDir','normal')
end





