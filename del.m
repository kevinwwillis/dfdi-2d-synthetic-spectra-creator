% function [combImg, combLineSep_px, fringePeriod_px, combLineSlope_px] = createSyntheticComb_V7(X_px, Y_px, X_up_pad_px, Y_up_pad_px, wl_up_pad_m, period_dPXdRad, endCombSepWanted_px, basedOnBeamNum)



%% Testing Params

clearvars;

img = fitsread('E:\Research\1D_pipeline_orig\HIP14810\profile\101_profile_fit\17_profile.fits');

period_dPXdRad = 8;

yUpsample_px = 20;

xUpsample_px = 20;

pad_px = 0;

Y_px = 1:25;                          % Choose pixel height of each spectra

X_px = 1:4001;                        % Choose pixel width of each spectra

basedOnBeamNum = 101;

obsNum = 17;

endCombSepWanted_px = 1.6;

load(strcat('E:\Research\1D_pipeline_orig\inputs\wlsoln.mat'));
wl_up_pad_m = interp1(X_px, squeeze(wlsoln(:, 1, basedOnBeamNum)) * 1e-10, (1:1/xUpsample_px:X_px(end)), 'linear');


% y-axis spatial data

imgHeight_px = Y_px(end);
Y_ind = 1:length(Y_px);

% Upsampled
Y_up_px = 1 :1/yUpsample_px: imgHeight_px;
Y_up_ind = 1:length(Y_up_px);

% Padded
Y_pad_px = 1 - pad_px/xUpsample_px : Y_px(end) + pad_px/xUpsample_px;
Y_pad_ind = 1:length(Y_pad_px);

% Upsampled and padded
Y_up_pad_px = Y_pad_px(1) :1/xUpsample_px: Y_pad_px(end);
Y_up_pad_ind = 1:length(Y_up_pad_px);

Y_withinPad_ind = pad_px : length(Y_up_pad_px) - (pad_px - 1);

%% x-axis spatial data


imgWidth_px = X_px(end);
X_ind = 1:length(X_px);

% Upsampled
X_up_px = 1 :1/xUpsample_px: imgWidth_px;
X_up_ind = 1:length(X_up_px);

% Padded
X_pad_px = 1 - pad_px/xUpsample_px : X_px(end) + pad_px/xUpsample_px;
X_pad_ind = 1:length(X_pad_px);

% Upsampled and padded
X_up_pad_px = X_pad_px(1) :1/xUpsample_px: X_pad_px(end);
X_up_pad_ind = 1:length(X_up_pad_px);

X_WithinPad_ind = pad_px : length(X_up_pad_px) - (pad_px - 1);


%% Spatial Matricies

WL_up_pad_m = repmat(wl_up_pad_m, length(Y_up_pad_px), 1);                  % Replicate the row of wavelengths to image height

% dWL = diff(wl);
% dWLdPX = [dWL(1) dWL];
% dPXdWL_up_pad = polyval(polyfit(X_px, 1./dWLdPX, 3), X_up_pad_px);
% 
% period_dRADdPX = 1 / period_dPXdRad;

wl_mid_m = WL_up_pad_m(1, find(min(abs(X_up_pad_px - 2000)) == abs(X_up_pad_px - 2000)));

load('E:\Research\2DsyntheticSpectraCreation\combLineSepFit.mat', 'combLineSepFit')
O_m = feval(combLineSepFit{basedOnBeamNum, 1}, endCombSepWanted_px);

%%


% y_rad = (Y_up_pad_px - 1) / period_dPXdRad;
% 
% Y_rad = repmat(y_rad', 1, size(WL_up_pad_m, 2));


xx = indClosestVal(X_up_pad_px, 3500):indClosestVal(X_up_pad_px, 3530);

for ii = 0:1e-4:1

% Y = repmat(linspace(0.074, 0.074 + 2e-6, size(WL_up_pad_m, 1))', 1, size(WL_up_pad_m, 2));
Y = repmat(linspace(ii, ii + 2e-6, size(WL_up_pad_m, 1))', 1, size(WL_up_pad_m, 2));

% B_dRADdM = 2 .* pi ./ WL_up_pad_m;

% A_rad = 2 .* pi .* Y_rad .* wl_mid_m ./ WL_up_pad_m;

combImg = 0.5 + 0.5 * cos(2 * pi .* Y(:,xx) ./ WL_up_pad_m(:,xx));

%% Predicted values
% 
% combLineSep_px = deriveCombLineSeparation(combImg(1,:), X_up_pad_px);
% combLineSep_px = spline(X_up_pad_px, combLineSep_px, X_px);
% 
% fringePeriod_px = deriveFringePeriod(period_dPXdRad, 2000, WL_up_pad_m(1,:), X_up_pad_px);
% fringePeriod_px = spline(X_up_pad_px, fringePeriod_px, X_px);
% 
% combLineSlope_px = fringePeriod_px ./ combLineSep_px;

% fringePhase_px = deriveFringePhase(fringePeriod_px, O_m, WL_up_pad_m(1,:));


%% Measured values
% 
% % Measure comb line separation
% combLineSep_measured_px = combLineSep_px;
% 
% % Measure fringe period
% fringePeriod_measured_px = measureFringePeriod(X_up_pad_px, Y_up_pad_px, X_px(1:10:end), combImg);
% 
% % Calc comb line slope
% combLineSlope_measured_px = fringePeriod_measured_px ./ combLineSep_measured_px;




figure(11); clf(figure(11)); hold on; title(num2str(ii))
imagesc(X_up_pad_px(xx), Y_up_pad_px, combImg);
colormap(gray);
set(gca,'YDir','normal')
pause(0.01)
end


