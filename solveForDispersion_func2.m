function [LSQ, imgClip] = solveForDispersion_func2(realSpectrum, wl_hiR_m, Y_px, C, tau_1, wl_offset, ...
    WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, WL_beam_log_gridded_m, Y_beam_log_gridded_0_px, LSF, flux_sun_pxv, gpu_q)


if sum(realSpectrum(:)) == 0
    LSQ = 0;
    imgClip = zeros(Y_px(end), size(realSpectrum, 2));
else
    % Create the comb
    combImg = createComb(wl_hiR_m, Y_px, C, tau_1, wl_offset, 0, gpu_q);
    
    
    % Multiply the comb and sun spectrum
    synthSpectrum = mat2gray(combImg) .* repmat(flux_sun_pxv, size(combImg, 1), 1); clear flux_sun_pxv combImg wl_hiR_m Y_px C tau_1
    
    
    % Convolve synth spectra with the PSF
    flux_hiR_ds_lsf_adu = conv2(synthSpectrum, LSF, 'same');
    
    
    % Create gridded data for final spectrum downsample interpolation
    % star_spectrum_log_deslanted = gpuArray(zeros(26, 4001));
    star_spectrum_log_deslanted = (zeros(size(realSpectrum)));
    star_spectrum_log_deslanted = interp2( ...
        WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, flux_hiR_ds_lsf_adu, ...
        WL_beam_log_gridded_m, Y_beam_log_gridded_0_px, 'linear', 0);
    
    clear WL_hiR_log_gridded_comb_m Y_hiR_gridded_comb_px WL_beam_log_gridded_m Y_beam_log_gridded_0_px
    
    
    imgClip = star_spectrum_log_deslanted;
    
    for col = 1:size(star_spectrum_log_deslanted, 2)
        
        tmp0 = normScale(star_spectrum_log_deslanted(:, col), 1, 0);
        tmp0(tmp0 < mean(tmp0(:))) = 0;
        star_spectrum_log_deslanted(:, col) = tmp0;
    end
    
    
    % Calulate LSQ value
    
%     subtracted0 = realSpectrum - mat2gray(star_spectrum_log_deslanted);
    subtracted0 = realSpectrum - star_spectrum_log_deslanted;
    
    LSQ = gather(sum((subtracted0(:)).^2));
end
% if gatherImg_q
% imgClip = gather(star_spectrum_log_deslanted);
% else
%     imgClip = [];
% end
% figure(78); clf(figure(78)); imagesc([realSpectrum ; mat2gray(star_spectrum_log_deslanted)]); colormap gray

1;
