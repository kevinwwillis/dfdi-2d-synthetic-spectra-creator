function I = createSyntheticComb_V3(imgWidth, imgHeight, wl)

%% Testing Params

% clearvars;
% 
% img = fitsread('E:\Research\1D_pipeline_orig\HIP14810\profile\101_profile_fit\17_profile.fits');
% wl = fitsread(strcat('E:\Research\2DsyntheticSpectraCreation\HIP_101_STAR_wlsoln.fits'));
% 
% yUpsample = 100;
% imgHeight = size(img,1);
% 
% xUpsample = 10;
% imgWidth = size(img,2);
% 
% beamNum = 101;
% 
% obsNum = 17;
% 
% % load('E:\Research\1D_pipeline_orig\inputs\wlsoln.mat', 'wlsoln')
% 
% wl = feval(fit((1:size(wl,2))', wl(obsNum,:)', 'linear'), 1:1/xUpsample:imgWidth)';
% 
% 
% yb = smoothn(fftFilter_V1(img(:,500), 2), 7);
% [a,b,phase_col500, period_col500, fit] = sinefit((1:length(yb))', yb, 6.5, 9);
% origPeaks_col500_x = (phase_col500:period_col500:imgHeight);


%% Spatial Matricies

row_pixel_height = imgHeight;

row_pixel_range = (1 :1/yUpsample: row_pixel_height)';

col_pixel_range = 1 :1/xUpsample: imgWidth;

col_pixel_matrix = repmat(col_pixel_range, row_pixel_height, 1);

lambda = wl .* 1e-10; %squeeze(wlsoln(:, 1, beamNum))' .* 1e-10;                            % Create equally spaced series of wavelengths in meters

lambda_matrix = repmat(lambda, length(row_pixel_range), 1);                  % Replicate the row of wavelengths to image height

%% Unused functions

% OPD = d ./ 2;

% d_m = lambda ./ (2 .* atan(opd1/opd2));                                        % Vertical fringe density (sine period)

%% Optical Path distance (OPD) at each physical pixel location.


% OPD_n = N_n * lambda_matrix(1);
% 
% OPD_n = ( (1:3) * lambda_matrix(1) ) + opd1;

dx_wl = (lambda_matrix(1, (col_pixel_range==500)) - lambda_matrix(1, (col_pixel_range==501)));

dx_px = 1;

dy = phase_col500 * (1 + abs(dx_wl));

slope_x = dy / dx_px

opd1 = 7.6 * 1e-3;

a2PiCyclesCount_col500 = (imgHeight-1) / period_col500;

Nrange = (phase_col500) + linspace(0, a2PiCyclesCount_col500, length(row_pixel_range));

OPD_n = ( Nrange * lambda_matrix(1, (col_pixel_range==500)) ) + opd1;


  y  = 1 - opd1/lambda - pha
 
% Nrange_y = 1 - opd1 / lambda_500;

Nrange_y1 = 1 - opd1 / lambda_matrix(1, (col_pixel_range==500)) - phase_col500;
Nrange_y2 = 1 - opd1 / lambda_matrix(1, (col_pixel_range==501)) - phase_col500;

1 / (Nrange_y1 - Nrange_y2)

% for alpha_y_degrees = -0.1 :0.001: 0.1
% for opd2 = opd1 :0.0001: opd1 + 100
% opd2 = 7.4+0.2326;

% OPD_y = 0.0314;

% alpha_y_degrees = 0.9;                                          % Mirror y-axis angle (degrees)
% 
% alpha_y_rad = alpha_y_degrees * 2 * pi / 180;                     % Mirror y-axis angle (rad)
% 
% OPD_y = row_pixel_range .* tan(alpha_y_rad);                      % y-axis optical distance
% 
% OPD = repmat(OPD_y .* row_pixel_range, 1, imgWidth);                                   % Replicate vector and add xy OPDs

% OPD_y = linspace(opd1, opd2, row_pixel_height)' * 1e-3;
% 
% OPD = repmat(OPD_y, 1, imgWidth);                                   % Replicate vector

OPD = repmat(OPD_n', 1, length(col_pixel_range));                                   % Replicate vector

%% Intensity Calculations

d_phi = (2 .* pi ./ lambda_matrix) .* OPD;                            % EM wave recombination phase offset

% phiPrime = repmat((row_pixel_range - 1) / 10, 1, imgWidth);               % (Not used)Row-wise phase offset (Estimation)

I_1 = 0.5;                                                              % Split beam 1 intesity

I_2 = 0.5;                                                              % Split beam 2 intesity

I = I_1 + I_2 + 2 .* (I_1 .* I_2).^(1/2) .* cos(d_phi);               % Intensity of recombined beams

%% Plotting

y = I(:, 3500);
x = 1:length(y);




figure(1); clf(figure(1))
subplot(211); hold on
xr = find(col_pixel_range==450):find(col_pixel_range==550);
imagesc(col_pixel_range(xr), row_pixel_range, I(:,xr));
plot(repmat(500, 1, length(origPeaks_col500_x)), origPeaks_col500_x, 'om')
set(gca,'YDir','normal'); 
colormap(gray)
% xlim([500 600])
subplot(212); hold on
xr = find(col_pixel_range==3900):find(col_pixel_range==4000);
imagesc(col_pixel_range(xr), row_pixel_range, I(:,xr));
set(gca,'YDir','normal'); 
colormap(gray)
% xlim([3900 4001])
% ylabel('Row # [px.]')
% xlabel('Col # [px.]')

% figure(2); clf(figure(2)); hold on
% p1 = plot(row_pixel_range, I(:,1), '.-k', 'displayname', 'First col');
% p2 = plot(row_pixel_range, I(:,imgWidth), '.-r', 'displayname', 'Last col');
% legend([p1 p2])
% xlabel('Row # [px.]')
% ylabel('Intensity')

pause(.3)
opd2
% alpha_y_degrees
% end
%%