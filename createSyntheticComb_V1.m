function combLineImg = createSyntheticComb(img)

img = FFTfilterFunc_V2(img, 4, 1, 1, 0, 0);

[combLineImg, offsetA, offsetB, offsetC, ampA, ampB, ampC, periodA, periodB, phaseA, phaseB] = combLineDirectSolver_V2(img, 1);

% initPhase = 1;
% period = 8;
% combLineSep = 5;%1.6; %Original resolution value
% combOpacity = 0.3;
% 
% 
% mg = meshgrid(1:size(img, 1), 1:size(img, 2))';
% 
% phases = combLineSep * x;
% 
% per_mg = meshgrid(phases, 1:size(img, 1));
% 
% combLinesRaw = sin( ( (2.*pi) ./ period) .* mg + per_mg) + 1;
% 
% combLineImg = mat2gray(combLinesRaw) .* combOpacity;







