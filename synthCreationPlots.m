function synthCreationPlots(...
    changePlot, finalSpectraPlot, combAndSpectraPlot, ...
    syntheticImg_tio_1, synthImg_tio, combImg_up, spectraImg_up_2D_instDrift_tio)


if changePlot
    figure(2); clf(figure(2)); hold on
    title('Shift show plot')
    C00 = imfuse(syntheticImg_tio_1, synthImg_tio, 'falsecolor','Scaling','joint','ColorChannels',[1 2 0]);
    imagesc(C00);
    colormap(gray);
    set(gca,'YDir','normal')
end


if finalSpectraPlot
    figure(23); clf(figure(23)); hold on
    title('Final synth')
    imagesc(synthImg_tio);
    colormap(jet(99999));
    set(gca,'YDir','normal')
end


if combAndSpectraPlot
    figure(22); clf(figure(22));
    subplot(211); hold on
    title('Comb')
    imagesc(combImg_up);
    colormap(gray);
    set(gca,'YDir','normal')
    subplot(212); hold on
    title('Spectrum')
    imagesc(spectraImg_up_2D_instDrift_tio);
    colormap(gray);
    set(gca,'YDir','normal')
end

pause(0.1);
