function fringePeriod_px = deriveFringePeriod(knownPeriod_px, knownPeriodCol_px, WL_m, X_px)

fringePeriod_px = knownPeriod_px .* WL_m ./ WL_m(indClosestVal(X_px, knownPeriodCol_px));