function [dWL_dispersion_star_ang, dWL_dispersion_tio_ang, badWLsolutions_star, badWLsolutions_tio, spectraQuantity] = createDispersionData(...
    s, pipeline_dir, nolan_dir, neil_dir, ...
    plateName, plateEmulationName, templateBeam, spectraQuantity, WLdispersionLimit_ang, ...
    applyTiO_dispersion_q, applyStar_dispersion_q, useNolanWLsolutions_q, useKevinWLsolutions_q, useSynthWLsolutions_q, setDispProfZeroAt2000px_q, correctStarDispByTIOdispOffsets_q, ...
    meanZeroDispersionTIO_q, meanZeroDispersionStar_q, modelObsNum)


% Get model number
if ~useSynthWLsolutions_q
    

    
    
    % Load BCV
    if 0 == exist(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'legacy_data', s, 'results', s, 'bc.mat'), 'file')
        getNeilFile(s, strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'legacy_data', s, 'results', s, 'bc.mat'), strcat(neil_dir, s, plateEmulationName, s, 'results', s, 'bc.mat'))
    end
    load(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'legacy_data', s, 'results', s, 'bc.mat'), 'bc');

    bc = double(bc);
    spectraQuantity = size(bc, 2);

    dWL_dispersion_star_ang = zeros(size(bc, 2), 4001);
    dWL_dispersion_tio_ang = zeros(size(bc, 2), 4001);
    
else
    
    dWL_dispersion_star_ang = zeros(spectraQuantity, 4001);
    dWL_dispersion_tio_ang = zeros(spectraQuantity, 4001);
end



if 0 == exist(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'legacy_data', s, 'results', s, 'BRdd.mat'), 'file')
    getNeilFile(s, strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'legacy_data', s, 'results', s, 'BRdd.mat'), strcat(neil_dir, s, plateEmulationName, s, 'results', s, 'BRdd.mat'))
end
load(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'legacy_data', s, 'results', s, 'BRdd.mat'), 'dd');

dX_tio_px = nanmean(dd(templateBeam, :, :), 3); clear dd

%convert to new model
dX_tio_px = dX_tio_px - dX_tio_px(modelObsNum);



badWLsolutions_tio = [];


%% TIO dispersion
if applyTiO_dispersion_q
    
    % Load WLs
    if useNolanWLsolutions_q
        
        getRemoteFile(s, ...
            strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'wlsoln_nolan', s, beamNum2legacyBeamNumStr(templateBeam), '_TIO_wlsoln.fits'), ...
            strcat(nolan_dir, s, plateEmulationName, s, 'wlsoln', s, beamNum2legacyBeamNumStr(templateBeam), '_TIO_wlsoln.fits'))
        
        wl_allObs_tio_ang = fitsread(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'wlsoln_nolan', s, beamNum2legacyBeamNumStr(templateBeam), '_TIO_wlsoln.fits'));
        
        
    elseif useKevinWLsolutions_q
        
        %load(strcat(synthPipeline_dir, s, 'plates', s, plateEmulationName, s, 'wlsoln_rest', s, 'TIO_wlsoln_rest.mat'), 'wl_allBeam_rest_ang');
        load(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'wlsoln_rest', s, 'TIO_beam', num2str(templateBeam), '_allObs_wlsoln.mat'), 'wl_allObs_ang')
        
        %wl_dispersionPlate_tio_ang = wl_allBeam_rest_ang{templateBeam}; clear wl_allBeam_rest_ang
        wl_allObs_tio_ang = wl_allObs_ang; clear wl_allObs_ang
        
        
    elseif useSynthWLsolutions_q
        
        load('wlsoln.mat', 'wlsoln')
        wl_est_ang = squeeze(wlsoln(:, 1, templateBeam))';
        wl_allObs_tio_ang = generateSyntheticDispersedWLsolutions(wl_est_ang, spectraQuantity, WLdispersionLimit_ang);
        
        
    else
        
        getRemoteFile(s, ...
            strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'legacy_data', s, 'wlsoln', s, beamNum2legacyBeamNumStr(templateBeam), '_TIO_wlsoln.fits'), ...
            strcat(neil_dir, s, plateEmulationName, s, 'wlsoln', s, beamNum2legacyBeamNumStr(templateBeam), '_TIO_wlsoln.fits'))
        
        wl_allObs_tio_ang = fitsread(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'legacy_data', s, 'wlsoln', s, beamNum2legacyBeamNumStr(templateBeam), '_TIO_wlsoln.fits'));
        
    end
    
    % Find NaN'ed WL solutions (filtered)
    badWLsolutions_tio = find(sum(isnan(wl_allObs_tio_ang), 2) == size(wl_allObs_tio_ang, 2))';
    
    % Set the bad WL solutions to the model WL solution (Will filter out the observations from comparision)
    for ii = badWLsolutions_tio
        wl_allObs_tio_ang(ii, :) = wl_allObs_tio_ang(modelObsNum, :);
    end
    
    
    for on = 1:size(bc, 2)
        
        wl_allObs_tio_ang(on, :) = interp1(1:4001, wl_allObs_tio_ang(on, :), (1:4001) + dX_tio_px(on), 'spline');
    end
    
    
    
    % Calculate the dispersion profiles
    [~, dWL_dispersion_tio_ang, tioDispersionOffsets] = findRealDispersion2(wl_allObs_tio_ang, modelObsNum, meanZeroDispersionTIO_q);
    
    
    if setDispProfZeroAt2000px_q
        
        for ii = 1:size(dWL_dispersion_tio_ang, 1)
            dWL_dispersion_tio_ang(ii, :) = dWL_dispersion_tio_ang(ii, :) - dWL_dispersion_tio_ang(ii, 2000);
        end
    end
end


%% Plot & save all TIO disperison profiles
figure(811); clf(figure(811)); hold on;
titlel(cat(2, {cat(2, 'TIO', ' Dispersion')}, {cat(2, 'Plate: ', underscoreTEXfixer(plateName), ' | Beam: ', n2s(beamNum))}))
plot(1:4001, dWL_dispersion_tio_ang, '-k');
xlabel('Channel [px]')
ylabel('d$\lambda$ [\AA]')
grid on
hold off

plot_dispersion_dir = strcat(pipeline_dir, s, 'plates', s, plateName, s, 'plots', s, 'synthSpectrumCreationPlots', s, 'dispersion');
try mkdir(plot_dispersion_dir); catch; end
print(gcf, '-r0', '-dpng', strcat(plot_dispersion_dir, s, 'TIO', '_beam', n2s(modelObsNum), '_obsALL_dispersionPlot.png'))



badWLsolutions_star = [];

%% Star dispersion
if applyStar_dispersion_q
    
    % Load WLs
    if useNolanWLsolutions_q
        
        getRemoteFile(s, ...
            strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'wlsoln_nolan', s, beamNum2legacyBeamNumStr(templateBeam), '_STAR_wlsoln.fits'), ...
            strcat(nolan_dir, s, plateEmulationName, s, 'wlsoln', s, beamNum2legacyBeamNumStr(templateBeam), '_STAR_wlsoln.fits'))
        
        wl_allObs_star_ang = fitsread(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'wlsoln_nolan', s, beamNum2legacyBeamNumStr(templateBeam), '_STAR_wlsoln.fits'));
        
        
    elseif useKevinWLsolutions_q
        
        if 0 == exist(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'wlsoln_rest', s, 'STAR_beam', num2str(templateBeam), '_allObs_wlsoln.mat'), 'file')
            a1_createWLsolutions([], plateEmulationName, templateBeam, 'STAR', 0)
        end
        load(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'wlsoln_rest', s, 'STAR_beam', num2str(templateBeam), '_allObs_wlsoln.mat'), 'wl_allObs_ang', 'px_lineValleys_allObs_DS_px', 'wl_lineValleys_allObs_rest_ang')
        
        %wl_dispersionPlate_star_ang = wl_allBeam_rest_ang{templateBeam}; clear wl_allBeam_rest_ang
        wl_allObs_star_ang = wl_allObs_ang; clear wl_allObs_ang
        
        
    elseif useSynthWLsolutions_q
        
        load('wlsoln.mat', 'wlsoln')
        wl_est_ang = squeeze(wlsoln(:, 1, templateBeam))';
        wl_allObs_star_ang = generateSyntheticDispersedWLsolutions(wl_est_ang, spectraQuantity, WLdispersionLimit_ang);
        
        
    else
        
        if 0 == exist(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'legacy_data', s, 'wlsoln', s, beamNum2legacyBeamNumStr(templateBeam), '_STAR_wlsoln.fits'), 'file')
            getRemoteFile(s, ...
                strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'legacy_data', s, 'wlsoln', s, beamNum2legacyBeamNumStr(templateBeam), '_STAR_wlsoln.fits'), ...
                strcat(neil_dir, s, plateEmulationName, s, 'wlsoln', s, beamNum2legacyBeamNumStr(templateBeam), '_STAR_wlsoln.fits'))
        end
        
        wl_allObs_star_ang = fitsread(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'legacy_data', s, 'wlsoln', s, beamNum2legacyBeamNumStr(templateBeam), '_STAR_wlsoln.fits'));
        
    end
    
    
    %% Optional TIO disersion offset subtraction
    
    if correctStarDispByTIOdispOffsets_q
        
        for ii = 1:size(tioDispersionOffsets, 1)
            wl_allObs_star_ang(ii, :) = wl_allObs_star_ang(ii, :) - tioDispersionOffsets(ii, 1);
        end
    end
    
    
    
    %% Set each spectrum to rest by removal of doppler shift
    
    
    % Load stellar velocities
    if strcmp(plateEmulationName, 'HIP14810') & templateBeam == 101
       
        cprintf([1,0,1],  '\n WARNING: ')
        cprintf(-[1,0,1],  'Interpolating the stellar spectra to rest using "HIRES.mat" data. These are the known RV''s.')
        cprintf(-[1,0,1], '\n');
        
        load('HIRES.mat', 'HIRES')
    else
        load(strcat('E:\Research', s, 'oldResults', s, plateEmulationName, '_V.mat'), 'Vrawci')
    end
    
    % Load instrument drift
%     if 0 ~= exist(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'results_1D', s, 'shifts_total_tio_px.mat'), 'file')
%         
%         if 0 == exist(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'legacy_data', s, 'results', s, 'BRdd.mat'), 'file')
%             getNeilFile(s, strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'legacy_data', s, 'results', s, 'BRdd.mat'), strcat(neil_dir, s, plateEmulationName, s, 'results', s, 'BRdd.mat'))
%         end
%         load(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'legacy_data', s, 'results', s, 'BRdd.mat'), 'dd');
% 
%         dX_tio_px = nanmean(dd(templateBeam, :, :), 3); clear dd
%         
%         %convert to new model
%         dX_tio_px = dX_tio_px - dX_tio_px(modelObsNum);
%     else
%         load(strcat(pipeline_dir, s, 'plates', s, plateEmulationName, s, 'results_1D', s, 'shifts_total_tio_px.mat'), 'shifts_total_tio_px')
%         
%         dX_tio_px = nanmean(shifts_total_tio_px(templateBeam, :, :), 3); clear dd; clear shifts_total_tio_px
%         
%         %convert to new model
%         dX_tio_px = dX_tio_px - dX_tio_px(modelObsNum);
%     end
    


    
    
    %% Spent a LONG time figuring this out. I could not get the dispersion profile offsets removed. Leave it alone... Let it be...
    
%     %HIRES = HIRES - HIRES(modelObsNum);
%     
%     cols = 1:4001;
%     
%     wl_allObs_star_ang1 = nan(size(wl_allObs_star_ang));
%     
    for on = 1:size(bc, 2)
        
        wl_allObs_star_ang(on, :) = interp1(1:4001, wl_allObs_star_ang(on, :), (1:4001) + dX_tio_px(on), 'spline');
    end
%     
%     
%     for on = 1:size(bc, 2)
%        
%        dwl_dispersionPlate_star_ang1(on, :) = wl_allObs_star_ang1(on, :) - wl_allObs_star_ang1(modelObsNum, :);
%        
%        dwl_dispersionPlate_star_ang(on, :) = wl_allObs_star_ang(on, :) - wl_allObs_star_ang(modelObsNum, :);
%     end
%     
%     figure(41413423); clf(figure(41413423)); hold on; 
%     plot(1:4001, dwl_dispersionPlate_star_ang1 * c_mps ./ wl_allObs_star_ang, '-k'); 
%     %plot(1:4001, wl_dispersionPlate_star_ang2 - repmat(wl_dispersionPlate_star_ang2(modelObsSpectrum, :), size(bc, 2), 1), '--m')
%     plot(1:4001, dwl_dispersionPlate_star_ang * c_mps ./ wl_allObs_star_ang, '--b')
%     
%     figure(141143423); clf(figure(141143423)); hold on;
%     plot(1:4001, dwl_dispersionPlate_star_ang1, '-k'); 
%     %plot(1:4001, wl_dispersionPlate_star_ang2 - repmat(wl_dispersionPlate_star_ang2(modelObsSpectrum, :), size(bc, 2), 1), '--m')
%     plot(1:4001, dwl_dispersionPlate_star_ang, '--b')
    
    
    
    
    %% Check by recreation of identified WL solution lines
    
% 
%     wl_allObs_star_ang1 = nan(size(wl_allObs_star_ang));
%     
%     for on = 1:size(bc, 2)
%         
%         %ff = fit(px_lineValleys_allObs_DS_px{on}, wl_lineValleys_allObs_rest_ang{on}', 'poly6', 'Robust', 'LAR');
%         
%         ff = fit(px_lineValleys_allObs_DS_px{on}, wl_lineValleys_allObs_rest_ang{on}' * (1 + (bc(templateBeam, on) + HIRES(on)) / c_mps), 'poly6', 'Robust', 'LAR');
%         
%         %wl_allObs_star_ang1(on, :) = ff((1:4001)) / (1 - (bc(templateBeam, on) + HIRES(on)) / c_mps);
%         
%         wl_allObs_star_ang1(on, :) = ff((1:4001) + dX_tio_px(on));
%     end
%     
%     
%     for on = 1:size(bc, 2)
%        
%        dwl_dispersionPlate_star_ang1(on, :) = wl_allObs_star_ang1(on, :) - wl_allObs_star_ang1(modelObsNum, :);
%        
%        dwl_dispersionPlate_star_ang(on, :) = wl_allObs_star_ang(on, :) - wl_allObs_star_ang(modelObsNum, :);
%     end
%     
%     figure(443423); clf(figure(443423)); hold on; 
%     plot(1:4001, dwl_dispersionPlate_star_ang1 * c_mps ./ wl_allObs_star_ang, '-k'); 
%     %plot(1:4001, wl_dispersionPlate_star_ang2 - repmat(wl_dispersionPlate_star_ang2(modelObsSpectrum, :), size(bc, 2), 1), '--m')
%     plot(1:4001, dwl_dispersionPlate_star_ang * c_mps ./ wl_allObs_star_ang, '--b')
%     
%     figure(1443423); clf(figure(1443423)); hold on;
%     plot(1:4001, dwl_dispersionPlate_star_ang1, '-k'); 
%     %plot(1:4001, wl_dispersionPlate_star_ang2 - repmat(wl_dispersionPlate_star_ang2(modelObsSpectrum, :), size(bc, 2), 1), '--m')
%     plot(1:4001, dwl_dispersionPlate_star_ang, '--b')
%     
%     
    
    
    
    
    %%
    
    
    % Find NaN'ed WL solutions (filtered)
    badWLsolutions_star = find(sum(isnan(wl_allObs_star_ang), 2) == size(wl_allObs_star_ang, 2))';
    
    % Set the bad WL solutions to the model WL solution (Will filter out the observations from comparision)
    for ii = badWLsolutions_star
        wl_allObs_star_ang(ii, :) = wl_allObs_star_ang(modelObsNum, :);
    end
    
    [~, dWL_dispersion_star_ang, ~] = findRealDispersion2(wl_allObs_star_ang, modelObsNum, meanZeroDispersionStar_q);
    
    
    if setDispProfZeroAt2000px_q
        
        for ii = 1:size(dWL_dispersion_star_ang, 1)
            
            dWL_dispersion_star_ang(ii, :) = dWL_dispersion_star_ang(ii, :) - dWL_dispersion_star_ang(ii, 2000);
        end
    end
    
end

%% Plot & save all STAR disperison profiles
figure(812); clf(figure(812)); hold on;
titlel(cat(2, {cat(2, 'STAR', ' Dispersion')}, {cat(2, 'Plate: ', underscoreTEXfixer(plateName), ' | Beam: ', n2s(beamNum))}))
plot(1:4001, dWL_dispersion_star_ang, '-k');
xlabel('Channel [px]')
ylabel('d$\lambda$ [\AA]')
grid on
hold off

plot_dispersion_dir = strcat(pipeline_dir, s, 'plates', s, plateName, s, 'plots', s, 'synthSpectrumCreationPlots', s, 'dispersion');
try mkdir(plot_dispersion_dir); catch; end
print(gcf, '-r0', '-dpng', strcat(plot_dispersion_dir, s, 'STAR', '_beam', n2s(modelObsNum), '_obsALL_dispersionPlot.png'))





%% Save data

try mkdir(strcat(pipeline_dir, s, 'plates', s, plateName)); catch end
save(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'creationDispersionProfiles.mat'), 'dWL_dispersion_star_ang', 'dWL_dispersion_tio_ang')





