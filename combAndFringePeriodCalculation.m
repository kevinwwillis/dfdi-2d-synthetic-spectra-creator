function [fp, cp] = combAndFringePeriodCalculation(X_px, X_up_pad_px, WL_up_pad_m, tau_1, C)

wl2px = polyfit(WL_up_pad_m(1,:), X_up_pad_px, 4);
wls = polyval(polyfit(X_up_pad_px, WL_up_pad_m(1,:), 4), X_px);

wl_1 = wls(1);
n_y10 = ceil(2 * tau_1 / wl_1 + 1 / 2);

for col = X_px
   
    wl_1 = wls(col);
    
    % Fringe period calculated
    y_zro = 1 / C * ( (n_y10:n_y10+5) * wl_1 / 2 - tau_1 - wl_1 / 4);
    fp(col) = 2 * nanmean(diff(y_zro));
    
    
    % Comb line period calculated
    n_y1 = ceil(2 * (C + tau_1) / wl_1 + 1 / 2);
    wl_zro = 4 * tau_1 ./ (2 * (n_y1:-1:n_y1-5) - 1);
    px_zero = polyval(wl2px, wl_zro);
    cp(col) = 2 * nanmean(diff(px_zero));
end