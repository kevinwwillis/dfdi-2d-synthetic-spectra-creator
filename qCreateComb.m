function C = qCreateComb(wl_m, Y_px, tau, C)

Y_px = Y_px(:);

wl_m = wl_m(:)';

Y0 = ( tau + C .* (Y_px - 1) );

C = 0.5 + 0.5 * cos( 2 * pi * repmat(Y0, 1, size(wl_m, 2)) ./ (repmat(wl_m, size(Y0, 1), 1)) );

%qf(11111, 1); imagesc(combImg)




