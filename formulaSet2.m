
%% Spatial Matricies

row_pixel_height = 24;

row_pixel_range = (1 : row_pixel_height)';

col_pixel_range = 1 : 4001;

col_pixel_matrix = repmat(col_pixel_range, row_pixel_height, 1);

lambda = linspace(500, 580, 4001) .* 1e-9;                            % Create equally spaced series of wavelengths in meters

lambda_matrix = repmat(lambda, row_pixel_height, 1);                  % Replicate the row of wavelengths to image height

%% Unused functions

% OPD = d ./ 2;

% d_m = lambda ./ (2 .* alpha);                                        % Vertical fringe density (sine period)

%% Optical Path distance (OPD) at each physical pixel location.

alpha_x_degrees = -0.000001;                                          % Mirror x-axis angle (degrees)

alpha_x_degrees = alpha_x_degrees * 2 * pi / 180;                     % Mirror x-axis angle (rad)

OPD_x = col_pixel_range .* tan(alpha_x_degrees);                      % x-axis optical distance

OPD = repmat(OPD_x, row_pixel_height, 1);                             % Replicate vector

alpha_y_degrees = 0.0000019;                                          % Mirror y-axis angle (degrees)

alpha_y_degrees = alpha_y_degrees * 2 * pi / 180;                     % Mirror y-axis angle (rad)

OPD_y = row_pixel_range .* tan(alpha_y_degrees);                      % y-axis optical distance

OPD = OPD + repmat(OPD_y, 1, 4001);                                   % Replicate vector and add xy OPDs

%% Intensity Calculations

d_phi = (2 .* pi ./ lambda_matrix) .* OPD;                            % EM wave recombination phase offset

phiPrime = repmat((row_pixel_range - 1) / 10, 1, 4001);               % (Not used)Row-wise phase offset (Estimation)

I_1 = 1;                                                              % Split beam 1 intesity

I_2 = 1;                                                              % Split beam 2 intesity

I = I_1 + I_2 + 2 .* (I_1 .* I_2).^(1/2) .* cos(d_phi);               % Intensity of recombined beams

%% Plotting


figure(1); clf(figure(1))
imagesc(I);set(gca,'YDir','normal'); colormap(gray)

figure(2); clf(figure(2))
plot(row_pixel_range, I(:,1), '.-k', row_pixel_range, I(:,4001), '.-r')

%%






% row_pixel_height = 24;
% 
% row_pixel_range = (1 : row_pixel_height)';
% 
% col_pixel_range = 1 : 4001;
% 
% col_pixel_matrix = repmat(col_pixel_range, row_pixel_height, 1);
% 
% 
% 
% 
% 
% lambda = linspace(500, 580, 4001) .* 1e-9;
% 
% lambda_matrix = repmat(lambda, row_pixel_height, 1);
% 
% 
% % m = fringe number
% 
% m = 0.001;
% 
% d_m = lambda ./ (2 .* alpha);
% 
% 
% 
% d = m .* lambda;
% 
% 
% % fringeSlope = 4 .* d_gamma ./ (c .* lambda);
% 
% nu = 2.99705e8;% SOL m/s in air
% 
% c = 299792458;
% 
% I_max = 1;
% I_min = 0;
% 
% gamma = (I_max - I_min) / (I_max + I_min);
% 
% phi_0 = 0;
% 
% I = 1 + gamma .* sin( 2 .* pi .* nu ./ (c .* lambda ./ d) + phi_0) ;
% 
% z =  nu ./ (c .* lambda ./ d);
% 
% 
% N = 1.5;
% 
% OPD = lambda .* N / 2 ;
% 
% d_phi = (2 .* pi) .* (OPD ./ lambda);
% 
% % d_phi = pi .* N;
% 
% 
% % Monochromatic two-beam interference equation
% 
% %def: OPD = optical path difference
% 
% % phi_1 - phi_2 = (2*pi/lambda) * OPD
% 
% I_1 = 1;
% I_2 = 1;
% 
% % I = I_1 + I_2 + 2 .* (I_1 .* I_2).^(1/2) .* cos(phi_1 - phi_2) ;
% 
% 
% OPD = (1:4001) .* tan (0.01);
% 
% d_phi = (2 .* pi ./ lambda) .* OPD;
% 
% I = I_1 + I_2 + 2 .* (I_1 .* I_2).^(1/2) .* cos(d_phi + phiPrime) ;
% 
% figure(1); clf(figure(1))
% plot(I, '-k')
% 
% figure(1); clf(figure(1))
% imagesc(comb_img)







