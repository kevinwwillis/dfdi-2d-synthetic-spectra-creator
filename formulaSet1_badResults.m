
row_pixel_height = 24;

row_pixel_range = (1 : row_pixel_height)';

col_pixel_range = 1 : 4001;

col_pixel_matrix = repmat(col_pixel_range, row_pixel_height, 1);



lambda_range = linspace(500, 580, 4001);

lambda_matrix = repmat(lambda, row_pixel_height, 1);



d0 = 4001;

linear_d_offset_px = ( row_pixel_height ./ ( 2 .* pi ) ) .* row_pixel_range;

linear_d_offset_matrix_px = repmat(linear_d_offset_px, 1, col_pixel_range(end));


linear_d_offset_wl = ( row_pixel_height ./ ( 2 .* pi ./ d0) ) .* row_pixel_range;

linear_d_offset_matrix_wl = repmat(linear_d_offset_wl, 1, col_pixel_range(end));



d = d0 + linear_d_offset_matrix_wl;

gamma = 1;

f = d ./ lambda_matrix;

% comb_img = 1 + gamma .* cos( 2 .* pi .* d ./ lambda_matrix );

% comb_img = 1 + gamma .* (cos( 2 .* pi .* d ./ col_pixel_matrix )).^2;

comb_img = 1 + gamma .* (cos( 2 .* pi .* d ./ lambda_matrix )).^2;


% t = 0.5;
% 
% comb_img = col_pixel_matrix .* (sin(t) ./ (1 - cos(t)) ) + ( 1 .* 500 + 2 .* 0.005) ./ ( 1-cos(t));


figure(1); clf(figure(1))
imagesc(comb_img)






