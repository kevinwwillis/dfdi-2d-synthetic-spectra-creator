function combImg = createSyntheticComb_V6(X_px, Y_px, X_up_pad_px, Y_up_pad_px, wl_up_pad_m, period_dPXdRad, endSepWanted_px)

% V6: Cleaned copy of V5.


%% Testing Params

% clearvars;
% 
% img = fitsread('E:\Research\1D_pipeline_orig\HIP14810\profile\101_profile_fit\17_profile.fits');
% wl = fitsread(strcat('E:\Research\2DsyntheticSpectraCreation\HIP_101_STAR_wlsoln.fits'));
% period_px = 8;
% yUpsample = 20;
% xUpsample = 20;
% imgHeight = 25;
% imgWidth = 4001;
% beamNum = 101;
% obsNum = 17;
% endSepWanted_px = 1.6;


%% OPD solver options
options = optimset;
options = optimset(options,'Display', 'off');
options = optimset(options,'MaxFunEvals', 5000);
options = optimset(options,'MaxIter',     5000);
options = optimset(options,'TolFun', 1e-16);
options = optimset(options,'TolX',   1e-16);


%% Spatial Matricies

% wl_up_pad_m = interp1(X_px', wl_m', X_up_pad_px, 'linear');

wl_up_pad_matrix_m = repmat(wl_up_pad_m, length(Y_up_pad_px), 1);                  % Replicate the row of wavelengths to image height


%% Optical Path distance (OPD) at each physical pixel location.

% Solve for OPD based on the desired comb line separation at the far right side of the spectra.
wl_1 = wl_up_pad_m(X_up_pad_px == (X_px(end) - 1));
wl_2 = wl_up_pad_m(X_up_pad_px == X_px(end));

endSep_dWLdPX_m = wl_2 - wl_1;      % WL separation between last two pixel locations.

wl_3 = wl_1 + endSepWanted_px * endSep_dWLdPX_m;

func = @(OPD) abs(1 - (cos(2 * pi * OPD / wl_3) - cos(2 * pi * OPD / wl_1)) / (wl_3 - wl_1));

% OPD_end_m = fminsearch(func, 20 * 1e-3, options);

[OPD_end_m, ~] = solve3DMinimizationProblem_V2(func, 0, 1000, 1000, 1e-16, 1e-16, 0, 0, 20, 20e-3, 0.01, 0.03, 1);

Y_up_px = Y_up_pad_px(Y_up_pad_px >= Y_px(1) & Y_up_pad_px <= Y_px(end));

radRange_0 = linspace(1, 3, length(Y_up_px));

radRange = interp1(Y_up_px, radRange_0, Y_up_pad_px, 'linear', 'extrap');

% Nrange = (Y_up_pad_px - 1) / period_dPXdRad;

Nrange = radRange / period_dPXdRad;

OPD_m = ( Nrange * wl_up_pad_matrix_m(1, (X_up_pad_px == 2000)) ) + OPD_end_m;

OPD_matrix_m = repmat(OPD_m', 1, length(X_up_pad_px));                                   % Replicate vector


%% Intensity Calculations

d_phi = (2 .* pi ./ wl_up_pad_matrix_m) .* OPD_matrix_m;                            % EM wave recombination phase offset

I_1 = 0.25;                                                              % Split beam 1 intesity

I_2 = 0.25;                                                              % Split beam 2 intesity

combImg = I_1 + I_2 + 2 .* (I_1 .* I_2).^(1/2) .* cos(d_phi);               % Intensity of recombined beams




figure(11); clf(figure(11));
imagesc(X_up_pad_px, Y_up_pad_px, combImg_up_pad_star);
colormap(gray);
set(gca,'YDir','normal')



