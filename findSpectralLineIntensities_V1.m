function lineIntensities = findSpectralLineIntensities_V1(img, testPlotsQ, spectraLineCenters_x)


colRange = 1:size(img,2);


img_1d = mat2gray(sum(img, 1));

pp = spline((10:colRange(end)-10)', img_1d(10:colRange(end)-10)');

img_1d_splined = nan(1, colRange(end));
img_1d_splined(10:colRange(end)-10) = ppval(pp, (10:colRange(end)-10));

rb = 200;
lb = 500;

[leftMax_x, leftMax_y, rightMax_x, rightMax_y, lineIntensities1] = deal(nan(1, length(spectraLineCenters_x)));

for spectralLineIndNum = 1:length(spectraLineCenters_x)
    
    valleyBottom = ppval(pp, spectraLineCenters_x(spectralLineIndNum));
    
    leftBox_x = round(spectraLineCenters_x(spectralLineIndNum) - lb : spectraLineCenters_x(spectralLineIndNum) - 1);
    leftBox_x = leftBox_x(leftBox_x > 1 & leftBox_x < colRange(end));
    leftBox_y = img_1d_splined(leftBox_x);
    [leftMax_y(spectralLineIndNum), leftMax_xInd] = max(leftBox_y);
    leftMax_x(spectralLineIndNum) = leftBox_x(leftMax_xInd);
    
    count0 = 0; pass = 0;
    while pass == 0
        rightBox_x = round(spectraLineCenters_x(spectralLineIndNum) + 1 : spectraLineCenters_x(spectralLineIndNum) + rb + count0 * rb/2);
        rightBox_x = rightBox_x(rightBox_x > 1 & rightBox_x < colRange(end));
        rightBox_y = img_1d_splined(rightBox_x);
        [rightMax_y(spectralLineIndNum), rightMax_xInd] = max(rightBox_y);
        rightMax_x(spectralLineIndNum) = rightBox_x(rightMax_xInd);
        
        if rightMax_x - spectraLineCenters_x(spectralLineIndNum) > 10 | count0 == 4
            pass = 1;
        else
            count0 = count0 + 1;
        end
    end
    
    dydx = (rightMax_y(spectralLineIndNum) - leftMax_y(spectralLineIndNum)) / (rightBox_x(rightMax_xInd) - leftBox_x(leftMax_xInd));
    
    yIntercept = leftMax_y(spectralLineIndNum) - dydx * leftBox_x(leftMax_xInd);
    
    valleyTop = dydx * spectraLineCenters_x(spectralLineIndNum) + yIntercept;
    
    lineIntensities1(spectralLineIndNum) = valleyTop - valleyBottom;
    
    %     figure(3); clf(figure(3)); hold on
    % %     plot(x, rimg_1d, '-k')
    %     x1 = round(slinesx(ii) - lb - 50 : slinesx(ii) + rb + 50 + c * rb/2);
    %     x1 = x1(x1>1 & x1<colRange(end));
    %     plot(x1, rimg_1d_splined(x1), '-c')
    %     plot([slinesx(ii) - lb, slinesx(ii) - lb], [0, 1], '-r')
    %     plot([slinesx(ii) + rb + c * rb/2, slinesx(ii) + rb + c * rb/2], [0, 1], '-r')
    %     plot(leftbox0x(lmvx), lmvy, 'sm')
    %     plot(rightbox0x(rmvx), rmvy, 'sb')
    %     plot(slinesx(ii), v, 'ok')
    %     xlim([slinesx(ii) - lb - 50 slinesx(ii) + rb + 50 + c * rb/2])
    %     pause(0.1)
    % %     if c > 0
    % %        1;
    % %     end
end


if testPlotsQ
    figure(333); clf(figure(333)); hold on
    imagesc(1:colRange(end), 0:1, img_1d); colormap(gray)
    plot(colRange, img_1d, '-k')
    plot(leftMax_x, leftMax_y, '*m', rightMax_x, rightMax_y, 'sg')
    x2 = [spectraLineCenters_x',spectraLineCenters_x'];
    valleyBottoms = ppval(pp, spectraLineCenters_x);
    plot(x2', ([valleyBottoms', valleyBottoms' + lineIntensities1'])', '-r')
end

continuumVals_x0 = [leftMax_x, rightMax_x];
[continuumVals_x1, uniqueInd] = unique(continuumVals_x0);
continuumVals_y0 = [leftMax_y, rightMax_y];
continuumVals_y1 = continuumVals_y0(uniqueInd);

[continuumVals_x2, sortInd] = sort(continuumVals_x1, 'ascend');
continuumVals_y2 = continuumVals_y1(sortInd);


continuumFit_y = feval(fit(continuumVals_x2', continuumVals_y2', 'smoothingSpline', 'smoothingparam', 0.0001), spectraLineCenters_x);
valleyBottoms = ppval(pp, spectraLineCenters_x);
lineIntensities = continuumFit_y' - valleyBottoms;

lineIntensities = (mat2gray(lineIntensities)) + 0.0000001;






