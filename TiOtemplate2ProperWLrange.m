% function TiOtemplate2ProperWLrange

% converts the hi resolution TIO template to the same WL range as the master comb image

clearvars

OS=computer; if strcmp(OS,'PCWIN64') || strcmp(OS,'PCWIN'); s='\'; else s='/'; end; clear OS


% %% Load master comb and its params
% 
% masterCombFolderName = 'masterComb_R247500_test';
% masterComb_dir = strcat('E:\Research\MARVELS_software\synthSpectraMaker_2D\masterCombImages\', masterCombFolderName);
% 
% load(strcat(masterComb_dir, s, 'wl_m.mat'), 'wl_m')
% 
% % Rename comb WLs
% wl_hiR_comb_m = wl_m; clear wl_m


%% Load TiO and its params

load('E:\Research\MARVELS_software\synthSpectraMaker_2D\TIO_TEMPLATE.mat', 'FL', 'WL');

% load('E:\Research\1D_pipeline_V2\WL_software\TEMPLATES\TIO_TemplateFlat.mat', 'FLcrop', 'WLcrop');
% 
% FL = FLcrop{1, 1}; clear FLcrop
% WL = WLcrop{1, 1}; clear WLcrop

% Rename comb flux
flux_hiR_tio_adu_0 = FL; clear FL

% Flip flux (current file is upside down for unknown reason
flux_hiR_tio_adu_0 = normScale(flux_hiR_tio_adu_0, 1, 0);

% Rename comb WLs
wl_hiR_tio_ang_0 = WL; clear WL

wl_hiR_tio_m_0 = wl_hiR_tio_ang_0 * 1e-10;

% Normalize the continuum
[~, flux_hiR_tio_adu_0, ~] = continuumFit_V4(wl_hiR_tio_m_0, flux_hiR_tio_adu_0, 0, 1);
flux_hiR_tio_adu_0 = flux_hiR_tio_adu_0';

%%

load('E:\Research\MARVELS_software\synthSpectraMaker_2D\masterSuntemplate.mat', 'wl_hiR_wide_sun_m', 'wl_hiR_wide_log_sun_m', 'flux_hiR_wide_sun_adu')


% Get index of desired range
% rangeInd = (find(wl_hiR_tio_m_0 == wl_hiR_comb_m(1))) : (find(wl_hiR_tio_m_0 == wl_hiR_comb_m(end)));
% rangeWideInd = (find(wl_hiR_tio_m_0 == wl_hiR_comb_m(1)) - 100) : (find(wl_hiR_tio_m_0 == wl_hiR_comb_m(end)) + 100);

% Cut down TiO flux to a bit wider than comb hires WL range
% flux_hiR_wide_tio_adu = flux_hiR_tio_adu_0(1, rangeWideInd);
flux_hiR_wide_tio_adu = interp1(wl_hiR_tio_m_0, flux_hiR_tio_adu_0, wl_hiR_wide_sun_m, 'spline', 0);

% Cut down TiO WLs to a bit wider than comb hires WL range
% wl_hiR_wide_tio_m = wl_hiR_tio_m_0(1, rangeWideInd);
wl_hiR_wide_tio_m = wl_hiR_wide_sun_m;

% Create logspace TiO WLs
% wl_hiR_wide_log_tio_m = log(wl_hiR_wide_tio_m);
wl_hiR_wide_log_tio_m = wl_hiR_wide_log_sun_m;





% figure(1); clf(figure(1)); imagesc(repmat(flux_hiR_tio_adu, 20, 1)); colormap gray

save('E:\Research\MARVELS_software\synthSpectraMaker_2D\masterTiOtemplate.mat', 'wl_hiR_wide_tio_m', 'wl_hiR_wide_log_tio_m', 'flux_hiR_wide_tio_adu')





realSpectrum = (fitsread('E:\Research\1D_pipeline_orig\HIP14810\TIO\BRdeslanted_orig\087_deslanted_fit\07_deslant.fits'));
wls = fitsread('E:\Research\1D_pipeline_orig\HIP14810\wlsoln\087_TIO_wlsoln.fits');


hiFlux = interp1(wl_hiR_wide_tio_m * 1e10, flux_hiR_wide_tio_adu, wls(7,:), 'spline');


figure(451); clf(figure(451)); 

% subplot(100,1,1:50)
imagesc([repmat(hiFlux, 10, 1) ; mat2gray(realSpectrum)]); colormap gray

% subplot(100, 1, 51:100)
% imagesc(realSpectrum); colormap gray




% %% interpolate to new range
% 
% % figure; plot(diff(wl_hiR_tio_m), '-k')
% 
% % figure; plot(diff(wl_hiR_comb_m), '-k')
% 
% flux_hiR_tio_adu = interp1(wl_hiR_tio_m_0', flux_hiR_tio_adu_0', wl_hiR_comb_m', 'spline', nan)';
% 
% % Convert all TIO WL values outside the comb WL range to zero
% flux_hiR_tio_adu(~isnan(flux_hiR_tio_adu)) = mat2gray(flux_hiR_tio_adu(~isnan(flux_hiR_tio_adu)));
% 
% % Convert all TIO WL values outside the comb WL range to zero
% flux_hiR_tio_adu(isnan(flux_hiR_tio_adu)) = 0;
% 
% wl_hiR_tio_m = wl_hiR_comb_m;
% 
% 
% figure(1); clf(figure(1)); imagesc(repmat(flux_hiR_tio_adu, 20, 1)); colormap gray
% 
% save('E:\Research\MARVELS_software\synthSpectraMaker_2D\masterTIOtemplate.mat', 'flux_hiR_tio_adu', 'wl_hiR_tio_m')













