% function marvels2DsyntheticSpectraCreation_testing2

clearvars

% image needs to be substaintially higher resolution than the original, that way small
% features can be blured and downsampled to look natural.

combOpacityLow = 0;
combOpacityHigh = 1;

slOpacityLow = 0;
slOpacityHigh = 1;

yUpsample = 100;
xUpsample = 10;

% Test vals 
plateName = 'HIP14810';
beamNum = 101;
obsNum = 17;

% dydx_comb = 4;



% rimg = fitsread(strcat('E:\Research\1D_pipeline_orig\47UMA\deslanted\048_deslanted_fit\28_deslant.fits'));
% img = fitsread(strcat('E:\Research\PV_density_analysis\deslantTIOimgs\day121_beam', num2str(beamNum), '_deslant_tio.fits'));
% img = fitsread(strcat('E:\Research\2DsyntheticSpectraCreation\HIP_101_17_deslant.fits'));
img = fitsread('E:\Research\1D_pipeline_orig\HIP14810\profile\101_profile_fit\17_profile.fits');
wl = fitsread(strcat('E:\Research\2DsyntheticSpectraCreation\HIP_101_STAR_wlsoln.fits'));
wl = wl(obsNum,:);


% % How many fringes at col 100?
% yb = smoothn(fftFilter_V1(img(:,500), 2), 7);
% xb = 1:length(yb);
% 
% ye = smoothn(fftFilter_V1(img(:,3500), 2), 7);
% xe = 1:length(ye);
% 
% figure(8); clf(figure(8)); plot(xb,yb,'.-k', xe,ye,'.-b')
% 
% numFringeB = length(findpeaks(yb));
% numFringeE = length(findpeaks(ye));
% 
% [a,b,phase_col500, period_col500, fit] = sinefit(xb', yb, 6.5, 9);
% % fit = a + b * sin(c + d .* x);

%%

imgn = mat2gray(img);

spectralLinesImg = 1 - createSyntheticSpectralLines_V1(img, plateName, beamNum, obsNum,  xUpsample, yUpsample, 1, 0);

% figure(1); clf(figure(1)); hold on
% imagesc(spectralLinesImg); 
% colormap(gray); 
% set(gca,'YDir','normal')


[combLineImg, wlUs] = createSyntheticComb_V4(size(img,2), size(img,1), beamNum, wl,  xUpsample, yUpsample);

%%

% syntheticImg = zeros(size(img));

ci = normScale(combLineImg, combOpacityLow, combOpacityHigh);

% si = normScale(spectralLinesImg, slOpacityLow, slOpacityHigh);

% syntheticImg = mat2gray((ci) .* (si));

syntheticImg = mat2gray((ci) .* (spectralLinesImg));

% syntheticImg = mat2gray(syntheticImg + syntheticImg_tmp);

%%

% PSF = synthetic2DspectraPSF_V1(wlUs);

% syntheticImg2 = conv2(syntheticImg, repmat(PSF, size(syntheticImg,1), 1), 'same');

% syntheticImg2 = syntheticImg2(:, 1:size(img,2));

syntheticImg2 = imgaussfilt(syntheticImg, 20);

syntheticImg2(:, 50:end-50) = mat2gray(syntheticImg2(:, 50:end-50));

syntheticImg2 = imresize(syntheticImg2, [size(img,1), size(img,2)], 'cubic', 'Antialiasing', false);


%%

figure(1); clf(figure(1)); hold on
imagesc([mat2gray(imgn - syntheticImg2); imgn ; syntheticImg2]); 
colormap(gray); 
set(gca,'YDir','normal')


figure(2); clf(figure(2)); hold on
subplot(211)
imagesc(syntheticImg)
colormap(gray); 
set(gca,'YDir','normal')
subplot(212)
imagesc(syntheticImg2)
colormap(gray); 
set(gca,'YDir','normal')



% xx =[slinesx',slinesx'];
% yy = [repmat(18, length(slinesx), 1),repmat(22, length(slinesx), 1)];
% plot(xx', yy', '-r')

hold off


% fitswrite(syntheticImg, 'E:\Research\PipelineSoftware\continuumFitting\syntheticImg_test.fits')

