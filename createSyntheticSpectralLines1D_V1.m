function spectralLinesNorm = createSyntheticSpectralLines1D_V1(colRangeUp, spectraLineCenters_x, lineIntensities, noLineSpread)

% Create 1D spectral line image

% spectralLinesRaw = ones(1, length(colRangeUp));
%
% for colInd = 1:length(spectraLineCenters_x)
%
%     col = spectraLineCenters_x(colInd);
%
%     spectralLinesRaw = spectralLinesRaw - lineIntensities(colInd) * (normpdf(colRangeUp, col));
% end
%
% spectralLinesNorm = mat2gray(spectralLinesRaw);


spectralLinesRaw = ones(1, length(colRangeUp));
if ~noLineSpread
    
    
    for colInd = 1:length(spectraLineCenters_x)
        
        col = spectraLineCenters_x(colInd);
        
        spectralLinesRaw = spectralLinesRaw - lineIntensities(colInd) * (normpdf(colRangeUp, col));
    end
    
    spectralLinesNorm = mat2gray(spectralLinesRaw);
    
else
    
    X_px = colRangeUp(1):colRangeUp(end);
    
    for colInd = 1:length(spectraLineCenters_x)
        
        x = find(min(abs(X_px - spectraLineCenters_x(colInd))) == abs(X_px - spectraLineCenters_x(colInd)));
        
        spectralLinesRaw(1, x) = 0;
    end
    
    spectralLinesNorm = mat2gray(spectralLinesRaw);
    
end







