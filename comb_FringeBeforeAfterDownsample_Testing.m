% create a typical plate of spectra synthetically with their own unique parameters

clearvars

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

synthPipelineDir = 'E:\Research\1a_syntheticPipelineTestEnv';

oneDpipelineDir = 'E:\Research\1D_pipeline_orig';

plots = 0;                          % Toggle plot viewing

beamNums = 1:2;                     % Beams to create

spectraQuantity = 5;               % How many observations

BCVmax_mps = 20000;                     % Max barycentric velocity to use

basisPlate = 'HIP14810';            % Stars based on which current plate?

basisBeamNum = 101;            % Stars based on which current plate?

modelSpectra = 1;                   % Choose model spectra for all beams

% yPadding = 10;                      % Pad images for proper PSF convolution

% xPadding = 10;                      % Pad images for proper PSF convolution



X_px = 1:4001;                        % Choose pixel width of each spectra

Y_px = 1:25;                          % Choose pixel height of each spectra

combOpacityLow = 0;                 % Choose opacity lower limit of the synth comb

combOpacityHigh = 1;                % Choose opacity upper limit of the synth comb

slOpacityLow = 0;                   % Choose opacity lower limit of the synth spectral lines

slOpacityHigh = 1;                  % Choose opacity upper limit of the synth spectral lines

combEndLineSep_px = 1.6;               % Comb line separation at px 4000

midFringePeriod_px = 8;                % Comb line fringe period



% Stellar params

maxRV_mps = 10;                         % [m/s] Maximum RV to create

orbitPeriod_day = 100;                  % [days] Obit period of planet

surveyDaySpan_day = 600;                % [days] Survey span

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

OS=computer; if strcmp(OS,'PCWIN64') || strcmp(OS,'PCWIN'); s='\'; else s='/'; end

% Define padding for images
pad_px = 25;%sigmaBlur * 2;
pad_px = 1;%sigmaBlur * 2;

% Define the speed of light
%c_SOL = 2.99792458e8; % m/s vacuum
c_SOL_mps = 2.99705e8; % m/s air

% Load and set the wavelengths for these spectra
load(strcat(oneDpipelineDir, '\inputs\wlsoln.mat'), 'wlsoln')
wlsoln_m = squeeze(wlsoln(:,1,:)) * 1e-10; % Angstom to Meters

v = VideoWriter('synthFringeVsGaussFilt.avi');
v.FrameRate = 2;
v.Quality = 100;
open(v);

figure(23); clf(figure(23));
figureFullScreen(gcf, 1)


    
    
    
    yUpsample_px = 20;                     % Upsample for spectra height
    
    xUpsample_px = 20;                     % Upsample for spectra width
    
    sigmaBlur = 4 * round(sqrt(xUpsample_px * yUpsample_px));
    
    
    %% y-axis spatial data
    
    imgHeight_px = Y_px(end);
    Y_ind = 1:length(Y_px);
    
    % Upsampled
    Y_up_px = 1 :1/yUpsample_px: imgHeight_px;
    Y_up_ind = 1:length(Y_up_px);
    
    % Padded
    Y_pad_px = 1 - pad_px/xUpsample_px : Y_px(end) + pad_px/xUpsample_px;
    Y_pad_ind = 1:length(Y_pad_px);
    
    % Upsampled and padded
    Y_up_pad_px = Y_pad_px(1) :1/xUpsample_px: Y_pad_px(end);
    Y_up_pad_ind = 1:length(Y_up_pad_px);
    
    Y_withinPad_ind = pad_px : length(Y_up_pad_px) - (pad_px - 1);
    
    %% x-axis spatial data
    
    imgWidth_px = X_px(end);
    X_ind = 1:length(X_px);
    
    % Upsampled
    X_up_px = 1 :1/xUpsample_px: imgWidth_px;
    X_up_ind = 1:length(X_up_px);
    
    % Padded
    X_pad_px = 1 - pad_px/xUpsample_px : X_px(end) + pad_px/xUpsample_px;
    X_pad_ind = 1:length(X_pad_px);
    
    % Upsampled and padded
    X_up_pad_px = X_pad_px(1) :1/xUpsample_px: X_pad_px(end);
    X_up_pad_ind = 1:length(X_up_pad_px);
    
    X_WithinPad_ind = pad_px : length(X_up_pad_px) - (pad_px - 1);
    
    
    %% STAR
    
    wl_m_star = repmat(wlsoln_m, 1, 1, spectraQuantity);
    
    beamNum = 1;
    
    wl_up_pad_rest_m = interp1(X_px, squeeze(wl_m_star(:, 1, 1)), X_up_pad_px, 'pchip');
    
    % Create comb image
    [combImg_0, combLineSep_px(:, beamNum), fringePeriod_px(:, beamNum), combLineSlope_px(:, beamNum)]...
        = createSyntheticComb_V7(...
        X_px, Y_px, X_up_pad_px, Y_up_pad_px, wl_up_pad_rest_m, midFringePeriod_px, combEndLineSep_px, beamNum);
    
    
    % Normalize comb image to desired scale
    combImg_0 = normScale(combImg_0, 0.1, 1);
    
    spectralLineCenters_px_star_orig = [510, 3510];
    lineIntensities_star_orig = ones(1, length(spectralLineCenters_px_star_orig));
    
    
    spectraImg_up_pad_1D_star = createSyntheticSpectralLines1D_V1(...
        X_up_pad_px, ...
        spectralLineCenters_px_star_orig, ...
        lineIntensities_star_orig, ...
        0);
    
    
    spectra0 = repmat(1 - spectraImg_up_pad_1D_star, size(combImg_0,1), 1);
    
    spectra0 = normScale(spectra0, 0.1, 1);
    
    finalSpectra = combImg_0 .* spectra0;
    
    
    u(1,:) = mat2gray(finalSpectra(:, indClosestVal(X_up_pad_px, 510)));
    u(2,:) = mat2gray(finalSpectra(:, indClosestVal(X_up_pad_px, 3510)));
    
    
    figure(21); clf(figure(21));
    subplot(311); hold on
    imagesc(X_up_pad_px, Y_up_pad_px, combImg_0);
    colormap(gray);
    set(gca,'YDir','normal')
    
    subplot(312); hold on
    imagesc(X_up_pad_px, Y_up_pad_px, spectra0);
    colormap(gray);
    set(gca,'YDir','normal')
    
    subplot(313); hold on
    imagesc(X_up_pad_px, Y_up_pad_px, finalSpectra);
    colormap(gray);
    set(gca,'YDir','normal')
    
    
    
    
    xx = indClosestVal(X_up_pad_px, 500) : indClosestVal(X_up_pad_px, 520);
    
    xx2 = indClosestVal(X_up_pad_px, 3500) : indClosestVal(X_up_pad_px, 3520);
    
  
    
PSF_Range = 1 :5: 60;

for iiInd = 1:length(PSF_Range)
    
    ii = PSF_Range(iiInd);
    
    finalSpectra_blur = gaussfiltGPU_V1(finalSpectra, ii);
    
    finalSpectra_blur = finalSpectra_blur(Y_withinPad_ind, X_WithinPad_ind);
    
    finalSpectra_blur_ds = imresize(finalSpectra_blur, [imgHeight_px, imgWidth_px], 'cubic', 'Antialiasing', false);
    
    n(1,:) = mat2gray(finalSpectra_blur_ds(:,510));
    n(2,:) = mat2gray(finalSpectra_blur_ds(:,3510));
    
    
    finalSpectra_blur_fft = fftFilter_V1(finalSpectra_blur, 2);
    finalSpectra_blur_ds_fft = fftFilter_V1(finalSpectra_blur_ds, 2);
    
    
    
    figure(2); clf(figure(2))
    subplot(2,3,1); hold on
    title('500')
    plot(Y_up_pad_px, u(1, :), '.-k')
    plot(Y_px, n(1, :), '.-m')
    
    subplot(2,3,2); hold on
    title(cat(2, 'Gauss Filter Sigma = ', num2str(ii)))
    imagesc(X_up_pad_px(xx), Y_up_pad_px, finalSpectra_blur(:, xx))
%         colormap(gray);
    set(gca,'YDir','normal')
    
    subplot(2,3,3); hold on
    title(cat(2, 'Downsampled'))
    imagesc(500:520, Y_px, finalSpectra_blur_ds(:, 500:520))
%         colormap(gray);
    set(gca,'YDir','normal')
    
    subplot(2,3,4); hold on
    title('3500')
    plot(Y_up_pad_px, u(2, :), '.-k')
    plot(Y_px, n(2, :), '.-m')
    
    subplot(2,3,5); hold on
    imagesc(X_up_pad_px(xx2), Y_up_pad_px, finalSpectra_blur(:, xx2))
%         colormap(gray);
    set(gca,'YDir','normal')
    
    subplot(2,3,6); hold on
    title(cat(2, 'Downsampled'))
    imagesc(3500:3520, Y_px, finalSpectra_blur_ds(:, 3500:3520))
%         colormap(gray);
    set(gca,'YDir','normal')
    
    
    
    
%     figure(23); clf(figure(23))
%     subplot(2,2,1); hold on
%     title(cat(2, 'Pre-downsample | Upsample Factor = ', num2str(ii), 'X'))
%     imagesc(X_up_pad_px(xx), Y_up_pad_px, finalSpectra_blur(:, xx));
%     colormap(gray);
%     set(gca,'YDir','normal')
%     xlabel('Channel # [px]')
%     xlabel('Row # [px]')
%     xlim([X_up_pad_px(xx(1))-0.5 X_up_pad_px(xx(end))+0.5])
%     ylim([0.5 imgHeight_px+0.5])
%     hold off
%     
%     subplot(2,2,3); hold on
%     imagesc(X_up_pad_px(xx2), Y_up_pad_px, finalSpectra_blur(:, xx2));
%     colormap(gray);
%     set(gca,'YDir','normal')
%     xlabel('Channel # [px]')
%     xlabel('Row # [px]')
%     xlim([X_up_pad_px(xx2(1))-0.5 X_up_pad_px(xx2(end))+0.5])
%     ylim([0.5 imgHeight_px+0.5])
%     hold off
%     
%     subplot(2,2,2); hold on
%     title(cat(2, 'Downsampled'))
%     imagesc(500:520, Y_px, finalSpectra_blur_ds(:, 500:520));
%     colormap(gray);
%     set(gca,'YDir','normal')
%     xlabel('Channel # [px]')
%     xlabel('Row # [px]')
%     xlim([500-0.5 520+0.5])
%     ylim([0.5 imgHeight_px+0.5])
%     hold off
%     
%     subplot(2,2,4); hold on
%     imagesc(3500:3520, Y_px, finalSpectra_blur_ds(:, 3500:3520));
%     colormap(gray);
%     set(gca,'YDir','normal')
%     xlabel('Channel # [px]')
%     xlabel('Row # [px]')
%     xlim([3500-0.5 3520+0.5])
%     ylim([0.5 imgHeight_px+0.5])
%     hold off
    
    if iiInd == 1
        for t = 1:2
            writeVideo(v, getframe(gcf));
        end
    elseif iiInd == length(PSF_Range)
        for t = 1:5
            writeVideo(v, getframe(gcf));
        end
    else
        writeVideo(v, getframe(gcf));
    end
    
end

close(v);













