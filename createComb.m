function combImg = createComb(wl_m, Y_px, C, tau, wl_offset, parP_q, gpu_q)


%% Create synth spectrum with no LSF or downsample

% Testing params

load('E:\Research\1a_syntheticPipelineTestEnv\beam1_CombParams.mat', 'C_best_px2', 'tau_best_px2', 'wl_offset_best_m2')
% Y_px = 1 :0.05: 30;

load('wlsoln.mat', 'wlsoln')

wlsoln = squeeze(wlsoln(:, 1, 101));

wl_m = wlsoln' * 1e-10;

Y_px = 1:26;

C = C_best_px2;

tau = tau_best_px2;

wl_offset = wl_offset_best_m2;

parP_q = 0;

gpu_q = 0;

tic

% Create the comb
if gpu_q
    Y0 = gpuArray(( tau + C .* (Y_px - 1) )');
else
    Y0 = ( tau + C .* (Y_px - 1) )';
end

% if gpu_q
%     combImg = gpuArray(zeros(length(Y0), length(wl_m)));
% else
%     combImg = zeros(length(Y0), length(wl_m));
% end
% 
% if parP_q
%     parfor rowInd = 1:length(Y0)
%         
%         combImg(rowInd, :) = cos( 2 * pi * Y0(rowInd) ./ (wl_m + wl_offset) );
%     end
% else
%     for rowInd = 1:length(Y0)
%         
%         combImg(rowInd, :) = cos( 2 * pi * Y0(rowInd) ./ (wl_m + wl_offset) );
%     end
% end

combImg = cos( 2 * pi * repmat(Y0, 1, size(wl_m, 2)) ./ (repmat(wl_m, size(Y0, 1), 1) + wl_offset) );

toc

qf(11111, 1); imagesc(combImg)

% combImg = flipud(combImg);






