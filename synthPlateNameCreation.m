function plateName = synthPlateNameCreation(plateEmulationName, templateBeam, ...
    addDispersion_q, linearWLsol_q, applyTIOdispersion_q, applyStar_dispersion_q, meanZeroDispersionStar_q, ...
    meanZeroDispersionTIO_q, useNolanWLsolutions_q, useKevinWLsolutions_q, useSynthWLsolutions_q, setDispProfZeroAt2000px_q, addNoise_q)




plateName = 'synthPlate';

plateName = strcat(plateName, '_', plateEmulationName, '_beam', num2str(templateBeam));

if addDispersion_q
  plateName = strcat(plateName, '_DP');
end

if linearWLsol_q
    plateName = strcat(plateName, '_LinWL');
end

if applyTIOdispersion_q & applyStar_dispersion_q
  plateName = strcat(plateName, '_dTS');
elseif applyTIOdispersion_q == 1 & applyStar_dispersion_q == 0
  plateName = strcat(plateName, '_dT');
elseif applyTIOdispersion_q == 0 & applyStar_dispersion_q == 1
  plateName = strcat(plateName, '_dS');
end

if meanZeroDispersionStar_q
  plateName = strcat(plateName, '_MZSD'); 
end

if meanZeroDispersionTIO_q
  plateName = strcat(plateName, '_MZTD'); 
end

if useNolanWLsolutions_q
  plateName = strcat(plateName, '_NWL'); 
elseif useKevinWLsolutions_q
  plateName = strcat(plateName, '_KWL'); 
elseif useSynthWLsolutions_q
  plateName = strcat(plateName, '_SWL', num2str(WLdispersionLimit_ang)); 
end

if setDispProfZeroAt2000px_q
  plateName = strcat(plateName, '_DZ2K'); 
end

if addNoise_q
  plateName = strcat(plateName, '_N', num2str(noiseLevel)); 
end

disp(cat(2, 'Synthetic plate name is: ', plateName))
