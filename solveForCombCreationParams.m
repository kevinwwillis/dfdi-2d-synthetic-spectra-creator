% function solveForCombCreationParams()

clearvars

downsample = 10;

R_LSF = 27000;

load('sun_WL_ang.mat', 'WL_ang')
load('E:\Research\MARVELS_software\synthSpectraMaker_2D\sun_flux_pxv.mat', 'flux_pxv')

load('wlsoln.mat', 'wlsoln')
maxWL_ang = max(max(wlsoln(:,1,:)));
minWL_ang = min(min(wlsoln(:,1,:)));

wl_lowR_m = squeeze(wlsoln(:,1,1))' .* 1e-10; clear wlsoln

dWL_sun_ang = nanmean(diff(WL_ang));

wl_hiR_m = ( floor(minWL_ang) :dWL_sun_ang * downsample: ceil(maxWL_ang) ) * 1e-10;

dWL_dPX = nanmean(diff(wl_hiR_m)) * 1e10;

% flux_sun_pxv = gpuArray(spline(WL_ang, flux_pxv, wl_hiR_m * 1e10)); clear WL_ang
flux_sun_pxv = (spline(WL_ang, flux_pxv, wl_hiR_m * 1e10)); clear WL_ang

wl_cent_ang = mean(wl_lowR_m) * 1e10;

% Create LSF
gaussianRange_x = 1:901;
mu = 450;
dl = wl_cent_ang / R_LSF / dWL_dPX;
sigma = (dl / (2 * sqrt(2 * log(2))));
psf_1d = normpdf(gaussianRange_x, mu, sigma) / 0.4;
LSF = psf_1d / sum(psf_1d); 

clear psf_1d dl mu gaussianRange_x wl_cent_ang dWL_sun_ang minWL_ang maxWL_ang R_LSF downsample flux_pxv


Y_px = 1 :0.05: 30;

% Create gridded data for final spectrum downsample interpolation
[WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px] = meshgrid(wl_hiR_m, Y_px);
[WL_beam_log_gridded_m, Y_beam_log_gridded_0_px] = meshgrid(wl_lowR_m, 1:26);


% Find real spectra
files0 = dir(strcat('E:\Research\1a_syntheticPipelineTestEnv\sunSpectra\*deslant*.fits'));

spectraNames = struct2cell(files0); clear files0

spectraNames = spectraNames(1,:);

for spectraNum = 1:20
    
%     realSpectrum = gpuArray(mat2gray(fitsread(strcat('E:\Research\1a_syntheticPipelineTestEnv\sunSpectra\', spectraNames{1, spectraNum}))));
    realSpectrum = (mat2gray(fitsread(strcat('E:\Research\1a_syntheticPipelineTestEnv\sunSpectra\', spectraNames{1, spectraNum}))));
    
%     realSpectrum(:, 50:3950) = continnumNorm(realSpectrum1(:, 50:3950), 1000);
%     realSpectrum(:, 60:3940) = flattenImgRows(realSpectrum(:, 60:3940), 1);
%     realSpectrum(:, [(1:60), (3940:4001)]) = 0;
    
% figure(88); clf(figure(88)); imagesc(realSpectrumGPU); colormap gray
    
    
%     [smoothedProfile, ~] = getSpectraProfile(realSpectrum, 1);
%     profileTemplate = smoothedProfile; clear smoothedProfile
%     
%     realSpectrumGPU = gpuArray(realSpectrum);
%     profileTemplateGPU = gpuArray(profileTemplate);
    
    
    disp(cat(2, '******** Spectra ', num2str(spectraNum), ' of ', num2str(spectraNum)))
    
    
    %   C     tau    wl_offset
    ub = [8.0e-08, 0.02, 100e-10];
    lb = [3.0e-08, 0.01, -100e-10];
    
    fun2 = @(x) solveForCombCreationParams_func(realSpectrum, wl_hiR_m, Y_px, x(1), x(2), x(3), ...
        WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, WL_beam_log_gridded_m, Y_beam_log_gridded_0_px, LSF, flux_sun_pxv);
    
    parP_q = 1;
    MaxFevals = 5000;
    MaxIter = 5000;
    TolFun = 1e-26;
    TolX = 1e-26;
    graph_q = 0;
    optimizationMatlabGraph_q = 1;
    numRandPoints = 1;
    startVals = [6.5555e-8, 0.014, 0];
    globalSolver_q = 0;
    
    [combParamsSolved(spectraNum, :), ~] = solve3DMinimizationProblem_V2(fun2, parP_q, MaxFevals, MaxIter, TolFun, TolX, graph_q, optimizationMatlabGraph_q, numRandPoints, startVals, lb, ub, globalSolver_q);
    
  
    
end





figure(33333); hold on
plot(chunkCenters_px2, R_solved_fit, '.-r')



save('E:\Research\1a_syntheticPipelineTestEnv\SpatiallyVariantResolutionSolvedData.mat', 'R_solved', 'chunkCenters_px', 'dR_dPX_solved_pp')





% Create the comb
combImg = createComb(wl_hiR_m, Y_px, combParamsSolved(spectraNum, 1), combParamsSolved(spectraNum, 2), combParamsSolved(spectraNum, 3), 1);

% Multiply the comb and sun spectrum
synthSpectrum = mat2gray(combImg) .* repmat(flux_sun_pxv, size(combImg, 1), 1);

% Convolve synth spectra with the PSF
flux_hiR_ds_lsf_adu = conv2(synthSpectrum, LSF, 'same');


% Create gridded data for final spectrum downsample interpolation
% star_spectrum_log_deslanted = gpuArray(zeros(26, 4001));
star_spectrum_log_deslanted = (zeros(26, 4001));
star_spectrum_log_deslanted = interp2( ...
    WL_hiR_log_gridded_comb_m, Y_hiR_gridded_comb_px, flux_hiR_ds_lsf_adu, ...
    WL_beam_log_gridded_m, Y_beam_log_gridded_0_px, 'linear', 0);


figure(78444); clf(figure(78444)); imagesc([mat2gray(realSpectrum(:, 300:3700)) ; (mat2gray([star_spectrum_log_deslanted(:, 300:3700)]))]); colormap gray







