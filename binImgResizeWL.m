function img2 = binImgResizeWL(img_up, X_px, Y_px, X_up_px, Y_up_px, wl_m, wl_up_m)
% function img2 = binImgResize(img, X_px, Y_px, X_up_px, Y_up_px, X_up_ind, Y_up_ind, xUpsample, yUpsample)

tic

img2 = nan(Y_px(end), X_px(end));




% Find WLs bin bounds
extent = interp1(X_px(2:end), diff(wl_m) / 2, X_px, 'spline');

bounds = [(wl_m - extent'), (wl_m + extent')];

xbounds(:, 1) = interp1(wl_up_m, X_up_px, bounds(:, 1), 'spline');
xbounds(:, 2) = interp1(wl_up_m, X_up_px, bounds(:, 2), 'spline');

% Find WLs bin bounds
yextent = interp1(Y_px(2:end), diff(Y_px) / 2, Y_px, 'spline');

ybounds = [(Y_px' - yextent'), (Y_px' + yextent')];

% Create surface fit of the image
[X, Y] = meshgrid(X_up_px, Y_up_px);
F = griddedInterpolant(X', Y', img_up');


for x = X_px
    
    for y = Y_px
        
%         img2(y, x) = nanmean(nanmean(img_up(yr_ind{y}, xr_ind{x})));

        xrange = linspace(xbounds(x, 1), xbounds(x, 2), 10)';
        yrange = linspace(ybounds(y, 1),ybounds(y, 2), 10)';

        vals = F(xrange, yrange);
        img2(y, x) = nanmean(vals(vals>0));
        
%         figure(1); clf(figure(1)); 
%         subplot(211); hold on
%         xr = indClosestVal(X_up_px, x-20) : indClosestVal(X_up_px, x+20);
%         xr(xr > length(X_up_px) | xr < 1) = [];
%         imagesc(X_up_px(xr), Y_up_px, img_up(:, xr));
%         plot([xrange(1), xrange(end)], [yrange(1), yrange(1)], '-m')
%         plot([xrange(1), xrange(end)], [yrange(end), yrange(end)], '-m')
%         plot([xrange(1), xrange(1)], [yrange(1), yrange(end)], '-m')
%         plot([xrange(end), xrange(end)], [yrange(1), yrange(end)], '-m')
%         
%         xlim([X_up_px(xr(1)) X_up_px(xr(end))])
%         ylim([Y_up_px(1) Y_up_px(end)])
%         
%         
%         subplot(212); hold on
%         xr2 = x-20 : x+20;
%         xr2(xr2 > length(X_px) | xr2 < 1) = [];
%         imagesc(xr2, Y_px, img2(:, xr2))
% %         plot(x, y, 'om')
%         plot([x-0.5, x+0.5], [y+0.5, y+0.5], '-m')
%         plot([x-0.5, x+0.5], [y-0.5, y-0.5], '-m')
%         plot([x-0.5, x-0.5], [y-0.5, y+0.5], '-m')
%         plot([x+0.5, x+0.5], [y-0.5, y+0.5], '-m')
%         
%         xlim([X_up_px(xr(1)) X_up_px(xr(end))])
%         ylim([Y_up_px(1) Y_up_px(end)])
%         
%         pause(0.01)
    end
    
%     img2(:, x) = mat2gray(img2(:, x));
    
%     figure(11); clf(figure(11)); hold on;
%     subplot(311)
%     imagesc(X_up_px, Y_up_px, img)
%     colormap(gray);
%     set(gca,'YDir','normal')
%     
%     subplot(312)
%     imagesc(X_px(1:x), Y_px, img2(:,1:x))
%     colormap(gray);
%     set(gca,'YDir','normal')
%     
%     subplot(313); hold on
%     plot(linspace(1, Y_px(end), length(img(:,x))), mat2gray(img(:,x)), '.-k')
%     plot(Y_px, mat2gray(img2(:,x)), '.-m')
%     
%     pause(0.01)
end
toc

figure(11); clf(figure(11)); hold on;
subplot(211)
imagesc(X_up_px, Y_up_px, img_up)
colormap(gray);
set(gca,'YDir','normal')

subplot(212)
imagesc(X_px, Y_px, img2)
colormap(gray);
set(gca,'YDir','normal')

1;

